package com.eklib.desktopviewer.persistance.repository.application;

import com.eklib.desktopviewer.persistance.model.application.ApplicationEntity;
import com.eklib.desktopviewer.persistance.model.application.DepartmentApplicationProductivityEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepositoryImpl;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Admin on 20.10.2015.
 */
@Repository
@Transactional
public class ApplicationRepositoryImpl extends BasePagingAndSortingRepositoryImpl<ApplicationEntity, Long> implements ApplicationRepository {

    @Override
    public ApplicationEntity findApplicationByName(String name) {
        Criteria criteria = getSession().createCriteria(ApplicationEntity.class);
        criteria.add(Restrictions.eq("name", name));
        return (ApplicationEntity)criteria.uniqueResult();
    }
    //TODO: rewrite this
    @Override
    public List<ApplicationEntity> findDepartmentFreeApplications(Long departmentId) {
        Criteria criteria = getSession().createCriteria(ApplicationEntity.class, "app");

        Criteria criteria1 = getSession().createCriteria(DepartmentApplicationProductivityEntity.class);
        criteria1.setProjection(Projections.groupProperty("application.id"));
        criteria1.createAlias("department", "department");
        criteria1.add(Restrictions.eq("department.id", departmentId));

        if(!criteria1.list().isEmpty()){
            criteria.add(Restrictions.not(Restrictions.in("id", criteria1.list())));
        }

        return criteria.list();
    }
}
