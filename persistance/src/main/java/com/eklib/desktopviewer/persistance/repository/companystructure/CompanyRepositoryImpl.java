package com.eklib.desktopviewer.persistance.repository.companystructure;

import com.eklib.desktopviewer.persistance.model.companystructure.CompanyEntity;
import com.eklib.desktopviewer.persistance.model.companystructure.DepartmentEntity;
import com.eklib.desktopviewer.persistance.model.enums.StatusEnum;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepositoryImpl;
import com.eklib.desktopviewer.persistance.repository.security.RoleRepository;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class CompanyRepositoryImpl extends BasePagingAndSortingRepositoryImpl<CompanyEntity, Long> implements CompanyRepository {


    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public CompanyEntity getCompanyByName(String name) {
        Criteria criteria = getSession().createCriteria(CompanyEntity.class);
        criteria.add(Restrictions.eq("name", name));
        return (CompanyEntity) criteria.uniqueResult();
    }

    @Override
    public boolean changeStatus(CompanyEntity company, StatusEnum newStatus) {
        if (!company.getStatus().equals(newStatus)) {
            if (company.getStatus().equals(StatusEnum.OPEN)) {
                for (DepartmentEntity department : company.getDepartments()) {
                    departmentRepository.changeStatus(department, newStatus);
                }
                company.setStatus(newStatus);
                update(company);
                return true;
            } else if (company.getStatus().equals(StatusEnum.PAUSED)) {
                if (newStatus.equals(StatusEnum.OPEN)) {
                    company.setStatus(newStatus);
                    update(company);
                    return true;
                } else {
                    for (DepartmentEntity department : company.getDepartments()) {
                        departmentRepository.changeStatus(department, newStatus);
                    }
                    company.setStatus(newStatus);
                    update(company);
                    return true;
                }
            } else if ((company.getStatus().equals(StatusEnum.CLOSED))) {
                company.setStatus(newStatus);
                update(company);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isCompanyAdmin(CompanyEntity company, UserEntity user) {
        if (company.getOwner().equals(user)) {
            return true;
        }

        return company.getUserCompanyRoles().stream()
                .filter(ucr -> RoleRepository.ADMIN_COMPANY_ROLE_NAME.equals(ucr.getRole().getName()))
                .anyMatch(ucr -> ucr.getUser().equals(user));
    }

    @Override
    public int getMaxSavingPeriod() {
        Criteria criteria = getSession().createCriteria(CompanyEntity.class);
        criteria.setProjection(Projections.max("screenshotsSavingMonth"));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return (int) criteria.uniqueResult();
    }
}
