package com.eklib.desktopviewer.persistance.model.enums;

/**
 * Created by Admin on 22.10.2015.
 */
public enum ProductivityEnum {
    PRODUCTIVE,
    NEUTRAL,
    UNPRODUCTIVE
}
