package com.eklib.desktopviewer.persistance.repository.security;

import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepositoryImpl;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by vadim on 30.09.2014.
 */
@Repository
@Transactional
public class UserRepositoryImpl extends BasePagingAndSortingRepositoryImpl<UserEntity, Long> implements UserRepository {

    private static final String FIND_FREE_USERS_SQL =
            "select * from user as usr " +
                    "where usr.id not in( " +
                    "select usr1.id from user as usr1 " +
                    "join user_project_role as upr on(usr1.id = upr.id_user) " +
                    "where upr.id_project = :projectId);";

    private static final String FIND_FREE_USERS_FOR_DEPARTMENT_SQL =
            "select * from user as usr " +
                    "where usr.id not in( " +
                    "select usr1.id from user as usr1 " +
                    "join user_department_role as udr on (usr1.id = udr.id_user) " +
                    "where udr.id_department = :departmentId); ";

    private static final String FIND_FREE_USERS_FOR_COMPANY_SQL =
            "select * from user as usr " +
                    "where usr.id not in( " +
                    "select usr1.id from user as usr1 " +
                    "join user_company_role as ucr on (usr1.id = ucr.id_user) " +
                    "where ucr.id_company = :companyId );";

    private static final String FIND_FREE_USERS_FOR_PROJECT_SQL =
            "SELECT us.* " +
                    "FROM desktopview.user as us " +
                    "inner join desktopview.user_company_role as com_user on com_user.ID_USER = us.ID " +
                    "inner join desktopview.client_company as com on com.ID = com_user.ID_COMPANY " +
                    "where com.ID = :companyId and us.id not in(select us1.id from desktopview.user as us1 join desktopview.user_project_role as pro_user1 on(us1.id = pro_user1.id_user) where pro_user1.id_project = :projectId) " +
                    "UNION " +
                    "SELECT us.* " +
                    "FROM desktopview.user as us " +
                    "inner join desktopview.user_department_role as dep_user on dep_user.ID_USER = us.ID " +
                    "inner join desktopview.department as dep on dep.ID = dep_user.ID_DEPARTMENT " +
                    "inner join desktopview.client_company as com on com.ID = dep.ID_CLIENT_COMPANY " +
                    "where com.ID = :companyId and us.id not in(select us1.id from desktopview.user as us1 join desktopview.user_project_role as pro_user1 on(us1.id = pro_user1.id_user) where pro_user1.id_project = :projectId) " +
                    "UNION " +
                    "SELECT us.* " +
                    "FROM desktopview.user as us " +
                    "inner join desktopview.user_project_role as pro_user on pro_user.ID_USER = us.ID " +
                    "inner join desktopview.project as pro on pro.id = pro_user.ID_PROJECT " +
                    "inner join desktopview.department as dep on dep.ID = pro.ID_DEPARTMENT " +
                    "inner join desktopview.client_company as com on com.ID = dep.ID_CLIENT_COMPANY " +
                    "where com.ID = :companyId and us.id not in(select us1.id from desktopview.user as us1 join desktopview.user_project_role as pro_user1 on(us1.id = pro_user1.id_user) where pro_user1.id_project = :projectId)";


    @Override
    public UserEntity getUserByName(String name) {
        Criteria criteria = getSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.or(Restrictions.eq("login", name), Restrictions.eq("email", name)));
        return (UserEntity) criteria.uniqueResult();
    }

    @Override
    public UserEntity getUserByToken(String token) {
        Criteria criteria = getSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.eq("changePasswordToken", token));
        return (UserEntity) criteria.uniqueResult();
    }

    @Override
    public UserEntity getUserByConfirmationToken(String token) {
        Criteria criteria = getSession().createCriteria(UserEntity.class);
        criteria.add(Restrictions.eq("confirmUserToken", token));
        return (UserEntity) criteria.uniqueResult();
    }

    @Override
    public List<UserEntity> findFreeUsers(Long projectId) {
        SQLQuery query = getSession().createSQLQuery(FIND_FREE_USERS_SQL).addEntity(UserEntity.class);
        query.setResultSetMapping("UserEntity");
        query.setParameter("projectId", projectId);
        return query.list();
    }

    @Override
    public List<UserEntity> findFreeProjectUsers(Long projectId, Long companyId) {
        SQLQuery query = getSession().createSQLQuery(FIND_FREE_USERS_FOR_PROJECT_SQL).addEntity(UserEntity.class);
        query.setParameter("companyId", companyId);
        query.setParameter("projectId", projectId);
        return query.list();
    }

    @Override
    public List<UserEntity> findFreeUsersForDepartment(Long departmentId) {
        SQLQuery query = getSession().createSQLQuery(FIND_FREE_USERS_FOR_DEPARTMENT_SQL).addEntity(UserEntity.class);
        query.setParameter("departmentId", departmentId);
        return query.list();
    }

    @Override
    public List<UserEntity> findFreeUsersForCompany(Long companyId) {
        SQLQuery query = getSession().createSQLQuery(FIND_FREE_USERS_FOR_COMPANY_SQL).addEntity(UserEntity.class);
        query.setParameter("companyId", companyId);
        return query.list();
    }

    @Override
    public List<UserEntity> findUsersForCompany(Long companyId) {
        Criteria criteria = getSession().createCriteria(UserEntity.class);
        criteria.createAlias("userProjectRoles", "roles");
        criteria.createAlias("roles.project", "project");
        criteria.createAlias("project.department", "department");
        criteria.createAlias("department.company", "company");
        criteria.add(Restrictions.eq("company.id", companyId));
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    @Override
    public boolean isSuperUser(UserEntity user) {
        String superUserRoleName = RoleRepository.ADMIN_APPLICATION;
        return user.getPrincipalRole() != null &&
                superUserRoleName.equals(user.getPrincipalRole().getName());
    }
}
