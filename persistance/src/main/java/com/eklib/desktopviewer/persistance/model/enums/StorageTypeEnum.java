package com.eklib.desktopviewer.persistance.model.enums;

public enum StorageTypeEnum {
    GoogleDrive,
    LocalStorage
}
