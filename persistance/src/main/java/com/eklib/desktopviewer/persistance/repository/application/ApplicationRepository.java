package com.eklib.desktopviewer.persistance.repository.application;

import com.eklib.desktopviewer.persistance.model.application.ApplicationEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepository;

import java.util.List;

/**
 * Created by Admin on 20.10.2015.
 */
public interface ApplicationRepository extends BasePagingAndSortingRepository<ApplicationEntity, Long>{

    ApplicationEntity findApplicationByName(String name);

    List<ApplicationEntity> findDepartmentFreeApplications(Long departmentId);
}
