package com.eklib.desktopviewer.persistance.model.snapshot;

import com.eklib.desktopviewer.persistance.model.LongGenericBaseEntity;
import com.eklib.desktopviewer.persistance.model.companystructure.ProjectEntity;
import com.eklib.desktopviewer.persistance.model.enums.ProductivityEnum;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SNAPSHOT")
public class SnapshotEntity extends LongGenericBaseEntity {

    @Column(name = "NOTE")
    private String note;

    @Column(name = "MESSAGE")
    private String message;

    @Column(name = "DATE")
    private Date date;

    @JoinColumn(name = "ID_PROJECT", nullable = true)
    @ManyToOne(fetch = FetchType.LAZY)
    private ProjectEntity project;

    @JoinColumn(name = "ID_USER", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private UserEntity user;

    @Column(name = "COUNT_MOUSE_CLICK")
    private Integer countMouseClick;

    @Column(name = "COUNT_KEYBOARD_CLICK")
    private Integer countKeyboardClick;

    @Column(name = "TIME_INTERVAL")
    private Integer timeInterval;

    @Enumerated(EnumType.STRING)
    @Column(name = "PRODUCTIVITY")
    private ProductivityEnum productivity = ProductivityEnum.NEUTRAL;

    @JoinColumn(name = "ID_IMAGE")
    @OneToOne(fetch = FetchType.EAGER)
    private ImageEntity image;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ProjectEntity getProject() {
        return project;
    }

    public void setProject(ProjectEntity project) {
        this.project = project;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public Integer getCountMouseClick() {
        return countMouseClick;
    }

    public void setCountMouseClick(Integer countMouseClick) {
        this.countMouseClick = countMouseClick;
    }

    public Integer getCountKeyboardClick() {
        return countKeyboardClick;
    }

    public void setCountKeyboardClick(Integer countKeyboardClick) {
        this.countKeyboardClick = countKeyboardClick;
    }

    public Integer getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(Integer timeInterval) {
        this.timeInterval = timeInterval;
    }

    public ProductivityEnum getProductivity() {
        return productivity;
    }

    public void setProductivity(ProductivityEnum productivity) {
        this.productivity = productivity;
    }

    public ImageEntity getImage() {
        return image;
    }

    public void setImage(ImageEntity image) {
        this.image = image;
    }
}
