package com.eklib.desktopviewer.persistance.model;

import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
public abstract class GenericBaseEntity<E extends Serializable> implements BaseEntity<E> {

    @Id
    @GeneratedValue
    @Column(name = "ID", unique = true, nullable = false)
    private E id;

    @Override
    public E getId() {
        return id;
    }

    @Override
    public void setId(E id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) {
            return true;
        }
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)){
            return false;
        }

        GenericBaseEntity entity = (GenericBaseEntity) o;
        if(this.getId() == null || entity.getId() == null){
            return false;
        }
        return this.getId().equals(entity.getId());
    }

    @Override
    public int hashCode() {
        return getId() == null ? super.hashCode() : getId().hashCode();
    }
}
