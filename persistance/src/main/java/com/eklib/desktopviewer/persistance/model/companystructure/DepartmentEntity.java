package com.eklib.desktopviewer.persistance.model.companystructure;

import com.eklib.desktopviewer.persistance.model.LongGenericBaseEntity;
import com.eklib.desktopviewer.persistance.model.application.DepartmentApplicationProductivityEntity;
import com.eklib.desktopviewer.persistance.model.enums.StatusEnum;
import com.eklib.desktopviewer.persistance.model.security.UserDepartmentRole;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Maxim on 10.11.2014.
 */

@Entity
@Table(name = "DEPARTMENT")
public class DepartmentEntity extends LongGenericBaseEntity implements Serializable {

    @Column(name = "NAME")
    private String name;

    @JoinColumn(name = "ID_CLIENT_COMPANY", nullable = false)
    @ManyToOne(targetEntity = CompanyEntity.class, fetch = FetchType.LAZY)
    private CompanyEntity company;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private StatusEnum status = StatusEnum.OPEN;

    @JoinColumn(name = "ID_OWNER", nullable = false)
    @ManyToOne(targetEntity = UserEntity.class, fetch = FetchType.LAZY)
    private UserEntity owner;

    @Column
    @OneToMany(mappedBy = "department", fetch = FetchType.LAZY,
            targetEntity = ProjectEntity.class, cascade = CascadeType.ALL)
    private Set<ProjectEntity> projects;

    @Column
    @OneToMany(mappedBy = "department", fetch = FetchType.LAZY,
            targetEntity = DepartmentApplicationProductivityEntity.class, cascade = CascadeType.ALL)
    private List<DepartmentApplicationProductivityEntity> applications;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "department", orphanRemoval = true,
            targetEntity = UserDepartmentRole.class, cascade = CascadeType.ALL)
    private Set<UserDepartmentRole> userDepartmentRoles = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CompanyEntity getCompany() {
        return company;
    }

    public void setCompany(CompanyEntity company) {
        this.company = company;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public UserEntity getOwner() {
        return owner;
    }

    public void setOwner(UserEntity owner) {
        this.owner = owner;
    }

    public Set<ProjectEntity> getProjects() {
        return projects;
    }

    public void setProjects(Set<ProjectEntity> projects) {
        this.projects = projects;
    }

    public List<DepartmentApplicationProductivityEntity> getApplications() {
        return applications;
    }

    public void setApplications(List<DepartmentApplicationProductivityEntity> applications) {
        this.applications = applications;
    }

    public Set<UserDepartmentRole> getUserDepartmentRoles() {
        return userDepartmentRoles;
    }

    public void setUserDepartmentRoles(Set<UserDepartmentRole> userDepartmentRoles) {
        this.userDepartmentRoles = userDepartmentRoles;
    }
}
