package com.eklib.desktopviewer.persistance.model;

import javax.persistence.EmbeddedId;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

/**
 * Created by s.sheman on 19.10.2015.
 */
@MappedSuperclass
public abstract class EmbeddedBaseEntity<E extends Serializable> implements BaseEntity<E>  {

    @EmbeddedId
    protected E id;

    @Override
    public E getId() {
        return id;
    }

    @Override
    public void setId(E id) {
        this.id = id;
    }
}
