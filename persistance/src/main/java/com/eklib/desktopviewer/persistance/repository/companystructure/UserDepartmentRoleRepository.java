package com.eklib.desktopviewer.persistance.repository.companystructure;

import com.eklib.desktopviewer.persistance.model.companystructure.DepartmentEntity;
import com.eklib.desktopviewer.persistance.model.security.Role;
import com.eklib.desktopviewer.persistance.model.security.UserDepartmentRole;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepository;

import java.util.List;

/**
 * Created by s.sheman on 23.10.2015.
 */
public interface UserDepartmentRoleRepository extends BasePagingAndSortingRepository<UserDepartmentRole, Long> {
    List<UserDepartmentRole> findByProperties(UserEntity user, DepartmentEntity department, Role role);

    void deleteUserDepartmentRolesForDepartment(DepartmentEntity department);
}
