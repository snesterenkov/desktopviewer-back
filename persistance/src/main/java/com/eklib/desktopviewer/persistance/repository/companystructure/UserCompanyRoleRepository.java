package com.eklib.desktopviewer.persistance.repository.companystructure;

import com.eklib.desktopviewer.persistance.model.companystructure.CompanyEntity;
import com.eklib.desktopviewer.persistance.model.security.Role;
import com.eklib.desktopviewer.persistance.model.security.UserCompanyRole;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepository;

import java.util.List;

/**
 * Created by s.sheman on 02.11.2015.
 */
public interface UserCompanyRoleRepository extends BasePagingAndSortingRepository<UserCompanyRole, Long> {
    public List<UserCompanyRole> findByProperties(UserEntity user, CompanyEntity company, Role role);

    public void deleteUserCompanyRolesForCompany(CompanyEntity company);
}
