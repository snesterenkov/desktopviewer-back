package com.eklib.desktopviewer.persistance.repository.snapshot;

import com.eklib.desktopviewer.persistance.model.companystructure.ProjectEntity;
import com.eklib.desktopviewer.persistance.model.snapshot.SnapshotEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepository;

import java.util.Date;
import java.util.List;

public interface SnapshotRepository extends BasePagingAndSortingRepository<SnapshotEntity, Long> {

    List<Date> getDatesWithSnapshotsByUser(Long userId);

    SnapshotEntity findSnapshotByUserIdAndProjectIdAndPeriod(Long userId, Date startDate, Date endDate);

    List<SnapshotEntity> findByUserIdAndDates(Long userId, Date startDate, Date endDate);

    Object[] findSnapshotsCountAndActivitySumForUsersByPeriodAndProjects(Long userId, List<ProjectEntity> projects, Date startDate, Date endDate);

    List<Object[]> getUsersStatsByCompanyAndDates(Long companyId, Date startDate, Date endDate);

    List<Object[]> getUsersStatsForProjectsAndDates(Date startDate, Date endDate, List<Long> projectsIds);
}
