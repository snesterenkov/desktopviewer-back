package com.eklib.desktopviewer.persistance.repository.companystructure;

import com.eklib.desktopviewer.persistance.model.companystructure.ProjectEntity;
import com.eklib.desktopviewer.persistance.model.security.Role;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.model.security.UserProjectRole;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepositoryImpl;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class UserProjectRoleRepositoryImpl extends BasePagingAndSortingRepositoryImpl<UserProjectRole, Long> implements UserProjectRoleRepository {

    @Override
    public List<UserProjectRole> findByProperties(UserEntity user, ProjectEntity project, Role role){
        Criteria criteria = getSession().createCriteria(UserProjectRole.class);
        if(user != null){
            criteria.add(Restrictions.eq("user", user));
        }
        if(project != null){
            criteria.add(Restrictions.eq("project", project));
        }
        if(role != null){
            criteria.add(Restrictions.eq("role", role));
        }
        return criteria.list();
    }

    @Override
    public void deleteUserProjectRolesForProject(ProjectEntity project){
        Criteria criteria = getSession().createCriteria(UserProjectRole.class);
        criteria.add(Restrictions.eq("project", project));
        List<UserProjectRole> usrForDelete = criteria.list();
        for (UserProjectRole usr : usrForDelete) {
            this.delete(usr);
        }
    }

    @Override
    public List<UserProjectRole> findUsersByProjects(List<ProjectEntity> projects) {
        Criteria criteria = getSession().createCriteria(UserProjectRole.class);
        criteria.setFetchMode("user", FetchMode.JOIN);
        criteria.add(Restrictions.in("project", projects));
        return criteria.list();
    }
}
