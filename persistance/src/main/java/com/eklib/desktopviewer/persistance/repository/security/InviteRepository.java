package com.eklib.desktopviewer.persistance.repository.security;


import com.eklib.desktopviewer.persistance.model.security.InviteEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepository;

public interface InviteRepository extends BasePagingAndSortingRepository<InviteEntity, Long>{

    InviteEntity getByToken(String token);

    long getInvitesCountForEmail(String email);
}
