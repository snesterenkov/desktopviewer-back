package com.eklib.desktopviewer.persistance.model.security;

import com.eklib.desktopviewer.persistance.model.LongGenericBaseEntity;
import com.eklib.desktopviewer.persistance.model.companystructure.StructureType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Лол on 08.10.2015.
 */
@Entity
@Table(name = "ROLE")
public class Role extends LongGenericBaseEntity implements Serializable{

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "CODE")
    private String code;

    @ManyToOne
    @JoinColumn(name = "ID_STRUCTURE_TYPE", nullable = false)
    private StructureType structureType;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ROLE_PERMISSION",
        joinColumns = @JoinColumn(name = "ID_ROLE"),
        inverseJoinColumns = @JoinColumn(name = "ID_PERMISSION"))
    private List<PermissionEntity> permissions;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<PermissionEntity> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PermissionEntity> permissions) {
        this.permissions = permissions;
    }

    public StructureType getStructureType() {
        return structureType;
    }

    public void setStructureType(StructureType structureType) {
        this.structureType = structureType;
    }
}
