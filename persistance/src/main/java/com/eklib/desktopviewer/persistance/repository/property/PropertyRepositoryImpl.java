package com.eklib.desktopviewer.persistance.repository.property;

import com.eklib.desktopviewer.persistance.model.property.PropertyEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepositoryImpl;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public class PropertyRepositoryImpl extends BasePagingAndSortingRepositoryImpl<PropertyEntity, Long> implements PropertyRepository {

    @Override
    public PropertyEntity findByName(String name) {
        Criteria criteria = getSession().createCriteria(PropertyEntity.class);
        criteria.add(Restrictions.eq("name", name));
        return (PropertyEntity)criteria.uniqueResult();
    }
}
