package com.eklib.desktopviewer.persistance.model.security;


import com.eklib.desktopviewer.persistance.model.BaseEntity;

import javax.persistence.*;

/**
 * Created by BigBoss on 08.10.2015.
 */

@Entity
@Table(name = "USER_PROJECT_ROLE")
@AssociationOverrides({
        @AssociationOverride(name = "pk.user", joinColumns = @JoinColumn(name = "ID_USER")),
        @AssociationOverride(name = "pk.project", joinColumns = @JoinColumn(name = "ID_PROJECT")),
        @AssociationOverride(name = "pk.role", joinColumns = @JoinColumn(name = "ID_ROLE"))
})
public class UserProjectRoleCompositeKey implements BaseEntity<UserProjectRoleId> {


    @EmbeddedId
    UserProjectRoleId pk = new UserProjectRoleId();

    public UserProjectRoleId getPk() {
        return pk;
    }

    public void setPk(UserProjectRoleId pk) {
        this.pk = pk;
    }

    @Override
    public UserProjectRoleId getId() {
        return getPk();
    }

    @Override
    public void setId(UserProjectRoleId id) {
        setPk(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserProjectRoleCompositeKey that = (UserProjectRoleCompositeKey) o;

        if (!pk.equals(that.pk)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return pk.hashCode();
    }
}