package com.eklib.desktopviewer.persistance.model.security;

import com.eklib.desktopviewer.persistance.model.companystructure.ProjectEntity;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by Лол on 08.10.2015.
 */
@Embeddable
public class UserProjectRoleId implements Serializable {

    @ManyToOne
    private UserEntity user;

    @ManyToOne
    private ProjectEntity project;

    @ManyToOne
    private Role role;

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public ProjectEntity getProject() {
        return project;
    }

    public void setProject(ProjectEntity project) {
        this.project = project;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserProjectRoleId that = (UserProjectRoleId) o;

        if (!project.equals(that.project)) return false;
        if (!role.equals(that.role)) return false;
        if (!user.equals(that.user)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = user.hashCode();
        result = 31 * result + project.hashCode();
        result = 31 * result + role.hashCode();
        return result;
    }
}
