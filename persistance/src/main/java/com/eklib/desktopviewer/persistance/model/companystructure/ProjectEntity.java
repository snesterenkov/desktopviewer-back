package com.eklib.desktopviewer.persistance.model.companystructure;

import com.eklib.desktopviewer.persistance.model.LongGenericBaseEntity;
import com.eklib.desktopviewer.persistance.model.enums.StatusEnum;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.model.security.UserProjectRole;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by vadim on 01.12.2014.
 */
@Entity
@Table(name = "PROJECT")
public class ProjectEntity extends LongGenericBaseEntity implements Serializable {

    @Column(name = "NAME")
    private String name;

    @JoinColumn(name = "ID_DEPARTMENT", nullable = false)
    @ManyToOne(targetEntity = DepartmentEntity.class, fetch = FetchType.EAGER)
    private DepartmentEntity department;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private StatusEnum status = StatusEnum.OPEN;

    @JoinColumn(name = "ID_OWNER", nullable = false)
    @ManyToOne(targetEntity = UserEntity.class, fetch = FetchType.EAGER)
    private UserEntity owner;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "project", orphanRemoval = true,
            targetEntity = UserProjectRole.class, cascade = CascadeType.ALL)
    private Set<UserProjectRole> userProjectRoles = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public UserEntity getOwner() {
        return owner;
    }

    public void setOwner(UserEntity owner) {
        this.owner = owner;
    }

    public DepartmentEntity getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentEntity department) {
        this.department = department;
    }

    public Set<UserProjectRole> getUserProjectRoles() {
        return userProjectRoles;
    }

    public void setUserProjectRoles(Set<UserProjectRole> userProjectRoles) {
        this.userProjectRoles = userProjectRoles;
    }
}
