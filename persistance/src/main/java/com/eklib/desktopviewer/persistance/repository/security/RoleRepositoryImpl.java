package com.eklib.desktopviewer.persistance.repository.security;

import com.eklib.desktopviewer.persistance.model.companystructure.StructureType;
import com.eklib.desktopviewer.persistance.model.security.Role;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepositoryImpl;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class RoleRepositoryImpl extends BasePagingAndSortingRepositoryImpl<Role, Long> implements RoleRepository {

    @Override
    public Role getRoleByName(String roleName){
        Criteria criteria = getSession().createCriteria(Role.class);
        criteria.add(Restrictions.eq("name", roleName));
        return (Role)criteria.uniqueResult();
    }

    @Override
    public List<Role> getRolesForStructureType(Long structureId){
        Criteria criteria = getSession().createCriteria(Role.class);
        criteria.createAlias("structureType", "struct");
        criteria.add(Restrictions.eq("struct.id", structureId));
        return criteria.list();
    }
}
