package com.eklib.desktopviewer.persistance.repository.companystructure;

import com.eklib.desktopviewer.persistance.model.companystructure.CompanyEntity;
import com.eklib.desktopviewer.persistance.model.security.Role;
import com.eklib.desktopviewer.persistance.model.security.UserCompanyRole;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepositoryImpl;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by s.sheman on 02.11.2015.
 */
@Repository
public class UserCompanyRoleRepositoryImpl extends BasePagingAndSortingRepositoryImpl<UserCompanyRole, Long> implements UserCompanyRoleRepository {

    @Override
    public List<UserCompanyRole> findByProperties(UserEntity user, CompanyEntity company, Role role){
        Criteria criteria = getSession().createCriteria(UserCompanyRole.class);
        if(user != null){
            criteria.add(Restrictions.eq("user", user));
        }
        if(company != null){
            criteria.add(Restrictions.eq("company", company));
        }
        if(role != null){
            criteria.add(Restrictions.eq("role", role));
        }
        return criteria.list();
    }

    @Override
    public void deleteUserCompanyRolesForCompany(CompanyEntity company){
        Criteria criteria = getSession().createCriteria(UserCompanyRole.class);
        criteria.add(Restrictions.eq("company", company));
        List<UserCompanyRole> usrForDelete = criteria.list();
        for (UserCompanyRole usr : usrForDelete) {
            this.delete(usr);
        }
    }
}
