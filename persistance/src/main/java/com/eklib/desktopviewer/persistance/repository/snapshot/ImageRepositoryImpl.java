package com.eklib.desktopviewer.persistance.repository.snapshot;

import com.eklib.desktopviewer.persistance.model.enums.StorageTypeEnum;
import com.eklib.desktopviewer.persistance.model.snapshot.ImageEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepositoryImpl;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class ImageRepositoryImpl extends BasePagingAndSortingRepositoryImpl<ImageEntity, Long> implements ImageRepository {

    @Override
    public void markExpiredImagesAsDeleted(Date expire) {
        final String updateSQL =
                "update snapshot, image\n" +
                        "set snapshot.ID_IMAGE = null, image.DELETED = :expire \n" +
                        "where snapshot.ID_IMAGE = image.ID and image.EXPIRE < :expire ;";

        SQLQuery query = getSession().createSQLQuery(updateSQL);
        query.setDate("expire", expire);
        query.executeUpdate();
    }

    @Override
    public List<ImageEntity> findDeletedImagesByStorageType(StorageTypeEnum storageType, int count) {
        Criteria criteria = getSession().createCriteria(ImageEntity.class);
        criteria.add(Restrictions.eq("storageType", storageType));
        criteria.add(Restrictions.isNotNull("deleted"));
        criteria.setMaxResults(count);
        return criteria.list();
    }

    @Override
    public void deleteImages(List<ImageEntity> images) {
        final String deleteQuery =
                "delete from ImageEntity as image\n" +
                        "where image in :images";
        Query query = getSession().createQuery(deleteQuery).setParameterList("images", images);
        query.executeUpdate();
    }
}
