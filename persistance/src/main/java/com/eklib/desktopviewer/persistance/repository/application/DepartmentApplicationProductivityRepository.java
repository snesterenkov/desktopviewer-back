package com.eklib.desktopviewer.persistance.repository.application;

import com.eklib.desktopviewer.persistance.model.application.DepartmentApplicationProductivityEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepository;

import java.util.List;

/**
 * Created by Admin on 23.10.2015.
 */
public interface DepartmentApplicationProductivityRepository extends BasePagingAndSortingRepository<DepartmentApplicationProductivityEntity, Long> {

    List<DepartmentApplicationProductivityEntity> findForDepartment(Long departmentId);

    DepartmentApplicationProductivityEntity findByDepartmentAndApplication(Long departmentId, Long applicationId);

    DepartmentApplicationProductivityEntity findProductivityByApplicationNameAndProjectId(String applicationName, Long projectId);
}
