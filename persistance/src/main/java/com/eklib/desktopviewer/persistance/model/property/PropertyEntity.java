package com.eklib.desktopviewer.persistance.model.property;

import com.eklib.desktopviewer.persistance.model.LongGenericBaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "PROPERTY")
public class PropertyEntity extends LongGenericBaseEntity implements Serializable {

    @Column(name = "NAME",nullable = false, unique = true)
    private String name;

    @Column(name = "VALUE")
    private String value;

    public PropertyEntity() {
    }

    public PropertyEntity(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
