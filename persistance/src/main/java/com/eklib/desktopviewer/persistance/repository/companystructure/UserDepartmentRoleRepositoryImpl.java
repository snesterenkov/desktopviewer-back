package com.eklib.desktopviewer.persistance.repository.companystructure;

import com.eklib.desktopviewer.persistance.model.companystructure.DepartmentEntity;
import com.eklib.desktopviewer.persistance.model.security.Role;
import com.eklib.desktopviewer.persistance.model.security.UserDepartmentRole;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepositoryImpl;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by s.sheman on 23.10.2015.
 */
@Repository
@Transactional
public class UserDepartmentRoleRepositoryImpl extends BasePagingAndSortingRepositoryImpl<UserDepartmentRole, Long> implements UserDepartmentRoleRepository {

    @Override
    public List<UserDepartmentRole> findByProperties(UserEntity user, DepartmentEntity department, Role role){
        Criteria criteria = getSession().createCriteria(UserDepartmentRole.class);
        if(user != null){
            criteria.add(Restrictions.eq("user", user));
        }
        if(department != null){
            criteria.add(Restrictions.eq("department", department));
        }
        if(role != null){
            criteria.add(Restrictions.eq("role", role));
        }
        return criteria.list();
    }

    @Override
    public void deleteUserDepartmentRolesForDepartment(DepartmentEntity department){
        Criteria criteria = getSession().createCriteria(UserDepartmentRole.class);
        criteria.add(Restrictions.eq("department", department));
        List<UserDepartmentRole> usrForDelete = criteria.list();
        for (UserDepartmentRole usr : usrForDelete) {
            this.delete(usr);
        }
    }
}
