package com.eklib.desktopviewer.persistance.repository.companystructure;

import com.eklib.desktopviewer.persistance.model.companystructure.ProjectEntity;
import com.eklib.desktopviewer.persistance.model.enums.StatusEnum;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.model.security.UserProjectRole;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepositoryImpl;
import com.eklib.desktopviewer.persistance.repository.security.RoleRepository;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@Transactional
public class ProjectRepositoryImpl extends BasePagingAndSortingRepositoryImpl<ProjectEntity, Long> implements ProjectRepository {

    @Override
    public ProjectEntity getProjectByDepartmentAndName(Long departmentId, String name) {
        Criteria criteria = getSession().createCriteria(ProjectEntity.class);
        criteria.add(Restrictions.eq("name", name));
        criteria.add(Restrictions.eq("department.id", departmentId));
        return (ProjectEntity) criteria.uniqueResult();
    }

    @Override
    public boolean changeStatus(ProjectEntity project, StatusEnum newStatus) {
        if (!project.getStatus().equals(newStatus)) {
            project.setStatus(newStatus);
            update(project);
            return true;
        }
        return false;
    }

    @Override
    public List<ProjectEntity> findByOwner(String client) {
        Criteria criteria = getSession().createCriteria(ProjectEntity.class);
        criteria.createAlias("owner", "ow", JoinType.LEFT_OUTER_JOIN);
        criteria.add(Restrictions.or(Restrictions.eq("ow.login", client), Restrictions.eq("ow.email", client)));
        return criteria.list();
    }

    @Override
    public List<ProjectEntity> findForUser(String client) {
        Criteria criteria = getSession().createCriteria(ProjectEntity.class);
        criteria.createAlias("userProjectRoles", "roles", JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias("roles.user", "user");
        criteria.add(Restrictions.or(Restrictions.eq("user.login", client), Restrictions.eq("user.email", client)));
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    @Override
    public Collection<ProjectEntity> findByProjectAdmin(String login) {
        Criteria criteria = getCriteriaForProject();
        criteria.createAlias("userProjectRoles", "roles", JoinType.INNER_JOIN);
        criteria.createAlias("roles.role", "role");
        criteria.createAlias("roles.user", "user");

        criteria.add(Restrictions.eq("user.login", login));
        criteria.add(Restrictions.like("role.name", "%admin%", MatchMode.ANYWHERE));

        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    @Override
    public Collection<ProjectEntity> findByDepartmentAdmin(String login) {
        Criteria criteria = getCriteriaForProject();
        criteria.createCriteria("department", JoinType.INNER_JOIN)
                .createAlias("userDepartmentRoles", "roles", JoinType.INNER_JOIN)
                .createAlias("roles.role", "role")
                .createAlias("roles.user", "user")
                .add(Restrictions.eq("user.login", login))
                .add(Restrictions.like("role.name", "%admin%", MatchMode.ANYWHERE));
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    @Override
    public Collection<ProjectEntity> findByCompanyAdmin(String login) {
        Criteria criteria = getCriteriaForProject();
        criteria.createCriteria("department", JoinType.INNER_JOIN).createCriteria("company", JoinType.INNER_JOIN)
                .createAlias("userCompanyRoles", "roles", JoinType.INNER_JOIN)
                .createAlias("roles.role", "role")
                .createAlias("roles.user", "user")
                .add(Restrictions.eq("user.login", login))
                .add(Restrictions.like("role.name", "%admin", MatchMode.ANYWHERE));
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    private Criteria getCriteriaForProject() {
        Criteria criteria = getSession().createCriteria(ProjectEntity.class);
        criteria.setFetchMode("department", FetchMode.JOIN);
        criteria.setFetchMode("department.company", FetchMode.JOIN);

        return criteria;
    }

    @Override
    public boolean isProjectAdmin(ProjectEntity project, UserEntity user) {
        if (project.getOwner().equals(user)) {
            return true;
        }
//        List<UserEntity> usersWhichCanAdminProject = project.getUserProjectRoles().stream()
//                .filter(upr -> RoleRepository.ADMIN_PROJECT_ROLE_NAME.equals(upr.getRole().getName()))
//                .map(UserProjectRole::getUser).collect(Collectors.toList());
        return project.getUserProjectRoles().stream()
                .anyMatch(upr -> upr.getUser().equals(user) && RoleRepository.ADMIN_PROJECT_ROLE_NAME.equals(upr.getRole().getName()));
    }
}