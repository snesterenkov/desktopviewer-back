package com.eklib.desktopviewer.persistance.model.security;

import com.eklib.desktopviewer.persistance.model.LongGenericBaseEntity;
import com.eklib.desktopviewer.persistance.model.companystructure.DepartmentEntity;

import javax.persistence.*;

/**
 * Created by s.sheman on 23.10.2015.
 */
@Entity
@Table(name = "USER_DEPARTMENT_ROLE")
public class UserDepartmentRole extends LongGenericBaseEntity {

    @JoinColumn(name = "ID_USER", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private UserEntity user;

    @JoinColumn(name = "ID_DEPARTMENT", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private DepartmentEntity department;

    @JoinColumn(name = "ID_ROLE", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Role role;

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public DepartmentEntity getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentEntity department) {
        this.department = department;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
