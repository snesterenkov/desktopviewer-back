package com.eklib.desktopviewer.persistance.repository.companystructure;

import com.eklib.desktopviewer.persistance.model.companystructure.ProjectEntity;
import com.eklib.desktopviewer.persistance.model.security.Role;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.model.security.UserProjectRole;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepository;

import java.util.List;

/**
 * Created by s.sheman on 19.10.2015.
 * */

 public interface UserProjectRoleRepository extends BasePagingAndSortingRepository<UserProjectRole, Long> {
    List<UserProjectRole> findByProperties(UserEntity user, ProjectEntity project, Role role);

    void deleteUserProjectRolesForProject(ProjectEntity project);

    List<UserProjectRole> findUsersByProjects(List<ProjectEntity> projects);
}
