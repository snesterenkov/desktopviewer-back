package com.eklib.desktopviewer.persistance.repository.companystructure;

import com.eklib.desktopviewer.persistance.model.companystructure.ProjectEntity;
import com.eklib.desktopviewer.persistance.model.enums.StatusEnum;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepository;

import java.util.Collection;
import java.util.List;

public interface ProjectRepository extends BasePagingAndSortingRepository<ProjectEntity, Long> {

    ProjectEntity getProjectByDepartmentAndName(Long departmentId, String name);

    boolean changeStatus(ProjectEntity project, StatusEnum newStatus);

    List findByOwner(String client);

    List<ProjectEntity> findForUser(String client);

    Collection<ProjectEntity> findByProjectAdmin(String login);

    Collection<ProjectEntity> findByDepartmentAdmin(String login);

    Collection<ProjectEntity> findByCompanyAdmin(String login);

    boolean isProjectAdmin(ProjectEntity project, UserEntity user);
}
