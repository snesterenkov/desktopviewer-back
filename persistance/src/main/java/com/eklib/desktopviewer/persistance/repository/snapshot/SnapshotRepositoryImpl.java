package com.eklib.desktopviewer.persistance.repository.snapshot;

import com.eklib.desktopviewer.persistance.model.companystructure.ProjectEntity;
import com.eklib.desktopviewer.persistance.model.snapshot.SnapshotEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepositoryImpl;
import org.hibernate.Criteria;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Repository
@Transactional
public class SnapshotRepositoryImpl extends BasePagingAndSortingRepositoryImpl<SnapshotEntity, Long> implements SnapshotRepository {

    @Override
    public List<Date> getDatesWithSnapshotsByUser(Long userId) {
        Criteria criteria = getSession().createCriteria(SnapshotEntity.class);
        criteria.createAlias("user", "userAls", JoinType.INNER_JOIN);
        criteria.add(Restrictions.eq("userAls.id", userId));

        List<SnapshotEntity> allSnapshotsForUser = criteria.list();
        Set<Date> result = new HashSet<>();
        for(SnapshotEntity snapshot : allSnapshotsForUser){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(snapshot.getDate());
            calendar.set(Calendar.HOUR, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            result.add(calendar.getTime());
        }
        return new ArrayList<>(result);
    }

    @Override
    public SnapshotEntity findSnapshotByUserIdAndProjectIdAndPeriod(Long userId, Date startDate, Date endDate) {
        Criteria criteria = getSession().createCriteria(SnapshotEntity.class);
        criteria.createAlias("user", "ow", JoinType.INNER_JOIN);
        criteria.add(Restrictions.eq("ow.id", userId));
        Conjunction conj = Restrictions.conjunction();
        conj.add(Restrictions.ge("date", startDate));
        conj.add(Restrictions.le("date", endDate));
        criteria.add(conj);
        criteria.addOrder(Order.asc("date"));
        Iterator<SnapshotEntity> snapshotEntityIterator = criteria.list().iterator();
        if(snapshotEntityIterator.hasNext()) {
            return snapshotEntityIterator.next();
        } else {
            return new SnapshotEntity();
        }
    }

    @Override
    public List<SnapshotEntity> findByUserIdAndDates(Long userId, Date startDate, Date endDate) {
        Criteria criteria = getSession().createCriteria(SnapshotEntity.class);
        criteria.createAlias("user", "user", JoinType.INNER_JOIN);
        criteria.add(Restrictions.eq("user.id", userId));
        criteria.add(Restrictions.and(
                Restrictions.ge("date", startDate),
                Restrictions.le("date", endDate)
        ));
        criteria.addOrder(Order.asc("date"));

        return criteria.list();
    }

    @Override
    public Object[] findSnapshotsCountAndActivitySumForUsersByPeriodAndProjects(Long userId, List<ProjectEntity> projects, Date startDate, Date endDate) {
        Criteria criteria = getSession().createCriteria(SnapshotEntity.class);
        criteria.setProjection(Projections.projectionList().
                        add(Projections.count("id"))
                        .add(Projections.sqlProjection(
                                "sum(PRODUCTIVITY <> 'UNPRODUCTIVE') as productive",
                                new String[]{"productive"},
                                new Type[]{new StringType()}
                        ))
                        .add(Projections.sqlProjection(
                                "sum(if(PRODUCTIVITY <> 'UNPRODUCTIVE', if((COUNT_KEYBOARD_CLICK + COUNT_MOUSE_CLICK)/TIME_INTERVAL/2 > 1, 1, (COUNT_KEYBOARD_CLICK + COUNT_MOUSE_CLICK)/TIME_INTERVAL/2), 0)) as activitySum",
                                new String[]{"activitySum"},
                                new Type[]{new StringType()}
                        ))
        );
        criteria.createAlias("user", "owner", JoinType.INNER_JOIN);
        criteria.add(Restrictions.eq("owner.id", userId));
        criteria.add(Restrictions.in("project", projects));
        criteria.add(Restrictions.between("date", startDate, endDate));

        return (Object[])criteria.uniqueResult();
    }

    @Override
    public List<Object[]> getUsersStatsByCompanyAndDates(Long companyId, Date startDate, Date endDate) {
        Criteria criteria = getSession().createCriteria(SnapshotEntity.class);
        criteria.setProjection(Projections.projectionList()
                        .add(Projections.groupProperty("user.id"))
                        .add(Projections.min("date"))
                        .add(Projections.count("id"))
                        .add(Projections.sqlProjection(
                                "sum(PRODUCTIVITY <> 'UNPRODUCTIVE') as productive",
                                new String[]{"productive"},
                                new Type[]{new StringType()}
                        ))
                        .add(Projections.sqlProjection(
                                "sum(if(PRODUCTIVITY <> 'UNPRODUCTIVE', if((COUNT_KEYBOARD_CLICK + COUNT_MOUSE_CLICK)/TIME_INTERVAL/2 > 1, 1, (COUNT_KEYBOARD_CLICK + COUNT_MOUSE_CLICK)/TIME_INTERVAL/2), 0)) as activitySum",
                                new String[]{"activitySum"},
                                new Type[]{new StringType()}
                        ))
        );
        criteria.createAlias("project", "project");
        criteria.createAlias("project.department", "department");
        criteria.createAlias("department.company", "company");
        criteria.add(Restrictions.eq("company.id", companyId));
        criteria.add(Restrictions.between("date", startDate, endDate));

        return criteria.list();
    }

    @Override
    public List<Object[]> getUsersStatsForProjectsAndDates(Date startDate, Date endDate, List<Long> projectsIds) {
        Criteria criteria = getSession().createCriteria(SnapshotEntity.class);
        criteria.setProjection(Projections.projectionList()
                        .add(Projections.groupProperty("user.id"))
                        .add(Projections.min("date"))
                        .add(Projections.count("id"))
                        .add(Projections.sqlProjection(
                                "sum(PRODUCTIVITY <> 'UNPRODUCTIVE') as productive",
                                new String[]{"productive"},
                                new Type[]{new StringType()}
                        ))
                        .add(Projections.sqlProjection(
                                "sum(if(PRODUCTIVITY <> 'UNPRODUCTIVE', if((COUNT_KEYBOARD_CLICK + COUNT_MOUSE_CLICK)/TIME_INTERVAL/2 > 1, 1, (COUNT_KEYBOARD_CLICK + COUNT_MOUSE_CLICK)/TIME_INTERVAL/2), 0)) as activitySum",
                                new String[]{"activitySum"},
                                new Type[]{new StringType()}
                        ))
        );
        criteria.createAlias("project", "project");
        criteria.add(Restrictions.in("project.id", projectsIds));
        criteria.add(Restrictions.between("date", startDate, endDate));

        return criteria.list();
    }
}
