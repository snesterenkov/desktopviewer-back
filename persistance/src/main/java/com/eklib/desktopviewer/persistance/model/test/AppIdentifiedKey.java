package com.eklib.desktopviewer.persistance.model.test;

import java.io.Serializable;

public interface AppIdentifiedKey extends Serializable {

    boolean isEmpty();
}
