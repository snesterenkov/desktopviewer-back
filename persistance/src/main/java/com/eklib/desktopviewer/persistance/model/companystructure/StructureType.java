package com.eklib.desktopviewer.persistance.model.companystructure;

import com.eklib.desktopviewer.persistance.model.LongGenericBaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by s.sheman on 15.10.2015.
 */
@Entity
@Table(name = "STRUCTURE_TYPE")
public class StructureType extends LongGenericBaseEntity implements Serializable {
    public enum Values {
        COMPANY(1L),
        DEPARTMENT(2L),
        PROJECT(3L),
        APPLICATION(4L);

        private Long id;

        Values(final Long id){
            this.id = id;
        }

        public Long getId(){
            return this.id;
        }
    }

    @Column(name = "TYPE", nullable = false)
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object obj) {
        if( !(obj instanceof StructureType)){
            return false;
        }
        return getId().equals(((StructureType)obj).getId());
    }
}
