package com.eklib.desktopviewer.persistance.model.security;

import com.eklib.desktopviewer.persistance.model.LongGenericBaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Лол on 09.10.2015.
 */
@Entity
@Table(name = "Permission")
public class PermissionEntity extends LongGenericBaseEntity  implements Serializable{

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "CODE", nullable = false)
    private String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
