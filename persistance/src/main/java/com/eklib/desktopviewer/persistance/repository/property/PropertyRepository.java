package com.eklib.desktopviewer.persistance.repository.property;

import com.eklib.desktopviewer.persistance.model.property.PropertyEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepository;

public interface PropertyRepository extends BasePagingAndSortingRepository<PropertyEntity, Long> {

    PropertyEntity findByName(String name);
}
