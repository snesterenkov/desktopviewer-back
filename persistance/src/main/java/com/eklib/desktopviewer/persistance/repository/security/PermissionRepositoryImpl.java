package com.eklib.desktopviewer.persistance.repository.security;

import com.eklib.desktopviewer.persistance.model.security.PermissionEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepositoryImpl;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Created by Лол on 09.10.2015.
 */
@Repository
@Transactional
public class PermissionRepositoryImpl extends BasePagingAndSortingRepositoryImpl<PermissionEntity, Long> implements PermissionRepository {

    @Override
    public PermissionEntity getPermissionByName(String name) {
        Criteria criteria = getSession().createCriteria(PermissionEntity.class);
        criteria.add(Restrictions.eq("name", name));
        return (PermissionEntity) criteria.uniqueResult();
    }

    @Override
    public PermissionEntity getPermissionByCode(String code) {
        Criteria criteria = getSession().createCriteria(PermissionEntity.class);
        criteria.add(Restrictions.eq("code", code));
        return (PermissionEntity) criteria.uniqueResult();
    }
}
