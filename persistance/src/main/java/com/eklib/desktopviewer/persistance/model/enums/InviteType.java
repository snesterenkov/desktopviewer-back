package com.eklib.desktopviewer.persistance.model.enums;


public enum InviteType {

    COMPANY,
    DEPARTMENT,
    PROJECT
}
