package com.eklib.desktopviewer.persistance.repository.security;

import com.eklib.desktopviewer.persistance.model.security.PermissionEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepository;


/**
 * Created by Лол on 09.10.2015.
 */

public interface PermissionRepository  extends BasePagingAndSortingRepository<PermissionEntity, Long> {

    PermissionEntity getPermissionByName(String name);

    PermissionEntity getPermissionByCode(String code);
}
