package com.eklib.desktopviewer.persistance.repository.security;


import com.eklib.desktopviewer.persistance.model.security.InviteEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepositoryImpl;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public class InviteRepositoryImpl extends BasePagingAndSortingRepositoryImpl<InviteEntity, Long> implements InviteRepository{

    @Override
    public InviteEntity getByToken(String token) {
        Criteria criteria = getSession().createCriteria(InviteEntity.class);
        criteria.add(Restrictions.eq("token", token));
        return (InviteEntity)criteria.uniqueResult();
    }

    @Override
    public long getInvitesCountForEmail(String email) {
        Criteria criteria = getSession().createCriteria(InviteEntity.class);
        criteria.add(Restrictions.eq("email", email));
        criteria.setProjection(Projections.count("id"));
        return (long)criteria.uniqueResult();
    }
}
