package com.eklib.desktopviewer.persistance.model.security;


import com.eklib.desktopviewer.persistance.model.LongGenericBaseEntity;
import com.eklib.desktopviewer.persistance.model.companystructure.CompanyEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * Created by vadim on 17.09.2014.
 */
@Entity
@Table(name = "USER")
public class UserEntity extends LongGenericBaseEntity implements Serializable {

    @Column(name = "FIRST_NAME", nullable = false)
    private String firstName;

    @Column(name = "LAST_NAME", nullable = false)
    private String lastName;

    @Column(name = "LOGIN", unique = true, nullable = false)
    private String login;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "ROLES")
    private String roles;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "CHANGE_PASSWORD_TOKEN")
    private String changePasswordToken;

    @Column(name = "CONFIRM_USER_TOKEN")
    private String confirmUserToken;

    @Column
    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
    private List<CompanyEntity> ownerCompanies = new ArrayList<CompanyEntity>();

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_PRINCIPAL_ROLE")
    private Role principalRole;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<UserProjectRole> userProjectRoles = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<UserDepartmentRole> userDepartmentRoles = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<UserCompanyRole> userCompanyRoles = new HashSet<>();

    private static final String ROLE_SEPARATOR = ",";

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    /**
     * This methods computes the pack list from internal string representation.
     * A 'common' getRoles is not used since it may be tricky because there is no way to deal with the returned set.
     *
     * @return an empty set if internal pack is null or empty
     */
    public Set<RoleEntity> readRoles() {
        if (this.roles == null || this.roles.isEmpty()) {
            return new HashSet<RoleEntity>();
        } else {
            Set<RoleEntity> retVal = new HashSet<RoleEntity>();
            for (String roleName : this.roles.split(ROLE_SEPARATOR)) {
                retVal.add(RoleEntity.valueOf(roleName.trim()));
            }
            return retVal;
        }
    }

    /**
     * Used to re-write a set of roles into the internal roleStringList representation (used for persistance).
     *
     * @param roles
     */
    public void writeRoles(final Set<RoleEntity> roles) {
        if (roles == null || roles.isEmpty()) {
            this.roles = null;
        } else {
            StringBuilder sb = new StringBuilder();
            Iterator<RoleEntity> it = roles.iterator();
            while (it.hasNext()) {
                RoleEntity role = it.next();
                sb.append(role.name());
                if (it.hasNext()) {
                    sb.append(ROLE_SEPARATOR);
                }
            }
            this.roles = sb.toString();
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getConfirmUserToken() {
        return confirmUserToken;
    }

    public void setConfirmUserToken(String confirmUserToken) {
        this.confirmUserToken = confirmUserToken;
    }

    public List<CompanyEntity> getOwnerCompanies() {
        return ownerCompanies;
    }

    public void setOwnerCompanies(List<CompanyEntity> ownerCompanies) {
        this.ownerCompanies = ownerCompanies;
    }

    public String getChangePasswordToken() {
        return changePasswordToken;
    }

    public void setChangePasswordToken(String changePasswordToken) {
        this.changePasswordToken = changePasswordToken;
    }

    public Role getPrincipalRole() {
        return principalRole;
    }

    public void setPrincipalRole(Role principalRole) {
        this.principalRole = principalRole;
    }

    public Set<UserProjectRole> getUserProjectRoles() {
        return userProjectRoles;
    }

    public void setUserProjectRoles(Set<UserProjectRole> userProjectRoles) {
        this.userProjectRoles = userProjectRoles;
    }

    public Set<UserDepartmentRole> getUserDepartmentRoles() {
        return userDepartmentRoles;
    }

    public void setUserDepartmentRoles(Set<UserDepartmentRole> userDepartmentRoles) {
        this.userDepartmentRoles = userDepartmentRoles;
    }

    public Set<UserCompanyRole> getUserCompanyRoles() {
        return userCompanyRoles;
    }

    public void setUserCompanyRoles(Set<UserCompanyRole> userCompanyRoles) {
        this.userCompanyRoles = userCompanyRoles;
    }
}

