package com.eklib.desktopviewer.persistance.repository.application;

import com.eklib.desktopviewer.persistance.model.application.DepartmentApplicationProductivityEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepositoryImpl;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Admin on 23.10.2015.
 */
@Repository
@Transactional
public class DepartmentApplicationProductivityRepositoryImpl extends BasePagingAndSortingRepositoryImpl<DepartmentApplicationProductivityEntity, Long>
        implements DepartmentApplicationProductivityRepository {

    @Override
    public List<DepartmentApplicationProductivityEntity> findForDepartment(Long departmentId) {
        Criteria criteria = getSession().createCriteria(DepartmentApplicationProductivityEntity.class);
        criteria.add(Restrictions.eq("department.id", departmentId));
        return criteria.list();
    }

    @Override
    public DepartmentApplicationProductivityEntity findByDepartmentAndApplication(Long departmentId, Long applicationId) {
        Criteria criteria = getSession().createCriteria(DepartmentApplicationProductivityEntity.class);
        criteria.add(Restrictions.and(
                Restrictions.eq("department.id", departmentId),
                Restrictions.eq("application.id", applicationId)
        ));
        return (DepartmentApplicationProductivityEntity)criteria.uniqueResult();
    }

    @Override
    public DepartmentApplicationProductivityEntity findProductivityByApplicationNameAndProjectId(String applicationName, Long projectId) {
        Criteria criteria = getSession().createCriteria(DepartmentApplicationProductivityEntity.class);
        criteria.createAlias("department.projects", "projects", JoinType.INNER_JOIN);
        criteria.add(Restrictions.eq("projects.id", projectId));
        criteria.createCriteria("application").add(Restrictions.sqlRestriction("locate({alias}.NAME, ?)", applicationName, StringType.INSTANCE));
        return (DepartmentApplicationProductivityEntity)criteria.uniqueResult();
    }
}
