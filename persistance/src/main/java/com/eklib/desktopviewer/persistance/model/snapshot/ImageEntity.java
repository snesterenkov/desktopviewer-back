package com.eklib.desktopviewer.persistance.model.snapshot;

import com.eklib.desktopviewer.persistance.model.LongGenericBaseEntity;
import com.eklib.desktopviewer.persistance.model.enums.StorageTypeEnum;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "IMAGE")
public class ImageEntity extends LongGenericBaseEntity {

    @Column(name = "EXPIRE", nullable = false)
    private Date expire;

    @Enumerated(EnumType.STRING)
    @Column(name = "STORAGE_TYPE")
    private StorageTypeEnum storageType;

    @Column(name = "VIEW_LINK")
    private String viewLink;

    @Column(name = "THUMB_LINK")
    private String thumbLink;

    @Column(name = "DELETED")
    private Date deleted;

    public Date getExpire() {
        return expire;
    }

    public void setExpire(Date expire) {
        this.expire = expire;
    }

    public StorageTypeEnum getStorageType() {
        return storageType;
    }

    public void setStorageType(StorageTypeEnum storageType) {
        this.storageType = storageType;
    }

    public String getViewLink() {
        return viewLink;
    }

    public void setViewLink(String viewLink) {
        this.viewLink = viewLink;
    }

    public String getThumbLink() {
        return thumbLink;
    }

    public void setThumbLink(String thumbLink) {
        this.thumbLink = thumbLink;
    }

    public Date getDeleted() {
        return deleted;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }
}
