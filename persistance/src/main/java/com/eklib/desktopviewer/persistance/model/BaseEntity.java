package com.eklib.desktopviewer.persistance.model;

import java.io.Serializable;

/**
 * Created by s.sheman on 19.10.2015.
 */
public interface BaseEntity<E extends Serializable> extends Serializable {

    E getId();

    void setId(E id);
}
