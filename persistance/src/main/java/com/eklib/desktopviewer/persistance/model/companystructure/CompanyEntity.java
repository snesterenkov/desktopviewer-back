package com.eklib.desktopviewer.persistance.model.companystructure;

import com.eklib.desktopviewer.persistance.model.LongGenericBaseEntity;
import com.eklib.desktopviewer.persistance.model.enums.StatusEnum;
import com.eklib.desktopviewer.persistance.model.security.UserCompanyRole;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "CLIENT_COMPANY")
public class CompanyEntity extends LongGenericBaseEntity implements Serializable {

    @Column(name = "NAME")
    private String name;

    @Column(name = "WORKDAY_START")
    private Date workdayStart;

    @Column(name = "WORKDAY_LENGTH")
    private Integer workdayLength;

    @Column(name = "WORKDAY_DELAY")
    private Integer workdayDelay;

    @Column(name = "SCREENSHOTS_SAVING_MONTH")
    private Integer screenshotsSavingMonth;
    @Column
    @OneToMany(mappedBy = "company", fetch = FetchType.LAZY,
            targetEntity = DepartmentEntity.class, cascade = CascadeType.ALL)
    private Set<DepartmentEntity> departments = new HashSet<>();

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private StatusEnum status = StatusEnum.OPEN;

    @JoinColumn(name = "ID_OWNER", nullable = false)
    @ManyToOne(targetEntity = UserEntity.class, fetch = FetchType.LAZY)
    private UserEntity owner;

    @Column
    @OneToMany(mappedBy = "company", fetch = FetchType.LAZY, orphanRemoval = true,
            targetEntity = UserCompanyRole.class, cascade = CascadeType.ALL)
    private Set<UserCompanyRole> userCompanyRoles = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getWorkdayStart() {
        return workdayStart;
    }

    public void setWorkdayStart(Date workdayStart) {
        this.workdayStart = workdayStart;
    }

    public Integer getWorkdayLength() {
        return workdayLength;
    }

    public void setWorkdayLength(Integer workdayLength) {
        this.workdayLength = workdayLength;
    }

    public Integer getWorkdayDelay() {
        return workdayDelay;
    }

    public void setWorkdayDelay(Integer workdayDelay) {
        this.workdayDelay = workdayDelay;
    }

    public Integer getScreenshotsSavingMonth() {
        return screenshotsSavingMonth;
    }

    public void setScreenshotsSavingMonth(Integer screenshotsSavingMonth) {
        this.screenshotsSavingMonth = screenshotsSavingMonth;
    }

    public Set<DepartmentEntity> getDepartments() {
        return departments;
    }

    public void setDepartments(Set<DepartmentEntity> departments) {
        this.departments = departments;
    }

    public UserEntity getOwner() {
        return owner;
    }

    public void setOwner(UserEntity owner) {
        this.owner = owner;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public Set<UserCompanyRole> getUserCompanyRoles() {
        return userCompanyRoles;
    }

    public void setUserCompanyRoles(Set<UserCompanyRole> userCompanyRoles) {
        this.userCompanyRoles = userCompanyRoles;
    }
}
