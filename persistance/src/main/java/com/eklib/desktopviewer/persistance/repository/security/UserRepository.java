package com.eklib.desktopviewer.persistance.repository.security;

import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepository;

import java.util.List;
import java.util.Set;


public interface UserRepository extends BasePagingAndSortingRepository<UserEntity, Long> {

    UserEntity getUserByName(String name);

    UserEntity getUserByToken(String token);

    UserEntity getUserByConfirmationToken(String token);

    List<UserEntity> findFreeUsers(Long projectId);

    List<UserEntity> findFreeProjectUsers(Long projectId, Long companyId);

    List<UserEntity> findFreeUsersForDepartment(Long departmentId);

    List<UserEntity> findFreeUsersForCompany(Long companyId);

    List<UserEntity> findUsersForCompany(Long companyId);

    boolean isSuperUser(UserEntity user);
}
