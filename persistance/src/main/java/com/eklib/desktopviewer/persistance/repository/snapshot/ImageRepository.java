package com.eklib.desktopviewer.persistance.repository.snapshot;

import com.eklib.desktopviewer.persistance.model.enums.StorageTypeEnum;
import com.eklib.desktopviewer.persistance.model.snapshot.ImageEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepository;

import java.util.Date;
import java.util.List;


public interface ImageRepository extends BasePagingAndSortingRepository<ImageEntity, Long> {

    void markExpiredImagesAsDeleted(Date expire);

    List<ImageEntity> findDeletedImagesByStorageType(StorageTypeEnum storageType, int count);

    void deleteImages(List<ImageEntity> images);
}
