package com.eklib.desktopviewer.persistance.repository.companystructure;

import com.eklib.desktopviewer.persistance.model.companystructure.CompanyEntity;
import com.eklib.desktopviewer.persistance.model.enums.StatusEnum;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepository;

public interface CompanyRepository extends BasePagingAndSortingRepository<CompanyEntity, Long> {

    CompanyEntity getCompanyByName(String name);

    boolean changeStatus(CompanyEntity companyEntity, StatusEnum status);

    boolean isCompanyAdmin(CompanyEntity company, UserEntity user);

    int getMaxSavingPeriod();
}
