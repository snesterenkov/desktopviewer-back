package com.eklib.desktopviewer.persistance.model.application;

import com.eklib.desktopviewer.persistance.model.LongGenericBaseEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Admin on 20.10.2015.
 */
@Entity
@Table(name = "APPLICATION")
public class ApplicationEntity extends LongGenericBaseEntity implements Serializable {

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column
    @OneToMany(mappedBy = "application", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<DepartmentApplicationProductivityEntity> productivities;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DepartmentApplicationProductivityEntity> getProductivities() {
        return productivities;
    }

    public void setProductivities(List<DepartmentApplicationProductivityEntity> productivities) {
        this.productivities = productivities;
    }
}
