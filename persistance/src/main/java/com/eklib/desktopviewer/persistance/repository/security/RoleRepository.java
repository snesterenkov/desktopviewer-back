package com.eklib.desktopviewer.persistance.repository.security;

import com.eklib.desktopviewer.persistance.model.security.Role;
import com.eklib.desktopviewer.persistance.repository.BasePagingAndSortingRepository;

import java.util.List;

public interface RoleRepository extends BasePagingAndSortingRepository<Role, Long> {

    String ADMIN_APPLICATION = "ADMIN_APPLICATION";
    String ADMIN_COMPANY_ROLE_NAME = "ADMIN_COMPANY";
    String ADMIN_DEPARTMENT_ROLE_NAME = "ADMIN_DEPARTMENT";
    String ADMIN_PROJECT_ROLE_NAME = "ADMIN_PROJECT";

    Role getRoleByName(String name);

    List<Role> getRolesForStructureType(Long structureId);
}
