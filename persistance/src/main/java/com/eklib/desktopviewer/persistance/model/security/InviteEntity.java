package com.eklib.desktopviewer.persistance.model.security;

import com.eklib.desktopviewer.persistance.model.LongGenericBaseEntity;
import com.eklib.desktopviewer.persistance.model.enums.InviteType;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "INVITE")
public class InviteEntity extends LongGenericBaseEntity{

    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE", nullable = false)
    private InviteType type;
    @Column(name = "ID_STRUCTURE", nullable = false)
    private Long structureId;
    @Column(name = "ID_ROLE", nullable = false)
    private Long roleId;
    @Column(name = "EMAIL", nullable = false)
    private String email;
    @Column(name = "EXPIRE", nullable = false)
    private Date expire;
    @Column(name = "TOKEN", nullable = false)
    private String token;

    public InviteType getType() {
        return type;
    }

    public void setType(InviteType type) {
        this.type = type;
    }

    public Long getStructureId() {
        return structureId;
    }

    public void setStructureId(Long structureId) {
        this.structureId = structureId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getExpire() {
        return expire;
    }

    public void setExpire(Date expire) {
        this.expire = expire;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
