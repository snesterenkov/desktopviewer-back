package com.eklib.desktopviewer.persistance.model.application;

import com.eklib.desktopviewer.persistance.model.LongGenericBaseEntity;
import com.eklib.desktopviewer.persistance.model.companystructure.DepartmentEntity;
import com.eklib.desktopviewer.persistance.model.enums.ProductivityEnum;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Admin on 23.10.2015.
 */
@Entity
@Table(name = "DEPARTMENT_APPLICATION_PRODUCTIVITY", uniqueConstraints =
    @UniqueConstraint(columnNames = {"ID_DEPARTMENT","ID_APPLICATION"})
)
public class DepartmentApplicationProductivityEntity extends LongGenericBaseEntity implements Serializable {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_DEPARTMENT", nullable = false)
    private DepartmentEntity department;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_APPLICATION", nullable = false)
    private ApplicationEntity application;

    @Enumerated(EnumType.STRING)
    @Column(name = "PRODUCTIVITY", nullable = false)
    private ProductivityEnum productivity;

    public DepartmentEntity getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentEntity department) {
        this.department = department;
    }

    public ApplicationEntity getApplication() {
        return application;
    }

    public void setApplication(ApplicationEntity application) {
        this.application = application;
    }

    public ProductivityEnum getProductivity() {
        return productivity;
    }

    public void setProductivity(ProductivityEnum productivity) {
        this.productivity = productivity;
    }
}
