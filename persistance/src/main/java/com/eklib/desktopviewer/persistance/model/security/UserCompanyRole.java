package com.eklib.desktopviewer.persistance.model.security;

import com.eklib.desktopviewer.persistance.model.LongGenericBaseEntity;
import com.eklib.desktopviewer.persistance.model.companystructure.CompanyEntity;

import javax.persistence.*;

/**
 * Created by s.sheman on 29.10.2015.
 */
@Entity
@Table(name = "USER_COMPANY_ROLE")
public class UserCompanyRole extends LongGenericBaseEntity{
    @JoinColumn(name = "ID_USER", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private UserEntity user;

    @JoinColumn(name = "ID_COMPANY", nullable = false)
    @ManyToOne(targetEntity = CompanyEntity.class, fetch = FetchType.LAZY)
    private CompanyEntity company;

    @JoinColumn(name = "ID_ROLE", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Role role;

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public CompanyEntity getCompany() {
        return company;
    }

    public void setCompany(CompanyEntity company) {
        this.company = company;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
