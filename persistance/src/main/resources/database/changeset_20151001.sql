ALTER TABLE `CLIENT_COMPANY` ADD `WORKDAY_START` TIMESTAMP NOT NULL COMMENT 'Workday start time';
ALTER TABLE `CLIENT_COMPANY` ADD `WORKDAY_LENGTH` INT NOT NULL COMMENT 'Workday length in minutes';