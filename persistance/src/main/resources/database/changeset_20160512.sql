CREATE TABLE `desktopview`.`image` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `EXPIRE` DATE NOT NULL,
  `STORAGE_TYPE` VARCHAR(128) NOT NULL,
  `VIEW_LINK` VARCHAR(2000) NULL,
  `THUMB_LINK` VARCHAR(2000) NULL,
  `DELETED` DATE NULL,
  PRIMARY KEY (`ID`));

ALTER TABLE `desktopview`.`snapshot`
  DROP COLUMN `RESIZED_FILE_NAME`,
  DROP COLUMN `ORIGINAL_FILE_NAME`;

ALTER TABLE `desktopview`.`snapshot`
  DROP FOREIGN KEY `snapshot_project_fk02`;

ALTER TABLE `desktopview`.`snapshot`
  CHANGE COLUMN `DATE` `DATE` TIMESTAMP NOT NULL COMMENT 'Snapshot date' ,
  CHANGE COLUMN `ID_PROJECT` `ID_PROJECT` INT(11) NOT NULL COMMENT 'Snapshot project' ;

ALTER TABLE `desktopview`.`snapshot`
  ADD CONSTRAINT `snapshot_project_fk02`
    FOREIGN KEY (`ID_PROJECT`)
    REFERENCES `desktopview`.`project` (`ID`);

ALTER TABLE `desktopview`.`snapshot`
  CHANGE COLUMN `DATE` `DATE` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Snapshot date' ;

ALTER TABLE `desktopview`.`snapshot`
  ADD COLUMN `ID_IMAGE` INT NULL AFTER `PRODUCTIVITY`;

ALTER TABLE `desktopview`.`snapshot`
  ADD INDEX `snapshot_image_fk1_idx` (`ID_IMAGE` ASC);

ALTER TABLE `desktopview`.`snapshot`
  ADD CONSTRAINT `snapshot_image_fk1`
    FOREIGN KEY (`ID_IMAGE`)
    REFERENCES `desktopview`.`image` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `desktopview`.`snapshot`
CHANGE COLUMN `NOTE` `NOTE` VARCHAR(2000) NOT NULL COMMENT 'Snapshot note' ;
