CREATE TABLE IF NOT EXISTS `APPLICATION`(
  `ID` int(11) not null auto_increment comment 'ID application',
  `NAME` varchar(128) not null comment 'Name application',
  PRIMARY KEY (`ID`),
  constraint application_uk1 UNIQUE(`NAME`)
)  ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='Table application';

