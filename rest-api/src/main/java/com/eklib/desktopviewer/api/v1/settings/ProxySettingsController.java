package com.eklib.desktopviewer.api.v1.settings;

import com.eklib.desktopviewer.dto.settings.network.ProxySettingsDTO;
import com.eklib.desktopviewer.services.settings.network.ProxySettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/settings/proxy")
public class ProxySettingsController {

    @Autowired
    private ProxySettingsService proxySettingsService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ProxySettingsDTO getProxySettings(@RequestParam(value = "client")String client) {
        return proxySettingsService.getProxySettings(client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void setProxySettings(@RequestBody ProxySettingsDTO proxySettings, @RequestParam(value = "client") String client) {
        proxySettingsService.setProxySettings(proxySettings, client);
    }
}
