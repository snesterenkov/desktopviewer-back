package com.eklib.desktopviewer.api.v1.exeption;

import com.eklib.desktopviewer.exception.client.DesktopviewerClientException;
import com.eklib.desktopviewer.exception.server.DesktopviewerServerException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class RestExceptionHandler {


    @ExceptionHandler(DesktopviewerServerException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorFormInfo handleServerExceptions(DesktopviewerServerException ex){
        String errorMessage = ex.getMessage();
        String code = String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value());
        return new ErrorFormInfo(code, errorMessage);
    }

    @ExceptionHandler(DesktopviewerClientException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorFormInfo handleClientExceptions(DesktopviewerClientException ex){
        String errorMessage = ex.getMessage();
        String code = String.valueOf(HttpStatus.BAD_REQUEST.value());
        return new ErrorFormInfo(code, errorMessage);
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(value=HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public ErrorFormInfo handleUnauthorizes(AccessDeniedException ex) {
        String errorMessage = ex.getMessage();
        String code = String.valueOf(HttpStatus.UNAUTHORIZED.value());
        return new ErrorFormInfo(code, errorMessage);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorFormInfo handleExeption(Exception ex) {
        String errorMessage = ex.getMessage();
        String code = String.valueOf(HttpStatus.BAD_REQUEST.value());
        return new ErrorFormInfo(code, errorMessage);
    }


}
