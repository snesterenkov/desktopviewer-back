package com.eklib.desktopviewer.api.v1.settings;

import com.eklib.desktopviewer.dto.settings.storage.GoogleDriveSettingsDTO;
import com.eklib.desktopviewer.services.settings.storage.GoogleDriveSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/settings/storage/googledrive")
public class GoogleDriveSettingsController {

    @Autowired
    private GoogleDriveSettingsService googleDriveSettingsService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public GoogleDriveSettingsDTO getGoogleDriveSettings(@RequestParam(value = "client") String client) {
        return googleDriveSettingsService.getGoogleDriveSettings(client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void setGoogleDriveSettings(@RequestBody GoogleDriveSettingsDTO newGoogleDriveSettings, @RequestParam(value = "client") String client) {
        googleDriveSettingsService.setGoogleDriveSettings(newGoogleDriveSettings, client);
    }
}
