package com.eklib.desktopviewer.api.v1.settings;

import com.eklib.desktopviewer.dto.settings.client.ClientSettingsDTO;
import com.eklib.desktopviewer.services.settings.client.ClientSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/settings/client")
public class ClientSettingsController {

    @Autowired
    private ClientSettingsService clientSettingsService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    public ClientSettingsDTO get(@RequestParam("client") String clientLogin){
        return clientSettingsService.get(clientLogin);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    public ClientSettingsDTO update(@RequestParam("client") String clientLogin,
                                    @RequestBody ClientSettingsDTO newSettings){
        return clientSettingsService.update(newSettings, clientLogin);
    }
}
