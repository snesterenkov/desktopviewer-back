package com.eklib.desktopviewer.api.v1.application;

import com.eklib.desktopviewer.dto.application.ApplicationDTO;
import com.eklib.desktopviewer.dto.enums.ProductivityDTO;
import com.eklib.desktopviewer.services.application.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 20.10.2015.
 */
@RestController
@RequestMapping("/application")
public class ApplicationController {

    @Autowired
    private ApplicationService applicationService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public ApplicationDTO createApplication(@RequestBody ApplicationDTO applicationDTO, @RequestParam(value = "client", required = false) String client){
        return applicationService.insert(applicationDTO, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ApplicationDTO updateApplication(@PathVariable("id") Long id, @RequestBody ApplicationDTO applicationDTO, @RequestParam(value = "client", required = false) String client){
        return applicationService.update(id, applicationDTO, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<ApplicationDTO> findAllApplications(@RequestParam(value = "client", required = false) String client){
        return new ArrayList<>(applicationService.findAll());
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{id}",method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ApplicationDTO findApplicationById(@PathVariable("id")Long id, @RequestParam(value = "client", required = false) String client){
        return applicationService.findById(id, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/department/{departmentId}", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Map<ProductivityDTO, List<ApplicationDTO>> findForDepartment(@PathVariable("departmentId") Long departmentId, @RequestParam(value = "client", required = false) String client){
        return applicationService.findForDepartment(departmentId, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/department/free/{departmentId}", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<ApplicationDTO> findDepartmentFree(@PathVariable("departmentId") Long departmentId, @RequestParam(value = "client", required = false) String client){
        return applicationService.findDepartmentFree(departmentId, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/delete", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void deleteApplications(@RequestParam(value = "client", required = false) String client, @RequestBody List<ApplicationDTO> applications) {
        applicationService.deleteApplications(applications, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/department/attach/{departmentId}", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void attachToDepartment(@PathVariable("departmentId") Long departmentId, @RequestParam (value = "client", required = false) String client, @RequestParam(value = "productivity") ProductivityDTO productivity ,@RequestBody List<ApplicationDTO> applications) {
        applicationService.attachToDepartment(departmentId, applications, productivity, client);
    }
}
