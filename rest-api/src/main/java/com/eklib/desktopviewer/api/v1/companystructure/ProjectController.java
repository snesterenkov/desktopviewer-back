package com.eklib.desktopviewer.api.v1.companystructure;

import com.eklib.desktopviewer.dto.companystructure.ProjectDTO;
import com.eklib.desktopviewer.dto.companystructure.ProjectDetailDTO;
import com.eklib.desktopviewer.dto.companystructure.ProjectExtendDTO;
import com.eklib.desktopviewer.dto.enums.StatusDTO;
import com.eklib.desktopviewer.services.companystructure.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/project")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public ProjectDetailDTO createProject(@RequestBody ProjectDTO projectDTO, @RequestParam(value = "client", required = false) String client){
        return projectService.insert(projectDTO, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<ProjectDetailDTO> findAllProjects(@RequestParam(value = "client", required = false) String client){
        return new ArrayList<>(projectService.findAll(client));
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/member", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<ProjectExtendDTO> findForUser(@RequestParam(value = "client", required = false) String client){
        return new ArrayList<>(projectService.findForUser(client));
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/dependent", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    public List<ProjectDetailDTO> findUserDependent(@RequestParam(value = "client", required = true)String client){
        return new ArrayList<>(projectService.findUserDependent(client));
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ProjectDetailDTO findById(@PathVariable("id") Long projectId, @RequestParam(value = "client", required = false) String client){
        return projectService.findById(projectId, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ProjectDTO updateProject(@PathVariable("id") Long projectId, @RequestBody ProjectDTO projectDTO, @RequestParam(value = "client", required = false) String client){
        return projectService.update(projectId, projectDTO, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ResponseBody
    public void deleteProject(@PathVariable("id") Long projectId,
                                 @RequestParam("client") String client) {
        projectService.delete(projectId, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/detailupdate/{id}", method = RequestMethod.PUT, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ProjectDetailDTO detailUpdateProject(@PathVariable("id") Long projectId,
                                                @RequestParam(value = "client", required = false) String client,
                                                @RequestBody ProjectDetailDTO projectDetailDTO){
        return projectService.detailUpdate(projectId, projectDetailDTO, client);
    }


    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/changestatus/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ProjectDetailDTO changeStatus(@PathVariable("id") Long id, @RequestParam(value = "client", required = false) String client, @RequestBody StatusDTO newStatus){
        return projectService.changeStatus(id,newStatus, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/removeUserProjectRole/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUserProjectRole(@PathVariable("id") Long userProjectRoleId,
                                      @RequestParam("client")String client){
        projectService.deleteUserProjectRole(userProjectRoleId, client);
    }
}
