package com.eklib.desktopviewer.api.v1.report;

import com.eklib.desktopviewer.dto.companystructure.CompanyExtendDTO;
import com.eklib.desktopviewer.dto.companystructure.ProjectListDTO;
import com.eklib.desktopviewer.dto.report.WorkDiaryDTO;
import com.eklib.desktopviewer.persistance.model.enums.PeriodEnum;
import com.eklib.desktopviewer.services.report.ReportService;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by alex on 4/1/2015.
 */
@RestController
@RequestMapping("/report")
public class ReportController {

    @Autowired
    private ReportService reportService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/workDiary", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<WorkDiaryDTO> getHoursByUserFromPeriod(
            @RequestBody ProjectListDTO projectDTOs,
            @RequestParam(value = "period") PeriodEnum period,
            @RequestParam(value = "startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
            @RequestParam(value = "endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate,
            @RequestParam(value = "client") String client) {
        return reportService.getWorkingHoursByTimePeriod(projectDTOs.getProjectDTOs(), period, startDate, endDate, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/admin/projects/list", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<CompanyExtendDTO> getProjectsHierarchyForAdmin(@RequestParam(value = "client", required = false) String client) {
        return new ArrayList<>(reportService.findAdminProjectsHierarchyByAdminLogin(client));
    }

}
