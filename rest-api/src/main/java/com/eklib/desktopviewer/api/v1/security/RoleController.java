package com.eklib.desktopviewer.api.v1.security;

import com.eklib.desktopviewer.dto.security.RoleDTO;
import com.eklib.desktopviewer.services.security.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by vadim on 29.10.2014.
 */
@RestController
@RequestMapping("/role")
public class RoleController {
    @Autowired
    private RoleService roleService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<RoleDTO> findAllCompanies(@RequestParam(value = "client", required = false) String client){
        return roleService.getAvailableRoles();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.POST, headers="Accept=application/json")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public RoleDTO createPermission(@RequestBody RoleDTO roleDTO){
        return roleService.createRole(roleDTO);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value= "/{id}", method = RequestMethod.DELETE, headers="Accept=application/json")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Long permissionId){
        roleService.delete(permissionId);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value= "/{id}", method = RequestMethod.PUT, headers="Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public RoleDTO update(@PathVariable("id") Long permissionId, @RequestBody RoleDTO roleDTO){
        return roleService.update(permissionId, roleDTO);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/rolesForStructureType/{structureId}", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<RoleDTO> getRolesForStructureType(@PathVariable("structureId") Long structureId){
        return roleService.getRolesForStructureType(structureId);
    }
}
