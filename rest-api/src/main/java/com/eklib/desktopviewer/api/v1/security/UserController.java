package com.eklib.desktopviewer.api.v1.security;

import com.eklib.desktopviewer.dto.security.UserDTO;
import com.eklib.desktopviewer.dto.security.UserDetailDTO;
import com.eklib.desktopviewer.services.security.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserServices userServices;

    @RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public UserDTO createUser(@RequestBody UserDetailDTO newUser) {
        return userServices.createUser(newUser);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<UserDTO> findAll(@RequestParam("client") String client) {
        return userServices.findAll(client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/free", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Set<UserDTO> findFree(@RequestParam(value = "projectid") Long projectId,
                                 @RequestParam("client") String client) {
        return userServices.findFreeUsers(projectId, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/freeUsersForDepartment", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<UserDTO> findFreeUsersForDepartment(@RequestParam(value = "departmentid") Long departmentId,
                                                    @RequestParam("client") String client) {
        return Collections.emptyList();//todo поменять логику
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/freeUsersForCompany", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<UserDTO> findFreeUsersForCompany(@RequestParam(value = "companyid") Long companyId,
                                                 @RequestParam("client") String client) {
        return Collections.emptyList();//todo поменять логику
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public UserDTO update(@PathVariable("id") Long userId,
                          @RequestParam("client") String client,
                          @RequestBody UserDTO updateUser) {
        return userServices.update(userId, updateUser, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{id}/admin", method = RequestMethod.PUT, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void makeAdmin(@PathVariable("id") Long id,
                          @RequestParam("client") String client) {
        userServices.makeAdmin(id, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{id}/nonadmin", method = RequestMethod.PUT, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void makeNonAdmin(@PathVariable("id") Long id,
                             @RequestParam("client") String client) {
        userServices.makeNonAdmin(id, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Long userId,
                       @RequestParam("client") String client) {
        userServices.delete(userId, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public UserDTO findById(@PathVariable("id") Long userId,
                            @RequestParam("client") String client) {
        return userServices.findById(userId, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/authorized", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public UserDetailDTO findByLogin(@RequestParam(value = "login") String login, @RequestParam(value = "client", required = false) String client) {
        if (login.equals(client)) {
            return userServices.getAutorizedUser(login);
        }
        throw new IllegalAccessError("You can not authorized to another user");
    }

    @RequestMapping(value = "/requestOnChangingPassword", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    public void restorePassword(@RequestParam(value = "email") String email) {
        userServices.requestOnChangingPassword(email);
    }

    @RequestMapping(value = "/changePassword/{token}", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    public void restorePassword(@RequestParam(value = "password") String password, @PathVariable("token") String token) {
        userServices.changePassword(password, token);
    }

    @RequestMapping(value = "/confirm/{token}", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    public void confirmUser(@PathVariable String token) {
        userServices.confirmUser(token);
    }
}
