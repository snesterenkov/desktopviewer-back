package com.eklib.desktopviewer.api.v1.settings;

import com.eklib.desktopviewer.dto.settings.storage.LocalStorageSettingsDTO;
import com.eklib.desktopviewer.services.settings.storage.LocalStorageSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/settings/storage/localstorage")
public class LocalStorageSettingsController {

    @Autowired
    private LocalStorageSettingsService localStorageSettingsService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public LocalStorageSettingsDTO getLocalStorageSettings(@RequestParam(value = "client") String client) {
        return localStorageSettingsService.getLocalStorageSettings(client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void setLocalStorageSettings(@RequestBody LocalStorageSettingsDTO newLocalStorageSettings, @RequestParam(value = "client") String client) {
        localStorageSettingsService.setLocalStorageSettings(newLocalStorageSettings, client);
    }
}
