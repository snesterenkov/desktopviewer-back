package com.eklib.desktopviewer.api.v1.settings;

import com.eklib.desktopviewer.dto.enums.StorageTypeDTO;
import com.eklib.desktopviewer.dto.settings.file.OutdatedFilesDeleteResultDTO;
import com.eklib.desktopviewer.dto.settings.file.StorageOutdatedFilesInfoDTO;
import com.eklib.desktopviewer.dto.settings.storage.StorageDTO;
import com.eklib.desktopviewer.services.settings.storage.StorageSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/settings/storage")
public class StorageController {

    @Autowired
    private StorageSettingsService storageSettingsService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<StorageDTO>  getAllAvailableStorage(@RequestParam(value = "client") String client) {
        return storageSettingsService.getAllAvailableStorage(client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public StorageDTO  setCurrentStorage(@RequestBody StorageDTO storage, @RequestParam(value = "client") String client) {
        return storageSettingsService.setCurrentStorage(storage, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/files/outdated", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<StorageOutdatedFilesInfoDTO> getInfoAboutOutdatedFiles(@RequestParam("client") String client){
        return storageSettingsService.getInfoAboutOutdatedFiles(client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{storage}/files/outdated", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public StorageOutdatedFilesInfoDTO getInfoAboutStorageOutdatedFiles(@PathVariable("storage")StorageTypeDTO storage,
                                                                        @RequestParam("client")String client){
        return storageSettingsService.getInfoAboutStorageOutdatedFiles(storage, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/files/outdated",method = RequestMethod.DELETE, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<OutdatedFilesDeleteResultDTO> deleteOutdatedFiles(@RequestParam("client") String client){
        return storageSettingsService.deleteOutdatedImages(client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{storage}/files/outdated", method = RequestMethod.DELETE, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public OutdatedFilesDeleteResultDTO deleteStorageOutdatedFiles(@PathVariable("storage") StorageTypeDTO storage,
                                                                   @RequestParam("client") String client){
        return storageSettingsService.deleteStorageOutdatedFiles(storage, client);
    }

}
