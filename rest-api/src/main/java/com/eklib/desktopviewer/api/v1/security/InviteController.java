package com.eklib.desktopviewer.api.v1.security;

import com.eklib.desktopviewer.dto.security.InviteDTO;
import com.eklib.desktopviewer.services.security.InviteServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/invite")
public class InviteController {

    @Autowired
    private InviteServices inviteServices;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public InviteDTO create(@RequestBody InviteDTO inviteDTO, @RequestParam("client") String clientLogin){
        return inviteServices.create(inviteDTO, clientLogin);
    }

    @RequestMapping(value = "/{token}", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public InviteDTO getByToken(@PathVariable String token){
        return inviteServices.getByToken(token);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{token}/confirm", method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public InviteDTO confirm(@PathVariable("token") String token, @RequestParam("client") String clientLogin){
        return inviteServices.confirm(token, clientLogin);
    }

}
