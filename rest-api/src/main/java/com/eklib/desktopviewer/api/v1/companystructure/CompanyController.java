package com.eklib.desktopviewer.api.v1.companystructure;

import com.eklib.desktopviewer.dto.companystructure.CompanyDTO;
import com.eklib.desktopviewer.dto.companystructure.CompanyDetailDTO;
import com.eklib.desktopviewer.dto.enums.StatusDTO;
import com.eklib.desktopviewer.services.companystructure.CompanyServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/company")
public class CompanyController {

    @Autowired
    private CompanyServices companyServices;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public CompanyDetailDTO createCompany(@RequestBody CompanyDTO company, @RequestParam(value = "client", required = false) String client) {
        return companyServices.insert(company, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public CompanyDetailDTO updateCompany(@PathVariable("id") Long id,
                                    @RequestBody CompanyDTO companyDetailDTO,
                                    @RequestParam(value = "client", required = false) String client) {
        return companyServices.update(id, companyDetailDTO, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ResponseBody
    public void deleteCompany(@PathVariable("id") Long companyId,
                              @RequestParam("client") String client) {
        companyServices.delete(companyId, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<CompanyDetailDTO> findAllCompanies(@RequestParam(value = "client", required = false) String client) {
        return new ArrayList<>(companyServices.findAll(client));
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/changestatus/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public CompanyDetailDTO changeStatus(@PathVariable("id") Long id, @RequestParam(value = "client", required = false) String client, @RequestBody StatusDTO newStatus) {
        return companyServices.changeStatus(id, newStatus, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public CompanyDetailDTO findCompanyById(@PathVariable("id") Long id, @RequestParam(value = "client", required = false) String client) {
        return companyServices.findById(id, client);

    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/open", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<CompanyDTO> findOpenCompanies(@RequestParam(value = "client", required = false) String client) {
        return new ArrayList<>(companyServices.findOpen(client));
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/removeUserCompanyRoles/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUserCompanyRoles(@PathVariable("id") Long userCompanyRoleId,
                                       @RequestParam("client") String client) {
        companyServices.deleteUserCompanyRole(userCompanyRoleId, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/detailupdate/{id}", method = RequestMethod.PUT, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public CompanyDetailDTO detailUpdateCompany(@PathVariable("id") Long companyId,
                                                @RequestParam(value = "client", required = false) String client,
                                                @RequestBody CompanyDetailDTO companyDetailDTO) {
        return companyServices.detailUpdate(companyId, companyDetailDTO, client);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/employee/{employeeId}", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<CompanyDTO> findCompaniesForEmployee(@PathVariable("employeeId") Long employeeId, @RequestParam(value = "client", required = false) String client) {
        return new ArrayList<>(companyServices.findForEmployee(employeeId, client));
    }
}
