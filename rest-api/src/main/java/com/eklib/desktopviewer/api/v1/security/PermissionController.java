package com.eklib.desktopviewer.api.v1.security;

import com.eklib.desktopviewer.dto.security.PermissionDTO;
import com.eklib.desktopviewer.services.security.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by s.sheman on 09.10.2015.
 */
@RestController
@RequestMapping("/permission")
public class PermissionController {

    @Autowired
    private PermissionService permissionService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<PermissionDTO> findAllCompanies(@RequestParam(value = "client", required = false) String client){
        return permissionService.getAvailablePermission();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(method = RequestMethod.POST, headers="Accept=application/json")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public PermissionDTO createPermission(@RequestBody PermissionDTO permissionDTO){
        return permissionService.createPermission(permissionDTO);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value= "/{id}", method = RequestMethod.DELETE, headers="Accept=application/json")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Long permissionId){
        permissionService.delete(permissionId);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value= "/{id}", method = RequestMethod.PUT, headers="Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public PermissionDTO update(@PathVariable("id") Long permissionId, @RequestBody PermissionDTO updatePermission){
        return permissionService.update(permissionId,updatePermission);
    }
}
