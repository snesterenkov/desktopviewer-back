package com.eklib.desktopviewer.dto.settings.network;

/**
 * Created by Admin on 15.12.2015.
 */
public class ProxyCredentialsDTO {

    private String username = "";

    private String password = "";

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
