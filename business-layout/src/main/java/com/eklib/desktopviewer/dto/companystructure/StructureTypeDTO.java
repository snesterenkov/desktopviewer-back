package com.eklib.desktopviewer.dto.companystructure;

import com.eklib.desktopviewer.dto.BaseDTO;

/**
 * Created by s.sheman on 15.10.2015.
 */
public class StructureTypeDTO extends BaseDTO{
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
