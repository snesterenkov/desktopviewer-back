package com.eklib.desktopviewer.dto.settings.storage;

public class GoogleDriveSettingsDTO {

    private String serviceAccountId;

    private String secretFileLocation;

    private String storageFolderName;

    private String tempFolderName;

    public String getServiceAccountId() {
        return serviceAccountId;
    }

    public void setServiceAccountId(String serviceAccountId) {
        this.serviceAccountId = serviceAccountId;
    }

    public String getSecretFileLocation() {
        return secretFileLocation;
    }

    public void setSecretFileLocation(String secretFileLocation) {
        this.secretFileLocation = secretFileLocation;
    }

    public String getStorageFolderName() {
        return storageFolderName;
    }

    public void setStorageFolderName(String storageFolderName) {
        this.storageFolderName = storageFolderName;
    }

    public String getTempFolderName() {
        return tempFolderName;
    }

    public void setTempFolderName(String tempFolderName) {
        this.tempFolderName = tempFolderName;
    }
}
