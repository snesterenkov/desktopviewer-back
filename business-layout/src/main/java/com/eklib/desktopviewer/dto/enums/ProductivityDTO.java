package com.eklib.desktopviewer.dto.enums;

/**
 * Created by Admin on 22.10.2015.
 */
public enum ProductivityDTO {
    PRODUCTIVE,
    NEUTRAL,
    UNPRODUCTIVE
}
