package com.eklib.desktopviewer.dto.security;

import com.eklib.desktopviewer.dto.enums.InviteTypeDTO;


public class InviteDTO {

    private InviteTypeDTO type;
    private Long structureId;
    private Long roleId;
    private String email;
    private UserDTO user;

    public InviteTypeDTO getType() {
        return type;
    }

    public void setType(InviteTypeDTO type) {
        this.type = type;
    }

    public Long getStructureId() {
        return structureId;
    }

    public void setStructureId(Long structureId) {
        this.structureId = structureId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }
}
