package com.eklib.desktopviewer.dto.enums;

/**
 * Created by Admin on 09.12.2015.
 */
public enum StorageTypeDTO {

    LocalStorage,
    GoogleDrive
}
