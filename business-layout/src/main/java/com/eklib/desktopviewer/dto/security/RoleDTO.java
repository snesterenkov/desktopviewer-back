package com.eklib.desktopviewer.dto.security;

import com.eklib.desktopviewer.dto.BaseDTO;
import com.eklib.desktopviewer.dto.companystructure.StructureTypeDTO;

import java.util.List;

/**
 * Created by s.sheman on 12.10.2015.
 */
public class RoleDTO extends BaseDTO {
    private String name;
    private String code;
    private StructureTypeDTO structureType;
    private List<PermissionDTO> permissions;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<PermissionDTO> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PermissionDTO> permissions) {
        this.permissions = permissions;
    }

    public StructureTypeDTO getStructureType() {
        return structureType;
    }

    public void setStructureType(StructureTypeDTO structureType) {
        this.structureType = structureType;
    }
}
