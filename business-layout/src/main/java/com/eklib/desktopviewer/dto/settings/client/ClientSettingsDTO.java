package com.eklib.desktopviewer.dto.settings.client;

public class ClientSettingsDTO {

    private String registrationConfirmURL = "";
    private String passwordRestoreURL = "";
    private String inviteConfirmURL = "";

    public ClientSettingsDTO() {
    }

    public ClientSettingsDTO(String registrationConfirmURL, String passwordRestoreURL, String inviteConfirmURL) {
        this.registrationConfirmURL = registrationConfirmURL;
        this.passwordRestoreURL = passwordRestoreURL;
        this.inviteConfirmURL = inviteConfirmURL;
    }

    public String getRegistrationConfirmURL() {
        return registrationConfirmURL;
    }

    public void setRegistrationConfirmURL(String registrationConfirmURL) {
        this.registrationConfirmURL = registrationConfirmURL;
    }

    public String getPasswordRestoreURL() {
        return passwordRestoreURL;
    }

    public void setPasswordRestoreURL(String passwordRestoreURL) {
        this.passwordRestoreURL = passwordRestoreURL;
    }

    public String getInviteConfirmURL() {
        return inviteConfirmURL;
    }

    public void setInviteConfirmURL(String inviteConfirmURL) {
        this.inviteConfirmURL = inviteConfirmURL;
    }
}
