package com.eklib.desktopviewer.dto.security;

import com.eklib.desktopviewer.dto.BaseDTO;
import com.eklib.desktopviewer.dto.companystructure.DepartmentDTO;

/**
 * Created by s.sheman on 23.10.2015.
 */
public class UserDepartmentRoleDTO extends BaseDTO {
    private UserDTO user;
    private DepartmentDTO department;
    private RoleDTO role;

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public DepartmentDTO getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentDTO department) {
        this.department = department;
    }

    public RoleDTO getRole() {
        return role;
    }

    public void setRole(RoleDTO role) {
        this.role = role;
    }
}
