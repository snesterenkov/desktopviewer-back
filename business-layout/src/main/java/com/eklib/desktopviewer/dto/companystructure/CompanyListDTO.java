package com.eklib.desktopviewer.dto.companystructure;

import java.util.List;

/**
 * Created by Admin on 24.11.2015.
 */
public class CompanyListDTO {

    private List<CompanyDTO> companyDTOs;

    public List<CompanyDTO> getCompanyDTOs() {
        return companyDTOs;
    }

    public void setCompanyDTOs(List<CompanyDTO> companyDTOs) {
        this.companyDTOs = companyDTOs;
    }
}
