package com.eklib.desktopviewer.dto.companystructure;

import com.eklib.desktopviewer.dto.BaseDTO;
import com.eklib.desktopviewer.dto.enums.StatusDTO;

public class CompanyDTO extends BaseDTO {

    private String name;
    private StatusDTO status;
    private String workdayStart;
    private Integer workdayLength;
    private Integer workdayDelay;
    private Integer screenshotsSavingMonth;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StatusDTO getStatus() {
        return status;
    }

    public void setStatus(StatusDTO status) {
        this.status = status;
    }

    public String getWorkdayStart() {
        return workdayStart;
    }

    public void setWorkdayStart(String workdayStart) {
        this.workdayStart = workdayStart;
    }

    public Integer getWorkdayLength() {
        return workdayLength;
    }

    public void setWorkdayLength(Integer workdayLength) {
        this.workdayLength = workdayLength;
    }

    public Integer getWorkdayDelay() {
        return workdayDelay;
    }

    public void setWorkdayDelay(Integer workdayDelay) {
        this.workdayDelay = workdayDelay;
    }

    public Integer getScreenshotsSavingMonth() {
        return screenshotsSavingMonth;
    }

    public void setScreenshotsSavingMonth(Integer screenshotsSavingMonth) {
        this.screenshotsSavingMonth = screenshotsSavingMonth;
    }
}
