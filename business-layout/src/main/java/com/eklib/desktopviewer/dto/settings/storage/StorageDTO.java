package com.eklib.desktopviewer.dto.settings.storage;

import com.eklib.desktopviewer.dto.enums.StorageTypeDTO;

/**
 * Created by Admin on 09.12.2015.
 */
public class StorageDTO {

    private StorageTypeDTO type;

    private boolean isCurrent;

    public StorageTypeDTO getType() {
        return type;
    }

    public void setType(StorageTypeDTO type) {
        this.type = type;
    }

    public boolean isCurrent() {
        return isCurrent;
    }

    public void setCurrent(boolean isCurrent) {
        this.isCurrent = isCurrent;
    }
}
