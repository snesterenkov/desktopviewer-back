package com.eklib.desktopviewer.dto.companystructure;

import com.eklib.desktopviewer.dto.BaseDTO;
import com.eklib.desktopviewer.dto.enums.StatusDTO;

public class ProjectDTO extends BaseDTO{

    private String name;
    private Long departmentId;
    private StatusDTO status;
    private StatusDTO parentStatus;
    private DepartmentDTO department;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public StatusDTO getStatus() {
        return status;
    }

    public void setStatus(StatusDTO status) {
        this.status = status;
    }

    public StatusDTO getParentStatus() {
        return parentStatus;
    }

    public void setParentStatus(StatusDTO parentStatus) {
        this.parentStatus = parentStatus;
    }

    public DepartmentDTO getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentDTO department) {
        this.department = department;
    }
}
