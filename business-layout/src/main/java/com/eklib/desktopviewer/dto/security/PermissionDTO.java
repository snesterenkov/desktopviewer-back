package com.eklib.desktopviewer.dto.security;

import com.eklib.desktopviewer.dto.BaseDTO;

/**
 * Created by Лол on 09.10.2015.
 */
public class PermissionDTO extends BaseDTO {
    private String name;
    private String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
