package com.eklib.desktopviewer.dto.other;

/**
 * Created by Admin on 24.11.2015.
 */
public class PlaceInCompanyDTO {

    private String companyName;

    private int employeesCount;

    private int employeePlace;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public int getEmployeesCount() {
        return employeesCount;
    }

    public void setEmployeesCount(int employeesCount) {
        this.employeesCount = employeesCount;
    }

    public int getEmployeePlace() {
        return employeePlace;
    }

    public void setEmployeePlace(int employeePlace) {
        this.employeePlace = employeePlace;
    }
}
