package com.eklib.desktopviewer.dto.enums;


public enum InviteTypeDTO {

    COMPANY,
    DEPARTMENT,
    PROJECT
}
