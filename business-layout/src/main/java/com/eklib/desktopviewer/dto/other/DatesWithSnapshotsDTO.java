package com.eklib.desktopviewer.dto.other;

import java.util.Date;
import java.util.List;

/**
 * Created by Лол on 05.10.2015.
 */
public class DatesWithSnapshotsDTO {
    private List<Date> dates;

    public List<Date> getDates() {
        return dates;
    }

    public void setDates(List<Date> dates) {
        this.dates = dates;
    }
}
