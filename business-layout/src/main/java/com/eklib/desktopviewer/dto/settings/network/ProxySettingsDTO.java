package com.eklib.desktopviewer.dto.settings.network;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.Proxy;

/**
 * Created by Admin on 14.12.2015.
 */
public class ProxySettingsDTO {

    private String proxyHost = "";

    private String proxyPort = "";

    private ProxyTypesDTO proxyType = ProxyTypesDTO.HTTP;

    private String nonProxyHosts = "";

    private ProxyCredentialsDTO proxyCredentials = new ProxyCredentialsDTO();

    public String getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public String getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(String proxyPort) {
        this.proxyPort = proxyPort;
    }

    public ProxyTypesDTO getProxyType() {
        return proxyType;
    }

    public void setProxyType(ProxyTypesDTO proxyType) {
        this.proxyType = proxyType;
    }

    public String getNonProxyHosts() {
        return nonProxyHosts;
    }

    public void setNonProxyHosts(String nonProxyHosts) {
        this.nonProxyHosts = nonProxyHosts;
    }

    public ProxyCredentialsDTO getProxyCredentials() {
        return proxyCredentials;
    }

    public void setProxyCredentials(ProxyCredentialsDTO proxyCredentials) {
        this.proxyCredentials = proxyCredentials;
    }
}
