package com.eklib.desktopviewer.dto.companystructure;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Admin on 05.11.2015.
 */
@Component
public class DepartmentExtendDTO extends DepartmentDTO {

    private List<ProjectDTO> projects;

    public List<ProjectDTO> getProjects() {
        return projects;
    }

    public void setProjects(List<ProjectDTO> projects) {
        this.projects = projects;
    }
}
