package com.eklib.desktopviewer.dto.companystructure;

/**
 * Created by User on 16.03.2016.
 */
public class ProjectExtendDTO extends ProjectDTO {

    private String departmentName;

    private long companyId;

    private String companyName;

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
