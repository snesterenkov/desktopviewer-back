package com.eklib.desktopviewer.dto.settings.file;

public class OutdatedFilesDeleteResultDTO extends StorageOutdatedFilesInfoDTO{

    private long deletedFilesCount;
    private double secondsSpent;

    public long getDeletedFilesCount() {
        return deletedFilesCount;
    }

    public void setDeletedFilesCount(long deletedFilesCount) {
        this.deletedFilesCount = deletedFilesCount;
    }

    public double getSecondsSpent() {
        return secondsSpent;
    }

    public void setSecondsSpent(double secondsSpent) {
        this.secondsSpent = secondsSpent;
    }
}
