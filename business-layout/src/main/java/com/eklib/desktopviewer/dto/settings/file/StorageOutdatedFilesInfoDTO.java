package com.eklib.desktopviewer.dto.settings.file;

import com.eklib.desktopviewer.dto.enums.StorageTypeDTO;

/**
 * Created by User on 23.05.2016.
 */
public class StorageOutdatedFilesInfoDTO {

    private StorageTypeDTO storage;
    private String limitDate;
    private long filesCount;
    private boolean failed;
    private String failMessage;

    public StorageTypeDTO getStorage() {
        return storage;
    }

    public void setStorage(StorageTypeDTO storage) {
        this.storage = storage;
    }

    public String getLimitDate() {
        return limitDate;
    }

    public void setLimitDate(String limitDate) {
        this.limitDate = limitDate;
    }

    public long getFilesCount() {
        return filesCount;
    }

    public void setFilesCount(long filesCount) {
        this.filesCount = filesCount;
    }

    public boolean isFailed() {
        return failed;
    }

    public void setFailed(boolean failed) {
        this.failed = failed;
    }

    public String getFailMessage() {
        return failMessage;
    }

    public void setFailMessage(String failMessage) {
        this.failMessage = failMessage;
    }
}
