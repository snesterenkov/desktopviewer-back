package com.eklib.desktopviewer.dto.companystructure;

import com.eklib.desktopviewer.dto.security.UserDTO;
import com.eklib.desktopviewer.dto.security.UserProjectRoleDTO;

import java.util.List;

public class ProjectDetailDTO extends ProjectDTO {

    private boolean admin;
    private UserDTO owner;
    private List<UserProjectRoleDTO> userRoles;

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public UserDTO getOwner() {
        return owner;
    }

    public void setOwner(UserDTO owner) {
        this.owner = owner;
    }

    public List<UserProjectRoleDTO> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<UserProjectRoleDTO> userRoles) {
        this.userRoles = userRoles;
    }
}
