package com.eklib.desktopviewer.dto.security;

import com.eklib.desktopviewer.dto.BaseDTO;
import com.eklib.desktopviewer.dto.companystructure.CompanyDTO;
import org.springframework.stereotype.Component;

/**
 * Created by s.sheman on 29.10.2015.
 */
public class UserCompanyRoleDTO extends BaseDTO{
    private UserDTO user;
    private CompanyDTO company;
    private RoleDTO role;

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public CompanyDTO getCompany() {
        return company;
    }

    public void setCompany(CompanyDTO company) {
        this.company = company;
    }

    public RoleDTO getRole() {
        return role;
    }

    public void setRole(RoleDTO role) {
        this.role = role;
    }
}
