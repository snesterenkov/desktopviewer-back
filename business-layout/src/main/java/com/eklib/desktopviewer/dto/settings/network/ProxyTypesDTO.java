package com.eklib.desktopviewer.dto.settings.network;

/**
 * Created by Admin on 15.12.2015.
 */
public enum ProxyTypesDTO {

    HTTP,
    HTTPS,
    FTP,
    SOCKS
}
