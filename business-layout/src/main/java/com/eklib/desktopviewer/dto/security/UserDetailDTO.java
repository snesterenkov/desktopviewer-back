package com.eklib.desktopviewer.dto.security;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by vadim on 28.10.2014.
 */
public class UserDetailDTO extends UserDTO {

    private String password;
    private RoleDTO role;
    private List<RoleDTO> roles;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleDTO getRole() {
        return role;
    }

    public void setRole(RoleDTO role) {
        this.role = role;
    }

    public List<RoleDTO> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleDTO> roles) {
        this.roles = roles;
    }
}
