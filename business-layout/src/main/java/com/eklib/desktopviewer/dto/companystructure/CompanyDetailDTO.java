package com.eklib.desktopviewer.dto.companystructure;

import com.eklib.desktopviewer.dto.security.UserCompanyRoleDTO;
import com.eklib.desktopviewer.dto.security.UserDTO;

import java.util.Set;

public class CompanyDetailDTO extends CompanyDTO {

    private boolean admin;
    private UserDTO owner;
    private Set<UserCompanyRoleDTO> userRoles;

    public boolean isAdmin() {
        return admin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.admin = isAdmin;
    }

    public UserDTO getOwner() {
        return owner;
    }

    public void setOwner(UserDTO owner) {
        this.owner = owner;
    }

    public Set<UserCompanyRoleDTO> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Set<UserCompanyRoleDTO> userRoles) {
        this.userRoles = userRoles;
    }
}
