package com.eklib.desktopviewer.dto.settings.storage;

public class LocalStorageSettingsDTO {

    private String storageFolderName;

    private String imagesHostURL;

    public String getStorageFolderName() {
        return storageFolderName;
    }

    public void setStorageFolderName(String storageFolderName) {
        this.storageFolderName = storageFolderName;
    }

    public String getImagesHostURL() {
        return imagesHostURL;
    }

    public void setImagesHostURL(String imagesHostURL) {
        this.imagesHostURL = imagesHostURL;
    }
}
