package com.eklib.desktopviewer.dto.companystructure;

import com.eklib.desktopviewer.dto.security.UserDTO;
import com.eklib.desktopviewer.dto.security.UserDepartmentRoleDTO;

import java.util.List;

public class DepartmentDetailDTO extends  DepartmentDTO {

    private boolean admin;
    private UserDTO owner;
    private List<UserDepartmentRoleDTO> userRoles;

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public UserDTO getOwner() {
        return owner;
    }

    public void setOwner(UserDTO owner) {
        this.owner = owner;
    }

    public List<UserDepartmentRoleDTO> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<UserDepartmentRoleDTO> userRoles) {
        this.userRoles = userRoles;
    }
}
