package com.eklib.desktopviewer.dto.security;

import com.eklib.desktopviewer.dto.BaseDTO;
import com.eklib.desktopviewer.dto.companystructure.ProjectDTO;

/**
 * Created by s.sheman on 15.10.2015.
 */

public class UserProjectRoleDTO extends BaseDTO{
    private UserDTO user;
    private ProjectDTO project;
    private RoleDTO role;

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public ProjectDTO getProject() {
        return project;
    }

    public void setProject(ProjectDTO project) {
        this.project = project;
    }

    public RoleDTO getRole() {
        return role;
    }

    public void setRole(RoleDTO role) {
        this.role = role;
    }
}
