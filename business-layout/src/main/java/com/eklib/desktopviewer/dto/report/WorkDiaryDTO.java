package com.eklib.desktopviewer.dto.report;

import com.eklib.desktopviewer.dto.security.UserDTO;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

/**
 * Created by alex on 4/2/2015.
 */
@Component
public class WorkDiaryDTO implements Serializable {

    private UserDTO user;

    private List<PeriodDTO> periods;

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public List<PeriodDTO> getPeriods() {
        return periods;
    }

    public void setPeriods(List<PeriodDTO> periods) {
        this.periods = periods;
    }
}
