package com.eklib.desktopviewer.dto.security;

import com.eklib.desktopviewer.dto.BaseDTO;

import java.util.Objects;

/**
 * @author alex
 */
public class UserDTO extends BaseDTO {

    private String firstName;
    private String lastName;
    private String login;
    private String email;
    private boolean superUser;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isSuperUser() {
        return superUser;
    }

    public void setIsSuperUser(boolean isSuperUser) {
        this.superUser = isSuperUser;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (obj == null) return false;
        if (!(obj instanceof UserDTO)) {
            return false;
        }
        UserDTO userDTO = (UserDTO) obj;
        return userDTO.getId().equals(this.getId()) && userDTO.login.equals(this.login);
    }

    @Override
    public int hashCode() {
        return (int) (getId() * login.hashCode() * email.hashCode());
    }
}
