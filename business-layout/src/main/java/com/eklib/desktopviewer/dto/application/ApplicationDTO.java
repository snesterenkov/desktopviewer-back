package com.eklib.desktopviewer.dto.application;

import com.eklib.desktopviewer.dto.BaseDTO;

/**
 * Created by Admin on 20.10.2015.
 */
public class ApplicationDTO extends BaseDTO{

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
