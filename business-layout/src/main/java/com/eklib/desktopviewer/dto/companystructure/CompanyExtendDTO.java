package com.eklib.desktopviewer.dto.companystructure;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Admin on 05.11.2015.
 */
@Component
public class CompanyExtendDTO extends CompanyDTO {

    private List<DepartmentExtendDTO> departments;

    public List<DepartmentExtendDTO> getDepartments() {
        return departments;
    }

    public void setDepartments(List<DepartmentExtendDTO> departments) {
        this.departments = departments;
    }
}


