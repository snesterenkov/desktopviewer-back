package com.eklib.desktopviewer.dto.snapshot;

import com.eklib.desktopviewer.dto.BaseDTO;
import com.eklib.desktopviewer.dto.enums.ProductivityDTO;
import com.eklib.desktopviewer.serializer.CustomDateSerializer;
import com.eklib.desktopviewer.serializer.CustomTimeDeserializer;
import com.eklib.desktopviewer.serializer.CustomTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;


/**
 * Created by vadim on 18.12.2014.
 */
public class SnapshotDTO extends BaseDTO {

    private static final int percent = 100;
    private static final int AVERAGE_CLICKS_COUNT_PER_SECOND = 2;

    private Long userId;
    private byte[] file;
    private String note;
    private String message;
    private String originalFileName;
    private String resizedFileName;
    private Long progectId;
    private Integer countMouseClick;
    private Integer countKeyboardClick;
    private Integer timeInterval;
    private Integer userActivityPercent;
    private Date date;
    private Date time;
    private ProductivityDTO productivity;
    private boolean canDelete;
    private boolean canChange;


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getProgectId() {
        return progectId;
    }

    public void setProgectId(Long progectId) {
        this.progectId = progectId;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public String getResizedFileName() {
        return resizedFileName;
    }

    public void setResizedFileName(String resizedFileName) {
        this.resizedFileName = resizedFileName;
    }

    public Integer getCountMouseClick() {
        return countMouseClick;
    }

    public void setCountMouseClick(Integer countMouseClick) {
        this.countMouseClick = countMouseClick;
    }

    public Integer getCountKeyboardClick() {
        return countKeyboardClick;
    }

    public void setCountKeyboardClick(Integer countKeyboardClick) {
        this.countKeyboardClick = countKeyboardClick;
    }

    public Integer getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(Integer timeInterval) {
        this.timeInterval = timeInterval;
    }

    public Integer getUserActivityPercent() {
        int totalUserClick = getCountKeyboardClick() + getCountMouseClick();
        int timeInterval = getTimeInterval();
        float result = ((totalUserClick/(float)timeInterval)/AVERAGE_CLICKS_COUNT_PER_SECOND)*percent;
        return Math.round(result > 100? 100: result);
    }

    @JsonSerialize(using = CustomDateSerializer.class)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @JsonSerialize(using = CustomTimeSerializer.class)
    public Date getTime() {
        return time;
    }

    @JsonDeserialize(using = CustomTimeDeserializer.class)
    public void setTime(Date time) {
        this.time = time;
    }

    public ProductivityDTO getProductivity() {
        return productivity;
    }

    public void setProductivity(ProductivityDTO productivity) {
        this.productivity = productivity;
    }

    public boolean isCanDelete() {
        return canDelete;
    }

    public void setCanDelete(boolean canDelete) {
        this.canDelete = canDelete;
    }

    public boolean isCanChange() {
        return canChange;
    }

    public void setCanChange(boolean canChange) {
        this.canChange = canChange;
    }
}