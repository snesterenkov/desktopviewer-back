package com.eklib.desktopviewer.services.report;

import com.eklib.desktopviewer.dto.companystructure.CompanyExtendDTO;
import com.eklib.desktopviewer.dto.companystructure.ProjectDTO;
import com.eklib.desktopviewer.dto.report.WorkDiaryDTO;
import com.eklib.desktopviewer.persistance.model.enums.PeriodEnum;
import org.joda.time.LocalDate;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created by alex on 4/1/2015.
 */
public interface ReportService {

    List<WorkDiaryDTO> getWorkingHoursByTimePeriod(List<ProjectDTO> projectDTOs, PeriodEnum period, LocalDate startDate, LocalDate endDate, String client);

    Collection<CompanyExtendDTO> findAdminProjectsHierarchyByAdminLogin(String login);
}
