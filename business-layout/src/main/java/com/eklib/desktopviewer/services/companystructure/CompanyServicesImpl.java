package com.eklib.desktopviewer.services.companystructure;

import com.eklib.desktopviewer.convertor.fromdto.companystructure.CompanyFromDTO;
import com.eklib.desktopviewer.convertor.fromdto.companystructure.CompanyFromDetailDTO;
import com.eklib.desktopviewer.convertor.fromdto.security.UserFromDTO;
import com.eklib.desktopviewer.convertor.todto.companystructure.CompanyToDTO;
import com.eklib.desktopviewer.convertor.todto.companystructure.CompanyToDetailDTO;
import com.eklib.desktopviewer.dto.companystructure.CompanyDTO;
import com.eklib.desktopviewer.dto.companystructure.CompanyDetailDTO;
import com.eklib.desktopviewer.dto.enums.StatusDTO;
import com.eklib.desktopviewer.persistance.model.companystructure.CompanyEntity;
import com.eklib.desktopviewer.persistance.model.companystructure.DepartmentEntity;
import com.eklib.desktopviewer.persistance.model.companystructure.ProjectEntity;
import com.eklib.desktopviewer.persistance.model.enums.StatusEnum;
import com.eklib.desktopviewer.persistance.model.security.*;
import com.eklib.desktopviewer.persistance.repository.companystructure.CompanyRepository;
import com.eklib.desktopviewer.persistance.repository.companystructure.ProjectRepository;
import com.eklib.desktopviewer.persistance.repository.companystructure.UserCompanyRoleRepository;
import com.eklib.desktopviewer.persistance.repository.security.RoleRepository;
import com.eklib.desktopviewer.persistance.repository.security.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class CompanyServicesImpl implements CompanyServices {

    @Autowired
    private UserFromDTO userFromDTO;
    @Autowired
    private CompanyFromDTO companyFromDTO;
    @Autowired
    private CompanyFromDetailDTO companyFromDetailDTO;
    @Autowired
    private CompanyToDTO companyToDTO;
    @Autowired
    private CompanyToDetailDTO companyToDetailDTO;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserCompanyRoleRepository userCompanyRoleRepository;
    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public CompanyDetailDTO insert(CompanyDTO companyDTO, String client) {
        validateCompanyInput(companyDTO);
        Assert.isNull(companyDTO.getId(), "Company id must be null.");

        CompanyEntity newCompany = companyFromDTO.apply(companyDTO);
        Assert.notNull(newCompany, "Company can not be parsed.");

        UserEntity user = userRepository.getUserByName(client);
        newCompany.setOwner(user);
        companyRepository.insert(newCompany);

        Role adminCompanyRole = roleRepository.getRoleByName(RoleRepository.ADMIN_COMPANY_ROLE_NAME);
        Assert.notNull(adminCompanyRole, "Required role doesn't exist.");

        attachUserToCompanyWithRole(user, newCompany, adminCompanyRole);

        CompanyDetailDTO dto = companyToDetailDTO.apply(newCompany);
        if (dto != null) {
            dto.setIsAdmin(true);
        }
        return dto;
    }

    @Override
    public CompanyDetailDTO update(Long id, CompanyDTO companyDTO, String client) {
        companyDTO.setId(id);
        CompanyEntity newCompany = companyFromDTO.apply(companyDTO);
        Assert.notNull(newCompany, "Company can not be parsed.");

        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean isCompanyAdmin = companyRepository.isCompanyAdmin(newCompany, user);
        Assert.isTrue(isSuperUser || isCompanyAdmin, "You haven't required permissions.");

        CompanyEntity updatedCompany = companyRepository.update(newCompany);
        Assert.notNull(updatedCompany, "Company didn't updated.");

        CompanyDetailDTO dto = companyToDetailDTO.apply(updatedCompany);
        if (dto != null) {
            dto.setIsAdmin(true);
        }
        return dto;
    }

    @Override
    public void delete(Long id, String client) {
        UserEntity clientEntity = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(clientEntity);
        Assert.isTrue(isSuperUser, "You haven't required permissions.");

        CompanyEntity company = companyRepository.findById(id);
        Assert.notNull(company, "Company doesn't exist.");

        companyRepository.delete(company);
    }

    @Override
    public CompanyDetailDTO findById(Long id, String client) {
        CompanyEntity company = companyRepository.findById(id);
        Assert.notNull(company, "Couldn't find company.");

        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean isCompanyAdmin = companyRepository.isCompanyAdmin(company, user);
        Assert.isTrue(isSuperUser || isCompanyAdmin, "You haven't required permissions.");

        CompanyDetailDTO dto = companyToDetailDTO.apply(company);
        if (dto != null) {
            dto.setIsAdmin(true);
        }
        return dto;
    }

    @Override
    public Collection<CompanyDetailDTO> findAll(String client) {
        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        if (isSuperUser) {
            List<CompanyEntity> allCompanies = companyRepository.findAll();
            return allCompanies.stream()
                    .map(companyToDetailDTO::apply)
                    .peek(dto -> {
                        if (dto != null) {
                            dto.setIsAdmin(true);
                        }
                    })
                    .collect(Collectors.toList());
        } else {
            List<CompanyEntity> usersCompanies = new ArrayList<>();
            usersCompanies.addAll(getUserCompanies(user));
            usersCompanies.addAll(getUserCompaniesByRelatedDepartments(user));
            usersCompanies.addAll(getUserCompaniesByRelatedProjects(user));
            return usersCompanies.stream()
                    .distinct()
                    .map(company -> {
                        CompanyDetailDTO dto = companyToDetailDTO.apply(company);
                        if (companyRepository.isCompanyAdmin(company, user)) {
                            if (dto != null) {
                                dto.setIsAdmin(true);
                            }
                        }
                        return dto;
                    })
                    .collect(Collectors.toList());
        }
    }

    @Override
    public CompanyDetailDTO changeStatus(Long id, StatusDTO statusDTO, String client) {
        Assert.notNull(statusDTO, "Status must not be null.");

        CompanyEntity company = companyRepository.findById(id);
        Assert.notNull(company, "Couldn't find company.");

        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean isCompanyAdmin = companyRepository.isCompanyAdmin(company, user);
        Assert.isTrue(isSuperUser || isCompanyAdmin, "You haven't required permissions.");

        StatusEnum newStatus = StatusEnum.valueOf(statusDTO.toString());
        CompanyEntity updatedCompany = company;
        if (companyRepository.changeStatus(company, newStatus)) {
            updatedCompany = companyRepository.findById(id);
        }
        CompanyDetailDTO dto = companyToDetailDTO.apply(updatedCompany);
        if (dto != null) {
            dto.setIsAdmin(true);
        }
        return dto;
    }

    @Override
    public Collection<CompanyDTO> findOpen(String client) {
        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        if (isSuperUser) {
            List<CompanyEntity> allCompanies = companyRepository.findAll();
            return allCompanies.stream()
                    .filter(company -> StatusEnum.OPEN.equals(company.getStatus()))
                    .map(companyToDTO::apply)
                    .collect(Collectors.toList());
        } else {
            List<CompanyEntity> openUserCompanies = new ArrayList<>();
            openUserCompanies.addAll(getUserCompanies(user));
            openUserCompanies.addAll(getUserCompaniesByRelatedDepartments(user));
            return openUserCompanies.stream()
                    .distinct()
                    .filter(company -> StatusEnum.OPEN.equals(company.getStatus()))
                    .map(companyToDTO::apply)
                    .collect(Collectors.toList());
        }
    }

    @Override
    public void deleteUserCompanyRole(Long userCompanyRoleId, String client) {
        UserCompanyRole userCompanyRole = userCompanyRoleRepository.findById(userCompanyRoleId);
        Assert.notNull(userCompanyRole, "Couldn't find user company role.");

        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean isCompanyAdmin = companyRepository.isCompanyAdmin(userCompanyRole.getCompany(), user);
        boolean isOwnerRole = userCompanyRole.getCompany().getOwner().equals(userCompanyRole.getUser()) &&
                RoleRepository.ADMIN_COMPANY_ROLE_NAME.equals(userCompanyRole.getRole().getName());
        Assert.isTrue(!isOwnerRole && (isSuperUser || isCompanyAdmin), "You haven't required permissions.");

        userCompanyRoleRepository.delete(userCompanyRole);
    }

    @Override
    public CompanyDetailDTO detailUpdate(Long id, CompanyDetailDTO companyDetailDTO, String client) {
        CompanyEntity company = companyRepository.findById(id);
        Assert.notNull(company, "Couldn't find company.");

        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean isCompanyAdmin = companyRepository.isCompanyAdmin(company, user);
        Assert.isTrue(isSuperUser || isCompanyAdmin, "You haven't required permissions.");

        //todo refactoring
        companyDetailDTO.getUserRoles().stream()
                .filter(userCompanyRoleDTO -> userCompanyRoleDTO.getId() == null && userCompanyRoleDTO.getRole().getName().equals(RoleRepository.ADMIN_COMPANY_ROLE_NAME))
                .forEach(userCompanyRoleDTO -> company.getDepartments()
                        .forEach(departmentEntity -> {
                            UserEntity userEntity = userFromDTO.apply(userCompanyRoleDTO.getUser());

                            Role adminDepartmentRole = roleRepository.getRoleByName(RoleRepository.ADMIN_DEPARTMENT_ROLE_NAME);

                            UserDepartmentRole departmentRole = new UserDepartmentRole();
                            departmentRole.setUser(userEntity);
                            departmentRole.setDepartment(departmentEntity);
                            departmentRole.setRole(adminDepartmentRole);

                            departmentEntity.getUserDepartmentRoles().add(departmentRole);

                            departmentEntity.getProjects()
                                    .forEach(projectEntity -> {

                                        Role adminProjectRole = roleRepository.getRoleByName(RoleRepository.ADMIN_PROJECT_ROLE_NAME);

                                        UserProjectRole userProjectRole = new UserProjectRole();
                                        userProjectRole.setUser(userEntity);
                                        userProjectRole.setProject(projectEntity);
                                        userProjectRole.setRole(adminProjectRole);

                                        projectEntity.getUserProjectRoles().add(userProjectRole);
                                    });
                        }));

        CompanyEntity updatedCompany = Objects.requireNonNull(companyFromDetailDTO.apply(companyDetailDTO));
        companyRepository.merge(updatedCompany);
        CompanyDetailDTO dto = companyToDetailDTO.apply(updatedCompany);
        if (dto != null) {
            dto.setIsAdmin(true);
        }
        return dto;
    }

    @Override
    public List<CompanyDTO> findForEmployee(Long employeeId, String client) {
        UserEntity employee = userRepository.findById(employeeId);
        Assert.notNull(employee, "Employee doesn't exist.");

        UserEntity user = userRepository.getUserByName(client);
        boolean isAdminProject = user.getUserProjectRoles().stream()
                .anyMatch(userProjectRole -> projectRepository.isProjectAdmin(userProjectRole.getProject(), employee));
        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean isEmployeeEqualUser = employee.getId().equals(user.getId());
        Assert.isTrue(isAdminProject || isSuperUser || isEmployeeEqualUser, "You haven't required permissions.");
        if (isSuperUser || isEmployeeEqualUser)
            return getUserCompaniesByRelatedProjects(employee).stream()
                    .map(companyToDTO::apply)
                    .collect(Collectors.toList());
        return getUserCompaniesForAdminProject(employee, user).stream()
                .map(companyToDTO::apply)
                .collect(Collectors.toList());
    }

    private void validateCompanyInput(CompanyDTO companyInput) {
        Assert.notNull(companyInput, "Company must be not null.");

        Assert.hasLength(companyInput.getName(), "Company name must not be null or empty string.");

        CompanyEntity sameNameCompany = companyRepository.getCompanyByName(companyInput.getName());
        Assert.isNull(sameNameCompany, "Company with the same name already exists.");

        Assert.hasLength(companyInput.getWorkdayStart(), "Company workday start time must not be null or empty string.");

        Assert.notNull(companyInput.getWorkdayLength(), "Company workday length must not be null.");
        Assert.isTrue(0 <= companyInput.getWorkdayLength(), "Company workday length must be positive or zero.");

        if (companyInput.getWorkdayDelay() != null) {
            Assert.isTrue(0 <= companyInput.getWorkdayDelay(), "Company workday delay must be positive or zero.");
        }

        if (companyInput.getScreenshotsSavingMonth() != null) {
            Assert.isTrue(0 <= companyInput.getScreenshotsSavingMonth(), "Saving term ust be positive or zero.");
        }
    }

    private void attachUserToCompanyWithRole(UserEntity user, CompanyEntity company, Role role) {
        UserCompanyRole relation = new UserCompanyRole();
        relation.setUser(user);
        relation.setCompany(company);
        relation.setRole(role);
        userCompanyRoleRepository.insert(relation);
        company.getUserCompanyRoles().add(relation);
    }

    private List<CompanyEntity> getUserCompanies(UserEntity user) {
        return user.getUserCompanyRoles().stream()
                .map(UserCompanyRole::getCompany)
                .collect(Collectors.toList());
    }

    private List<CompanyEntity> getUserCompaniesByRelatedProjects(UserEntity user) {
        return user.getUserProjectRoles().stream()
                .map(UserProjectRole::getProject)
                .map(ProjectEntity::getDepartment)
                .map(DepartmentEntity::getCompany)
                .distinct()
                .collect(Collectors.toList());
    }

    private List<CompanyEntity> getUserCompaniesByRelatedDepartments(UserEntity user) {
        return user.getUserDepartmentRoles().stream()
                .map(UserDepartmentRole::getDepartment)
                .map(DepartmentEntity::getCompany)
                .distinct()
                .collect(Collectors.toList());
    }

    private Set<CompanyEntity> getUserCompaniesForAdminProject(UserEntity user, UserEntity manager) {
        Set<CompanyEntity> managerCompanies = manager.getUserCompanyRoles().stream()
                .map(UserCompanyRole::getCompany)
                .collect(Collectors.toSet());
        return user.getUserProjectRoles().stream()
                .map(UserProjectRole::getProject)
                .map(ProjectEntity::getDepartment)
                .map(DepartmentEntity::getCompany)
                .filter(managerCompanies::contains)
                .collect(Collectors.toSet());
    }
}
