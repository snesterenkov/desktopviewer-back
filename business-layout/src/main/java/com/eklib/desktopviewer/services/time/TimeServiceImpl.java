package com.eklib.desktopviewer.services.time;

import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
public class TimeServiceImpl implements TimeService {

    @Override
    public String getCurrentTime() {
        return DateTime.now().toString("dd.MM.yyyy HH:mm:ss");
    }

    @Override
    public Map<String, String> getTime() {
        Instant timestamp = Instant.now();
        Map<String, String> time = new HashMap<>();
        time.put("seconds", String.valueOf(timestamp.getEpochSecond()));
        time.put("nanos", String.valueOf(timestamp.getNano()));
        time.put("zoneId", ZoneId.systemDefault().getId());
        return time;
    }
}
