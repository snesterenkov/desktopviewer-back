package com.eklib.desktopviewer.services.settings.network;

import com.eklib.desktopviewer.dto.settings.network.ProxySettingsDTO;

/**
 * Created by Admin on 14.12.2015.
 */
public interface ProxySettingsService {

    ProxySettingsDTO getProxySettings(String client);

    void setProxySettings(ProxySettingsDTO proxySettings, String client);
}
