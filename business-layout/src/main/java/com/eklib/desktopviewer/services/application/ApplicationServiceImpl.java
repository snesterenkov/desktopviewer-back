package com.eklib.desktopviewer.services.application;

import com.eklib.desktopviewer.convertor.fromdto.application.ApplicationFromDTO;
import com.eklib.desktopviewer.convertor.todto.application.ApplicationToDTO;
import com.eklib.desktopviewer.dto.application.ApplicationDTO;
import com.eklib.desktopviewer.dto.enums.ProductivityDTO;
import com.eklib.desktopviewer.persistance.model.application.ApplicationEntity;
import com.eklib.desktopviewer.persistance.model.application.DepartmentApplicationProductivityEntity;
import com.eklib.desktopviewer.persistance.model.companystructure.DepartmentEntity;
import com.eklib.desktopviewer.persistance.model.enums.ProductivityEnum;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.repository.application.ApplicationRepository;
import com.eklib.desktopviewer.persistance.repository.application.DepartmentApplicationProductivityRepository;
import com.eklib.desktopviewer.persistance.repository.companystructure.DepartmentRepository;
import com.eklib.desktopviewer.persistance.repository.security.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class ApplicationServiceImpl implements ApplicationService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ApplicationRepository applicationRepository;
    @Autowired
    private DepartmentRepository departmentRepository;
    @Autowired
    private DepartmentApplicationProductivityRepository departmentApplicationProductivityRepository;

    @Autowired
    private ApplicationToDTO applicationToDTO;

    @Autowired
    private ApplicationFromDTO applicationFromDTO;


    @Override
    public ApplicationDTO update(Long id, ApplicationDTO applicationDTO, String client) {
        applicationDTO.setId(id);
        ApplicationEntity applicationEntity = applicationFromDTO.apply(applicationDTO);
        Assert.notNull(applicationEntity, "Can't find company");

        ApplicationEntity updatedApplication = applicationRepository.update(applicationEntity);
        return applicationToDTO.apply(updatedApplication);
    }

    @Override
    public ApplicationDTO insert(ApplicationDTO applicationDTO, String client) {
        Assert.hasLength(applicationDTO.getName(), "Application name must not be null or empty.");
        Assert.isNull(applicationRepository.findApplicationByName(applicationDTO.getName()), "Application already exists");

        ApplicationEntity applicationEntity = applicationFromDTO.apply(applicationDTO);
        Assert.notNull(applicationEntity, "Can't create application");

        ApplicationEntity updatedApplication = applicationRepository.insert(applicationEntity);
        return applicationToDTO.apply(updatedApplication);
    }

    @Override
    public ApplicationDTO findById(Long id, String client) {
        ApplicationEntity applicationEntity = applicationRepository.findById(id);
        Assert.notNull(applicationEntity, "Can't find application");
        return applicationToDTO.apply(applicationEntity);
    }

    @Override
    public Collection<ApplicationDTO> findAll() {
        return applicationRepository.findAll().stream().map(applicationToDTO::apply).collect(Collectors.toList());
    }

    @Override
    public void delete(Long id, String client) {
        ApplicationDTO applicationDTO = new ApplicationDTO();
        applicationDTO.setId(id);
        ApplicationEntity deletedApplication = applicationFromDTO.apply(applicationDTO);
        Assert.notNull(deletedApplication, "Can't find application");
        applicationRepository.delete(deletedApplication);
    }

    @Override
    public Map<ProductivityDTO, List<ApplicationDTO>> findForDepartment(Long departmentId, String client) {
        DepartmentEntity department = departmentRepository.findById(departmentId);
        Assert.notNull(department, "Department doesn't exists.");

        Map<ProductivityDTO, List<ApplicationDTO>> apps = new HashMap<>();
        List<DepartmentApplicationProductivityEntity> daps = departmentApplicationProductivityRepository.findForDepartment(departmentId);
        apps.put(ProductivityDTO.PRODUCTIVE, new ArrayList<>());
        apps.put(ProductivityDTO.UNPRODUCTIVE, new ArrayList<>());
        for (DepartmentApplicationProductivityEntity dap : daps) {
            apps.get(ProductivityDTO.valueOf(dap.getProductivity().toString())).add(applicationToDTO.apply(dap.getApplication()));
        }
        return apps;
    }

    @Override
    public List<ApplicationDTO> findDepartmentFree(Long departmentId, String client) {
        DepartmentEntity department = departmentRepository.findById(departmentId);
        Assert.notNull(department, "Department doesn't exist.");

        List<ApplicationEntity> apps = applicationRepository.findDepartmentFreeApplications(departmentId);
        return apps.stream().map(applicationToDTO::apply).collect(Collectors.toList());
    }

    @Override
    public void deleteApplications(List<ApplicationDTO> applications, String client) {
        List<ApplicationEntity> deletedApplications = applications.stream().map(applicationFromDTO::apply).collect(Collectors.toList());
        deletedApplications.forEach(applicationRepository::delete);
    }

    @Override
    public void attachToDepartment(Long departmentId, List<ApplicationDTO> applications, ProductivityDTO productivity, String client) {
        DepartmentEntity department = departmentRepository.findById(departmentId);
        Assert.notNull(department, "Department doesn't exist.");

        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean isDepartmentAdmin = departmentRepository.isDepartmentAdmin(department, user);
        Assert.isTrue(isSuperUser || isDepartmentAdmin, "You haven't required permissions.");
        for (ApplicationDTO application : applications) {
            DepartmentApplicationProductivityEntity dap = departmentApplicationProductivityRepository.findByDepartmentAndApplication(departmentId, application.getId());
            if (productivity == ProductivityDTO.NEUTRAL) {
                if (dap != null) {
                    departmentApplicationProductivityRepository.delete(dap);
                }
            } else {
                if (dap != null) {
                    dap.setProductivity(ProductivityEnum.valueOf(productivity.toString()));
                    departmentApplicationProductivityRepository.update(dap);
                } else {
                    dap = new DepartmentApplicationProductivityEntity();
                    dap.setProductivity(ProductivityEnum.valueOf(productivity.toString()));
                    dap.setDepartment(department);
                    dap.setApplication(applicationFromDTO.apply(application));
                    departmentApplicationProductivityRepository.insert(dap);
                }
            }
        }
    }
}
