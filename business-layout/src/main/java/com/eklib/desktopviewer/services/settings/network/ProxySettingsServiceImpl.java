package com.eklib.desktopviewer.services.settings.network;

import com.eklib.desktopviewer.dto.settings.network.ProxySettingsDTO;
import com.eklib.desktopviewer.dto.settings.network.ProxyTypesDTO;
import com.eklib.desktopviewer.persistance.model.property.PropertyEntity;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.repository.property.PropertyRepository;
import com.eklib.desktopviewer.persistance.repository.security.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.net.Authenticator;
import java.net.PasswordAuthentication;

@Service
@Transactional
public class ProxySettingsServiceImpl implements ProxySettingsService {

    @Autowired
    private PropertyRepository propertyRepository;
    @Autowired
    private UserRepository userRepository;

    private ProxySettingsDTO proxySettings;

    @Override
    public ProxySettingsDTO getProxySettings(String client) {
        UserEntity user = userRepository.getUserByName(client);
        Assert.notNull(user, "Couldn't find user with same name.");
        Assert.isTrue(userRepository.isSuperUser(user), "You have not required permissions.");
        return proxySettings;
    }

    @Override
    public void setProxySettings(ProxySettingsDTO newProxySettings, String client) {
        UserEntity user = userRepository.getUserByName(client);
        Assert.notNull(user, "Couldn't find user with same name.");
        Assert.isTrue(userRepository.isSuperUser(user), "You have not required permissions.");
        proxySettings = newProxySettings;
        setProxy();
        PropertyEntity px_host = propertyRepository.findByName("Proxy.host");
        if(px_host != null) {
            px_host.setValue(proxySettings.getProxyHost());
            propertyRepository.update(px_host);
        } else {
            px_host = new PropertyEntity();
            px_host.setName("Proxy.host");
            px_host.setValue(proxySettings.getProxyHost());
            propertyRepository.insert(px_host);
        }
        PropertyEntity px_port = propertyRepository.findByName("Proxy.port");
        if(px_port != null) {
            px_port.setValue(proxySettings.getProxyPort());
            propertyRepository.update(px_port);
        } else {
            px_port = new PropertyEntity();
            px_port.setName("Proxy.port");
            px_port.setValue(proxySettings.getProxyPort());
            propertyRepository.insert(px_port);
        }
        PropertyEntity px_type = propertyRepository.findByName("Proxy.type");
        if(px_type != null) {
            px_type.setValue(proxySettings.getProxyType().toString());
            propertyRepository.update(px_type);
        } else {
            px_type = new PropertyEntity();
            px_type.setName("Proxy.type");
            px_type.setValue(proxySettings.getProxyType().toString());
            propertyRepository.insert(px_type);
        }
        PropertyEntity px_nonProxyHosts = propertyRepository.findByName("Proxy.nonProxyHosts");
        if(px_nonProxyHosts != null) {
            px_nonProxyHosts.setValue(proxySettings.getNonProxyHosts());
            propertyRepository.update(px_nonProxyHosts);
        } else {
            px_nonProxyHosts = new PropertyEntity();
            px_nonProxyHosts.setName("Proxy.nonProxyHosts");
            px_nonProxyHosts.setValue(proxySettings.getNonProxyHosts());
            propertyRepository.insert(px_nonProxyHosts);
        }
        PropertyEntity px_username = propertyRepository.findByName("Proxy.username");
        PropertyEntity px_password = propertyRepository.findByName("Proxy.password");
        if(proxySettings.getProxyCredentials() != null) {
            if(px_username != null) {
                px_username.setValue(proxySettings.getProxyCredentials().getUsername());
                propertyRepository.update(px_username);
            } else {
                px_username =  new PropertyEntity();
                px_username.setName("Proxy.username");
                px_username.setValue(proxySettings.getProxyCredentials().getUsername());
                propertyRepository.insert(px_username);
            }
            if(px_password != null) {
                px_password.setValue(proxySettings.getProxyCredentials().getPassword());
                propertyRepository.update(px_password);
            } else {
                px_password =  new PropertyEntity();
                px_password.setName("Proxy.password");
                px_password.setValue(proxySettings.getProxyCredentials().getPassword());
                propertyRepository.insert(px_password);
            }
        } else {
            if(px_username != null) {
                propertyRepository.delete(px_username);
            }
            if(px_password != null) {
                propertyRepository.delete(px_password);
            }
        }

    }

    @PostConstruct
    private void getProxySettingsFromDB() {
        proxySettings = new ProxySettingsDTO();
        PropertyEntity px_host = propertyRepository.findByName("Proxy.host");
        if(px_host != null) {
            proxySettings.setProxyHost(px_host.getValue());
        }
        PropertyEntity px_port = propertyRepository.findByName("Proxy.port");
        if(px_port != null) {
            proxySettings.setProxyPort(px_port.getValue());
        }
        PropertyEntity px_type = propertyRepository.findByName("Proxy.type");
        if(px_type != null) {
            proxySettings.setProxyType(ProxyTypesDTO.valueOf(px_type.getValue()));
        }
        PropertyEntity px_nonProxyHosts = propertyRepository.findByName("Proxy.nonProxyHosts");
        if(px_nonProxyHosts != null) {
            proxySettings.setNonProxyHosts(px_nonProxyHosts.getValue());
        }
        PropertyEntity px_username = propertyRepository.findByName("Proxy.username");
        if(px_username != null) {
            proxySettings.getProxyCredentials().setUsername(px_username.getValue());
        }
        PropertyEntity px_password = propertyRepository.findByName("Proxy.password");
        if(px_password != null) {
            proxySettings.getProxyCredentials().setPassword(px_password.getValue());
        }
        setProxy();
    }

    private void setProxy() {
        switch(proxySettings.getProxyType()) {
            case SOCKS:
                setSocksProxy();
                break;
            case HTTP:
                setHttpProxy();
                break;
            case HTTPS:
                setHttpsProxy();
                break;
            case FTP:
                setFtpProxy();
                break;
            default:
                Assert.isTrue(false, "Unknown proxy type.");
        }
        if(proxySettings.getProxyCredentials() != null) {
            Authenticator.setDefault(new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(proxySettings.getProxyCredentials().getUsername(), proxySettings.getProxyCredentials().getPassword().toCharArray());
                }
            });
        } else {
            Authenticator.setDefault(null);
        }

    }

    private void setSocksProxy() {
        System.setProperty("socksProxyHost", proxySettings.getProxyHost());
        System.setProperty("socksProxyPort", proxySettings.getProxyPort());
    }

    private void setHttpProxy() {
        System.setProperty("http.proxyHost", proxySettings.getProxyHost());
        System.setProperty("http.proxyPort", proxySettings.getProxyPort());
        System.setProperty("http.nonProxyHosts", proxySettings.getNonProxyHosts());
    }

    private void setHttpsProxy() {
        System.setProperty("https.proxyHost", proxySettings.getProxyHost());
        System.setProperty("https.proxyPort", proxySettings.getProxyPort());
        System.setProperty("http.nonProxyHosts", proxySettings.getNonProxyHosts());
    }

    private void setFtpProxy() {
        System.setProperty("ftp.proxyHost", proxySettings.getProxyHost());
        System.setProperty("ftp.proxyPort", proxySettings.getProxyPort());
        System.setProperty("ftp.nonProxyHosts", proxySettings.getNonProxyHosts());
    }
}
