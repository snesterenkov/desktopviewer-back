package com.eklib.desktopviewer.services.snapshot;

import com.eklib.desktopviewer.dto.companystructure.CompanyDTO;
import com.eklib.desktopviewer.dto.companystructure.ProjectDTO;
import com.eklib.desktopviewer.dto.enums.ProductivityDTO;
import com.eklib.desktopviewer.dto.other.PlaceInCompanyDTO;
import com.eklib.desktopviewer.dto.snapshot.SnapshotDTO;
import com.eklib.desktopviewer.dto.snapshot.UserStatsDTO;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface SnapshotService {

    SnapshotDTO insert(SnapshotDTO companyDTO, Integer version, LocalDateTime date, String client);

    List<Date> getDatesWithSnapshotsByUser(Long userId);

    Map<Long, UserStatsDTO> getUsersStatsByCompanyAndDate(Long companyId, LocalDate date, String client);

    Map<Long, UserStatsDTO> getUsersStatsForProjectsAndDate(String client, LocalDate date, List<ProjectDTO> projects);

    List<SnapshotDTO> findSnapshotsByUserAndDates(Long userId, LocalDate startDate, LocalDate endDate, String client);

    PlaceInCompanyDTO calculatePlacesInCompaniesForEmployee(Long userId, CompanyDTO company, LocalDate startDate, LocalDate endDate, String client);

    void changeSnapshotsProductivity(List<SnapshotDTO> snapshots, ProductivityDTO productivity, String client);

    void deleteSnapshots(List<SnapshotDTO> snapshots, String client);
}
