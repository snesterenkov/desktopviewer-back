package com.eklib.desktopviewer.services.companystructure;

import com.eklib.desktopviewer.dto.companystructure.CompanyDTO;
import com.eklib.desktopviewer.dto.companystructure.CompanyDetailDTO;
import com.eklib.desktopviewer.dto.enums.StatusDTO;

import java.util.Collection;
import java.util.List;

public interface CompanyServices {

    CompanyDetailDTO insert(CompanyDTO companyDTO, String client);

    CompanyDetailDTO update(Long id, CompanyDTO companyDTO, String client);

    void delete(Long id, String client);

    CompanyDetailDTO findById(Long id, String client);

    Collection<CompanyDetailDTO> findAll(String client);

    CompanyDetailDTO changeStatus(Long id, StatusDTO statusDTO, String client);

    Collection<CompanyDTO> findOpen(String client);

    void deleteUserCompanyRole(Long userCompanyRoleId, String client);

    CompanyDetailDTO detailUpdate(Long id, CompanyDetailDTO dto, String client);

    List<CompanyDTO> findForEmployee(Long employeeId, String client);
}
