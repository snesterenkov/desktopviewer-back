package com.eklib.desktopviewer.services.settings.storage;

import com.eklib.desktopviewer.container.service.FileServiceContainer;
import com.eklib.desktopviewer.dto.enums.StorageTypeDTO;
import com.eklib.desktopviewer.dto.settings.file.OutdatedFilesDeleteResultDTO;
import com.eklib.desktopviewer.dto.settings.file.StorageOutdatedFilesInfoDTO;
import com.eklib.desktopviewer.dto.settings.storage.StorageDTO;
import com.eklib.desktopviewer.persistance.model.property.PropertyEntity;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.repository.companystructure.CompanyRepository;
import com.eklib.desktopviewer.persistance.repository.property.PropertyRepository;
import com.eklib.desktopviewer.persistance.repository.security.UserRepository;
import com.eklib.desktopviewer.services.file.FileService;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class StorageSettingsServiceImpl implements StorageSettingsService {

    @Autowired
    private FileServiceContainer fileServiceContainer;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PropertyRepository propertyRepository;
    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public List<StorageDTO> getAllAvailableStorage(String client) {
        UserEntity user = userRepository.getUserByName(client);
        Assert.notNull(user, "Couldn't find user with same name.");
        Assert.isTrue(userRepository.isSuperUser(user), "You have not required permissions.");
        StorageTypeDTO currentType = fileServiceContainer.getCurrentFileServiceType();
        List<StorageDTO> result = new ArrayList<>();
        for(StorageTypeDTO type: StorageTypeDTO.values()) {
            StorageDTO item = new StorageDTO();
            item.setType(type);
            item.setCurrent(type.equals(currentType));
            result.add(item);
        }
        return result;
    }

    @Override
    public StorageDTO setCurrentStorage(StorageDTO storage, String client) {
        UserEntity user = userRepository.getUserByName(client);
        Assert.notNull(user, "Couldn't find user with same name.");
        Assert.isTrue(userRepository.isSuperUser(user), "You have not required permissions.");
        fileServiceContainer.setCurrentFileServiceType(storage.getType());

        PropertyEntity currentStorage = propertyRepository.findByName("Storage.CurrentStorage");
        if(currentStorage == null) {
            currentStorage = new PropertyEntity();
            currentStorage.setName("Storage.CurrentStorage");
            currentStorage.setValue(storage.getType().toString());
            propertyRepository.insert(currentStorage);
        } else {
            currentStorage.setValue(storage.getType().toString());
            propertyRepository.update(currentStorage);
        }

        storage.setCurrent(true);
        return storage;
    }

    @Override
    public List<StorageOutdatedFilesInfoDTO> getInfoAboutOutdatedFiles(String client) {
        UserEntity user = userRepository.getUserByName(client);
        Assert.notNull(user, "User doesn't exist.");
        Assert.isTrue(userRepository.isSuperUser(user), "You haven't required permissions.");

        List<StorageOutdatedFilesInfoDTO> results = new ArrayList<>();
        LocalDate limit = LocalDate.now().minusMonths(companyRepository.getMaxSavingPeriod());
        for(StorageTypeDTO storage: StorageTypeDTO.values()){
            results.add(getInfoAboutOutdatedFiles(storage, limit));
        }
        return results;
    }

    @Override
    public StorageOutdatedFilesInfoDTO getInfoAboutStorageOutdatedFiles(StorageTypeDTO storage, String client) {
        UserEntity user = userRepository.getUserByName(client);
        Assert.notNull(user, "User doesn't exist.");
        Assert.isTrue(userRepository.isSuperUser(user), "You haven't required permissions.");

        LocalDate limit = LocalDate.now().minusMonths(companyRepository.getMaxSavingPeriod());
        return getInfoAboutOutdatedFiles(storage, limit);
    }

    @Override
    public List<OutdatedFilesDeleteResultDTO> deleteOutdatedImages(String client) {
        UserEntity user = userRepository.getUserByName(client);
        Assert.notNull(user, "User doesn't exist.");
        Assert.isTrue(userRepository.isSuperUser(user), "You have not required permissions.");

        LocalDate limitDate = LocalDate.now().minusMonths(companyRepository.getMaxSavingPeriod());
        List<OutdatedFilesDeleteResultDTO> results = new ArrayList<>();
        for(StorageTypeDTO storageType: StorageTypeDTO.values()) {
            results.add(deleteOutdatedFiles(storageType, limitDate));
        }
        return results;
    }

    @Override
    public OutdatedFilesDeleteResultDTO deleteStorageOutdatedFiles(StorageTypeDTO storage, String client) {
        UserEntity user = userRepository.getUserByName(client);
        Assert.notNull(user, "User doesn't exist.");
        Assert.isTrue(userRepository.isSuperUser(user), "You have not required permissions.");

        LocalDate limitDate = LocalDate.now().minusMonths(companyRepository.getMaxSavingPeriod());
        return deleteOutdatedFiles(storage, limitDate);
    }

    @PostConstruct
    private void getCurrentStorageFormDB() {
        PropertyEntity currentStorage = propertyRepository.findByName("Storage.CurrentStorage");
        if(currentStorage != null){
            fileServiceContainer.setCurrentFileServiceType(StorageTypeDTO.valueOf(currentStorage.getValue()));
        }
    }

    private StorageOutdatedFilesInfoDTO getInfoAboutOutdatedFiles(StorageTypeDTO storage, LocalDate limit){
        FileService fileService = fileServiceContainer.getFileService(storage);
        StorageOutdatedFilesInfoDTO result = fileService.getOutdatedFilesInfo(limit);
        result.setStorage(storage);
        result.setLimitDate(limit.toString());
        return result;
    }

    private OutdatedFilesDeleteResultDTO deleteOutdatedFiles(StorageTypeDTO storage, LocalDate limit){
        long startTime = LocalDateTime.now().toDate().getTime();
        FileService fileService = fileServiceContainer.getFileService(storage);
        OutdatedFilesDeleteResultDTO result = fileService.deleteOutdatedFiles(limit);
        result.setStorage(storage);
        result.setLimitDate(limit.toString());
        result.setSecondsSpent((LocalDateTime.now().toDate().getTime() - startTime)/1000);
        return result;
    }
}
