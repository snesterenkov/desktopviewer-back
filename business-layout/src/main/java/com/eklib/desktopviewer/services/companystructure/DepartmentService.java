package com.eklib.desktopviewer.services.companystructure;

import com.eklib.desktopviewer.dto.companystructure.DepartmentDTO;
import com.eklib.desktopviewer.dto.companystructure.DepartmentDetailDTO;
import com.eklib.desktopviewer.dto.enums.StatusDTO;

import java.util.Collection;

public interface DepartmentService {

    DepartmentDetailDTO insert(DepartmentDTO departmentDTO, String client);

    DepartmentDetailDTO update(Long id, DepartmentDTO departmentDTO, String client);

    void delete(Long id, String client);

    DepartmentDetailDTO findById(Long id, String client);

    Collection<DepartmentDetailDTO> findAll(String client);

    DepartmentDetailDTO changeStatus(Long id, StatusDTO newStatus, String client);

    Collection<DepartmentDTO> findOpen(String client);

    DepartmentDetailDTO detailUpdate(Long id, DepartmentDetailDTO departmentDetailDTO, String client);

    void deleteUserDepartmentRole(Long id, String client);
}
