package com.eklib.desktopviewer.services.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailParseException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;

@Service
@Transactional
public class MailServiceImpl implements MailService {

    @Value("${EMAIL_FROM}")
    private String from;
    @Autowired
    private JavaMailSender mailSender;

    @Override
    public void sendMailForRestorePassword(String email, String restoreLink) {
        String subject = "Desktopviewer password restore";
        String text = String.format(restorePasswordTemplate, restoreLink, restoreLink);
        MimeMessage message = createMessage(from, email, subject, text);
        mailSender.send(message);
    }

    @Override
    public void sendMailForConfirmRegistration(String email, String confirmLink) {
        String subject = "Desktopviewer sign up confirm";
        String text = String.format(mailConfirmTemplate, confirmLink, confirmLink);
        MimeMessage message = createMessage(from, email, subject, text);
        mailSender.send(message);
    }

    @Override
    public void sendMailForConfirmInvite(String email, String confirmLink, String structureName) {
        String subject = "Desktoviewer invite confirm";
        String text = String.format(inviteConfirmTemplate, structureName, confirmLink, confirmLink);
        MimeMessage message = createMessage(from, email, subject, text);
        mailSender.send(message);
    }

    @Override
    public void sendMailForInviteAuthor(String email, String invitedEmail, String structureName) {
        String subject = "Desktopviewer invite created";
        String text = String.format(inviteAuthorTemplate, invitedEmail, structureName);
        MimeMessage message = createMessage(from, email, subject, text);
        mailSender.send(message);
    }

    private MimeMessage createMessage(String form, String to, String subject, String text){
        ExtendedMimeMailMessage message = new ExtendedMimeMailMessage(mailSender.createMimeMessage());
        message.setFrom(from);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text, true);
        return message.getMimeMessage();
    }

    private String inviteConfirmTemplate =
            "<h3>Hello</h3>\n" +
            "<br/>\n" +
            "<p>You were invited in %s </p>\n" +
            "<p>To confirm invite go to: <a target=\"blank\" href=\"%s\">%s</a></p>\n" +
            "<br/>";

    private String inviteAuthorTemplate =
            "<h3>Hello</h3>\n" +
            "<br/>\n" +
            "<p>You invite %s to %s.</p>\n" +
            "<br/>";

    private String mailConfirmTemplate =
            "<h3>Hello</h3>\n" +
            "<br/>\n" +
            "<p>You need confirm yor sign up.</p>\n" +
            "<p>To do this go to: <a target=\"blank\" href=\"%s\">%s</a></p>\n" +
            "<br/>";

    private String restorePasswordTemplate =
            "<h3>Hello</h3>\n" +
            "<br/>\n" +
            "<p>You want restore your password.</p>\n" +
            "<p>To do this go to: <a target=\"blank\" href=\"%s\">%s</a></p>\n" +
            "<br/>";

    private class ExtendedMimeMailMessage extends MimeMailMessage{

        public ExtendedMimeMailMessage(MimeMessageHelper mimeMessageHelper) {
            super(mimeMessageHelper);
        }

        public ExtendedMimeMailMessage(MimeMessage mimeMessage) {
            super(mimeMessage);
        }

        public void setText(String text, boolean html) throws MailParseException{
            try{
                getMimeMessageHelper().setText(text, html);
            } catch (MessagingException var3){
                throw new MailParseException(var3);
            }
        }
    }
}
