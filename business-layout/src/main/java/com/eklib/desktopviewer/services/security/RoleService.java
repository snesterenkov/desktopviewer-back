package com.eklib.desktopviewer.services.security;

import com.eklib.desktopviewer.dto.security.RoleDTO;

import java.util.List;

/**
 * Created by s.sheman on 12.10.2015.
 */
public interface RoleService {

    RoleDTO createRole(RoleDTO roleDTO);

    void delete(Long id);

    List<RoleDTO> getAvailableRoles();

    RoleDTO update(Long id, RoleDTO dto);

    List<RoleDTO> getRolesForStructureType(Long structureTypeId);
}
