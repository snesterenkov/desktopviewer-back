package com.eklib.desktopviewer.services.snapshot;

import com.eklib.desktopviewer.container.service.FileServiceContainer;
import com.eklib.desktopviewer.convertor.fromdto.companystructure.CompanyFromDTO;
import com.eklib.desktopviewer.convertor.fromdto.snapshot.SnapshotFromDTO;
import com.eklib.desktopviewer.convertor.todto.snapshot.SnapshotToDTO;
import com.eklib.desktopviewer.convertor.todto.snapshot.UsersStatsToMapWithDTO;
import com.eklib.desktopviewer.dto.BaseDTO;
import com.eklib.desktopviewer.dto.companystructure.CompanyDTO;
import com.eklib.desktopviewer.dto.companystructure.ProjectDTO;
import com.eklib.desktopviewer.dto.enums.ProductivityDTO;
import com.eklib.desktopviewer.dto.other.PlaceInCompanyDTO;
import com.eklib.desktopviewer.dto.snapshot.SnapshotDTO;
import com.eklib.desktopviewer.dto.snapshot.UserStatsDTO;
import com.eklib.desktopviewer.persistance.model.application.DepartmentApplicationProductivityEntity;
import com.eklib.desktopviewer.persistance.model.companystructure.CompanyEntity;
import com.eklib.desktopviewer.persistance.model.companystructure.ProjectEntity;
import com.eklib.desktopviewer.persistance.model.enums.ProductivityEnum;
import com.eklib.desktopviewer.persistance.model.enums.StorageTypeEnum;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.model.snapshot.ImageEntity;
import com.eklib.desktopviewer.persistance.model.snapshot.SnapshotEntity;
import com.eklib.desktopviewer.persistance.repository.application.DepartmentApplicationProductivityRepository;
import com.eklib.desktopviewer.persistance.repository.companystructure.ProjectRepository;
import com.eklib.desktopviewer.persistance.repository.security.UserRepository;
import com.eklib.desktopviewer.persistance.repository.snapshot.ImageRepository;
import com.eklib.desktopviewer.persistance.repository.snapshot.SnapshotRepository;
import com.eklib.desktopviewer.util.ImageUtil;
import com.google.common.collect.FluentIterable;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class SnapshotServiceImpl implements SnapshotService {

    private final int resizedImageWidth = 400;
    private final int resizedImageHeight = 300;

    @Value("${REQUIRED_TRACKER_VERSION}")
    private Integer requiredTrackerVersion;
    @Autowired
    private SnapshotRepository snapshotRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private ImageRepository imageRepository;
    @Autowired
    private DepartmentApplicationProductivityRepository departmentApplicationProductivityRepository;
    @Autowired
    private FileServiceContainer fileServiceContainer;
    @Autowired
    private SnapshotFromDTO snapshotFromDTO;
    @Autowired
    private SnapshotToDTO snapshotToDTO;
    @Autowired
    private CompanyFromDTO companyFromDTO;
    @Autowired
    private UsersStatsToMapWithDTO usersStatsToMapWithDTO;

    @Override
    public SnapshotDTO insert(SnapshotDTO snapshotDTO, Integer version, LocalDateTime date, String client) {
        Assert.isTrue(version >= requiredTrackerVersion, "Outdated tracker.");
        UserEntity userEntity = userRepository.getUserByName(client);
        Assert.notNull(userEntity, "User doesn't exist.");
        Assert.notNull(snapshotDTO.getProgectId(), "Snapshot must be attached to project.");
        ProjectEntity attachedProject = projectRepository.findById(snapshotDTO.getProgectId());
        Assert.notNull(attachedProject, "Project doesn't exist.");

        String viewLink = fileServiceContainer.getCurrentFileService().saveToFile(snapshotDTO.getFile());
        byte[] resizedImage = ImageUtil.resizeImage(snapshotDTO.getFile(), resizedImageWidth, resizedImageHeight);
        String thumbnailLink = fileServiceContainer.getCurrentFileService().saveToFile(resizedImage);
        ImageEntity attachedImage = new ImageEntity();
        Date imageExpire = date.plusMonths(attachedProject.getDepartment().getCompany().getScreenshotsSavingMonth()).toDate();
        attachedImage.setExpire(imageExpire);
        attachedImage.setViewLink(viewLink);
        attachedImage.setThumbLink(thumbnailLink);
        attachedImage.setStorageType(StorageTypeEnum.valueOf(fileServiceContainer.getCurrentFileServiceType().toString()));
        attachedImage = imageRepository.insert(attachedImage);

        SnapshotEntity snapshot = snapshotFromDTO.apply(snapshotDTO);
        snapshot.setDate(date.toDate());
        snapshot.setUser(userEntity);
        snapshot.setImage(attachedImage);
        DepartmentApplicationProductivityEntity dap = departmentApplicationProductivityRepository.findProductivityByApplicationNameAndProjectId(snapshot.getNote(), snapshot.getProject().getId());
        if (dap != null) {
            snapshot.setProductivity(dap.getProductivity());
        }

        if (isCurrentSnapshotNewer(date.toDate(), userEntity)) {
            snapshot = snapshotRepository.insert(snapshot);
            return snapshotToDTO.apply(snapshot);
        } else {
            attachedImage.setDeleted(LocalDate.now().toDate());
            imageRepository.update(attachedImage);
            return new SnapshotDTO();
        }
    }

    @Override
    public List<Date> getDatesWithSnapshotsByUser(Long userId) {
        return snapshotRepository.getDatesWithSnapshotsByUser(userId);
    }

    @Override
    public Map<Long, UserStatsDTO> getUsersStatsByCompanyAndDate(Long companyId, LocalDate date, String client) {
        List<Object[]> stats = snapshotRepository.getUsersStatsByCompanyAndDates(companyId, date.toDate(), date.plusDays(1).toDate());
        return usersStatsToMapWithDTO.apply(stats);
    }

    @Override
    public Map<Long, UserStatsDTO> getUsersStatsForProjectsAndDate(String client, LocalDate date, List<ProjectDTO> projects) {
        List<Long> projectsIds = projects.stream().filter(project -> project.getId() != null).map(BaseDTO::getId).collect(Collectors.toList());
        List<Object[]> stats = snapshotRepository.getUsersStatsForProjectsAndDates(date.toDate(), date.plusDays(1).toDate(), projectsIds);
        return usersStatsToMapWithDTO.apply(stats);
    }

    @Override
    public List<SnapshotDTO> findSnapshotsByUserAndDates(Long userId, LocalDate startDate, LocalDate endDate, String client) {
        List<SnapshotEntity> snapshotEntities = snapshotRepository.findByUserIdAndDates(userId, startDate.toDate(), endDate.plusDays(1).toDate());
        UserEntity clientEntity = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(clientEntity);
        List<SnapshotDTO> resultSnapshots = new ArrayList<>();
        if (userId.equals(clientEntity.getId())) {
            resultSnapshots = snapshotEntities.stream().map(snapshotToDTO::apply).collect(Collectors.toList());
            resultSnapshots.forEach(snapshot -> {
                snapshot.setCanDelete(true);
                snapshot.setCanChange(false);
            });
            return resultSnapshots;
        } else if (isSuperUser) {
            resultSnapshots = snapshotEntities.stream().map(snapshotToDTO::apply).collect(Collectors.toList());
            resultSnapshots.forEach(snapshot -> {
                snapshot.setCanDelete(true);
                snapshot.setCanChange(true);
            });
            return resultSnapshots;
        } else {//todo check admin project
            for (SnapshotEntity snapshot : snapshotEntities) {
                SnapshotDTO resultSnapshot = snapshotToDTO.apply(snapshot);
                if (projectRepository.isProjectAdmin(snapshot.getProject(), clientEntity)) {
                    resultSnapshot.setCanChange(true);
                    resultSnapshot.setCanDelete(true);
                } else {
                    resultSnapshot.setCanChange(false);
                    resultSnapshot.setCanDelete(false);
                }
                resultSnapshots.add(resultSnapshot);
            }
            return resultSnapshots;
        }
    }

    @Override
    public PlaceInCompanyDTO calculatePlacesInCompaniesForEmployee(Long userId, CompanyDTO companyDTO, LocalDate startDate, LocalDate endDate, String client) {
        Assert.notNull(companyDTO, "Company not set.");
        UserEntity employee = userRepository.findById(userId);
        Assert.notNull(employee, "Employee doesn't exist.");
        CompanyEntity company = companyFromDTO.apply(companyDTO);
        PlaceInCompanyDTO result = new PlaceInCompanyDTO();
        result.setCompanyName(company.getName());
        List<UserEntity> employees = userRepository.findUsersForCompany(company.getId());
        result.setEmployeesCount(employees.size());
        Map<Long, UserStatsDTO> stats = usersStatsToMapWithDTO.apply(snapshotRepository.getUsersStatsByCompanyAndDates(company.getId(), startDate.toDate(), endDate.plusDays(1).toDate()));
        Set<Long> efficiencyMulSnapshotsCountSet = stats.keySet().stream()
                .map(key -> stats.get(key).getEfficiency() * stats.get(key).getSnapshotsCount())
                .collect(Collectors.toSet());
        if (efficiencyMulSnapshotsCountSet.isEmpty()) {
            return result;
        }
        int place = 1;
        if (stats.get(userId) == null) {
            place = efficiencyMulSnapshotsCountSet.size() + 1;
        } else {
            for (Long efficiency : efficiencyMulSnapshotsCountSet) {
                if ((stats.get(userId).getEfficiency() * stats.get(userId).getSnapshotsCount()) < efficiency) {
                    place++;
                }
            }
        }
        result.setEmployeePlace(place);
        return result;
    }

    @Override
    public void changeSnapshotsProductivity(List<SnapshotDTO> snapshots, ProductivityDTO productivity, String client) {
        UserEntity clientEntity = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(clientEntity);
        List<SnapshotEntity> snapshotEntities = FluentIterable.from(snapshots).transform(snapshotFromDTO).toList();
        ProductivityEnum productivityEntity = ProductivityEnum.valueOf(productivity.toString());
        for (SnapshotEntity snapshotEntity : snapshotEntities) {
            boolean isProjectAdmin = projectRepository.isProjectAdmin(snapshotEntity.getProject(), clientEntity);
            if (isSuperUser || isProjectAdmin) {
                snapshotEntity.setProductivity(productivityEntity);
                snapshotRepository.update(snapshotEntity);
            } else {
                Assert.isTrue(false, "You haven't required permissions.");
            }
        }
    }

    @Override
    public void deleteSnapshots(List<SnapshotDTO> snapshots, String client) {
        UserEntity clientEntity = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(clientEntity);
        List<SnapshotEntity> snapshotEntities = FluentIterable.from(snapshots).transform(snapshotFromDTO).toList();
        for (SnapshotEntity snapshotEntity : snapshotEntities) {
            boolean isProjectAdmin = projectRepository.isProjectAdmin(snapshotEntity.getProject(), clientEntity);
            if (isSuperUser || isProjectAdmin || snapshotEntity.getUser().getLogin().equals(client)) {
                snapshotEntity.getImage().setDeleted(LocalDate.now().toDate());
                imageRepository.update(snapshotEntity.getImage());
                snapshotRepository.delete(snapshotEntity);
            } else {
                Assert.isTrue(false, "You haven't required permissions.");
            }
        }
    }

    private boolean isCurrentSnapshotNewer(Date date, UserEntity userEntity) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, -(calendar.get(Calendar.MINUTE) % 10));
        Date startDate = calendar.getTime();
        calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, -(calendar.get(Calendar.MINUTE) % 10) + 10);
        Date endDate = calendar.getTime();
        SnapshotEntity snapshotEntity = snapshotRepository.findSnapshotByUserIdAndProjectIdAndPeriod(userEntity.getId(), startDate, endDate);
        if (snapshotEntity.getId() == null) {
            return true;
        } else {
            if (snapshotEntity.getDate().after(date)) {
                return false;
            } else {
                snapshotRepository.delete(snapshotEntity);
                return true;
            }
        }
    }
}
