package com.eklib.desktopviewer.services.security;

import com.eklib.desktopviewer.convertor.fromdto.security.RoleFromDTO;
import com.eklib.desktopviewer.convertor.todto.security.RoleToDTO;
import com.eklib.desktopviewer.dto.security.RoleDTO;
import com.eklib.desktopviewer.persistance.model.security.Role;
import com.eklib.desktopviewer.persistance.repository.security.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by s.sheman on 12.10.2015.
 */
@Service
@Transactional
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleFromDTO roleFromDTO;

    @Autowired
    private RoleToDTO roleToDTO;

    @Override
    public RoleDTO createRole(RoleDTO roleDTO){
        Assert.hasLength(roleDTO.getName(), "Name shouldn't be empty");
        Assert.isNull(roleRepository.getRoleByName(roleDTO.getName()), "Role with it name already exist");
        Role instantiatedRole = roleFromDTO.apply(roleDTO);
        Role newRole = roleRepository.insert(instantiatedRole);
        return roleToDTO.apply(newRole);
    }

    @Override
    public void delete(Long id){
        Role roleForDelete = roleRepository.findById(id);
        roleRepository.delete(roleForDelete);
    }

    @Override
    public List<RoleDTO> getAvailableRoles(){
        return roleRepository.findAll().stream().map(roleToDTO::apply).collect(Collectors.toList());
    }

    @Override
    public RoleDTO update(Long id, RoleDTO dto){
        dto.setId(id);
        Role instantiatedRole = roleFromDTO.apply(dto);
        Role updatedRole = roleRepository.update(instantiatedRole);
        return roleToDTO.apply(updatedRole);
    }

    @Override
    public List<RoleDTO> getRolesForStructureType(Long structureTypeId){
        return roleRepository.getRolesForStructureType(structureTypeId).stream().map(roleToDTO::apply).collect(Collectors.toList());
    }
}
