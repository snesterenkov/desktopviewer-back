package com.eklib.desktopviewer.services.security;

import com.eklib.desktopviewer.convertor.fromdto.security.PermissionFromDTO;
import com.eklib.desktopviewer.convertor.todto.security.PermissionToDTO;
import com.eklib.desktopviewer.dto.security.PermissionDTO;
import com.eklib.desktopviewer.persistance.model.security.PermissionEntity;
import com.eklib.desktopviewer.persistance.repository.security.PermissionRepository;
import com.eklib.desktopviewer.persistance.repository.security.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Лол on 09.10.2015.
 */
@Service
@Transactional
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PermissionToDTO permissionToDTO;

    @Autowired
    private PermissionFromDTO permissionFromDTO;

    @Override
    public PermissionDTO createPermission(PermissionDTO permissionForSave) {
        Assert.hasLength(permissionForSave.getName(), "Name should not be empty");
        Assert.hasLength(permissionForSave.getCode(), "Code should not be empty");
        Assert.isNull(permissionRepository.getPermissionByName(permissionForSave.getName()), "Permission with this name already exist");

        PermissionEntity permission = permissionFromDTO.apply(permissionForSave);
        PermissionEntity newPermission = permissionRepository.insert(permission);
        return permissionToDTO.apply(newPermission);
    }

    @Override
    public List<PermissionDTO> getAvailablePermission(){
        return permissionRepository.findAll().stream()
                .map(permissionToDTO::apply)
                .collect(Collectors.toList());
    }

    @Override
    public void delete(Long id){
        PermissionEntity permissionForDelete = permissionRepository.findById(id);
        permissionRepository.delete(permissionForDelete);
    }

    @Override
    public PermissionDTO update(Long id, PermissionDTO dto){
        dto.setId(id);
        PermissionEntity createdEntity = permissionFromDTO.apply(dto);
        PermissionEntity updatedEntity = permissionRepository.update(createdEntity);
        return permissionToDTO.apply(updatedEntity);
    }
}
