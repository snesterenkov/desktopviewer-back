package com.eklib.desktopviewer.services.application;

import com.eklib.desktopviewer.dto.application.ApplicationDTO;
import com.eklib.desktopviewer.dto.enums.ProductivityDTO;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 20.10.2015.
 */
public interface ApplicationService {

    ApplicationDTO update(Long id, ApplicationDTO applicationDTO, String client);

    ApplicationDTO insert(ApplicationDTO applicationDTO, String client);

    ApplicationDTO findById(Long id, String client);

    Collection<ApplicationDTO> findAll();

    void delete(Long id, String client);

    Map<ProductivityDTO, List<ApplicationDTO>> findForDepartment(Long departmentId, String client);

    List<ApplicationDTO> findDepartmentFree(Long departmentId, String client);

    void deleteApplications(List<ApplicationDTO> applications, String client);

    void attachToDepartment(Long departmentId, List<ApplicationDTO> applications, ProductivityDTO productivity, String client);
}
