package com.eklib.desktopviewer.services.file;

import com.eklib.desktopviewer.dto.settings.file.OutdatedFilesDeleteResultDTO;
import com.eklib.desktopviewer.dto.settings.file.StorageOutdatedFilesInfoDTO;
import org.joda.time.LocalDate;

public interface FileService {

    String saveToFile(byte[] data);

    boolean deleteFileByLink(String link);

    StorageOutdatedFilesInfoDTO getOutdatedFilesInfo(LocalDate limitDate);

    OutdatedFilesDeleteResultDTO deleteOutdatedFiles(LocalDate limitDate);
}
