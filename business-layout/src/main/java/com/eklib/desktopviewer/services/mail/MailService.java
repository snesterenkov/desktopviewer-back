package com.eklib.desktopviewer.services.mail;


public interface MailService {

    void sendMailForRestorePassword(String email, String restoreLink);

    void sendMailForConfirmRegistration(String email, String confirmLink);

    void sendMailForConfirmInvite(String email, String confirmLink, String structureName);

    void sendMailForInviteAuthor(String email, String invitedEmail, String structureName);
}
