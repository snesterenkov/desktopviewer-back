package com.eklib.desktopviewer.services.security;

import com.eklib.desktopviewer.convertor.fromdto.security.UserFromDTO;
import com.eklib.desktopviewer.convertor.fromdto.security.UserFromDetailDTO;
import com.eklib.desktopviewer.convertor.todto.security.UserToDTO;
import com.eklib.desktopviewer.convertor.todto.security.UserToDetailDTO;
import com.eklib.desktopviewer.dto.security.AuthenticableDTO;
import com.eklib.desktopviewer.dto.security.RoleEntityDTO;
import com.eklib.desktopviewer.dto.security.UserDTO;
import com.eklib.desktopviewer.dto.security.UserDetailDTO;
import com.eklib.desktopviewer.persistance.model.companystructure.ProjectEntity;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.repository.companystructure.ProjectRepository;
import com.eklib.desktopviewer.persistance.repository.security.InviteRepository;
import com.eklib.desktopviewer.persistance.repository.security.UserRepository;
import com.eklib.desktopviewer.services.mail.MailService;
import com.eklib.desktopviewer.services.settings.client.ClientSettingsService;
import com.eklib.desktopviewer.util.TokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserServicesImpl implements UserServices {

    @Autowired
    private UserToDTO userToDTO;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private InviteRepository inviteRepository;
    @Autowired
    private UserFromDTO userFromDTO;
    @Autowired
    private UserFromDetailDTO userDetailFromDTO;
    @Autowired
    private UserToDetailDTO userToDetailDTO;
    @Autowired
    private MailService mailService;
    @Autowired
    private ClientSettingsService clientSettingsService;

    @Override
    public UserDTO findById(Long id, String client) {
        UserEntity user = userRepository.findById(id);
        Assert.notNull(user, "User doesn't exist.");

        UserEntity clientEntity = userRepository.getUserByName(client);

        boolean isSuperUser = userRepository.isSuperUser(clientEntity);
        boolean isAdminProject = user.getUserProjectRoles().stream()
                .anyMatch(userProjectRole -> projectRepository.isProjectAdmin(userProjectRole.getProject(), clientEntity));
        Assert.isTrue(isSuperUser || isAdminProject, "You haven't required permissions.");

        UserDTO dto = userToDTO.apply(user);
        if (dto != null) {
            dto.setIsSuperUser(userRepository.isSuperUser(user));
        }
        return dto;
    }

    @Override
    public List<UserDTO> findAll(String client) {
        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        Assert.isTrue(isSuperUser, "You haven't required permissions.");
        List<UserEntity> allUsers = userRepository.findAll();
        return allUsers.stream()
                .map(u -> {
                    UserDTO dto = userToDTO.apply(u);
                    if (dto != null) {
                        dto.setIsSuperUser(userRepository.isSuperUser(u));
                    }
                    return dto;
                })
                .collect(Collectors.toList());
    }

    @Override
    public Set<UserDTO> findFreeUsers(Long projectId, String client) {
        UserEntity user = userRepository.getUserByName(client);
        ProjectEntity projectEntity = projectRepository.findById(projectId);
        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean isAdminProject = projectRepository.isProjectAdmin(projectEntity, user);
        Assert.isTrue(isSuperUser | isAdminProject, "You haven't required permissions.");

        /*Set<UserDTO> usersOnProject = projectEntity.getUserProjectRoles().stream()
                .map(UserProjectRole::getUser)
                .map(userToDetailDTO::apply)
                .collect(Collectors.toSet());

        return projectEntity.getUserProjectRoles().stream()
                .map(UserProjectRole::getUser)
                .map(userToDetailDTO::apply)
                .collect(Collectors.toSet());*/
        Long companyId = projectEntity.getDepartment().getCompany().getId();
        return userRepository.findFreeProjectUsers(projectId, companyId).stream()
                .map(userToDetailDTO::apply)
                .collect(Collectors.toSet());
    }

    @Override
    public List<UserDTO> findFreeUsersForDepartment(Long departmentId, String client) {
        return userRepository.findFreeUsersForDepartment(departmentId).stream()
                .map(userToDTO::apply)
                .collect(Collectors.toList());
    }

    @Override
    public List<UserDTO> findFreeUsersForCompany(Long companyId, String client) {
        return userRepository.findFreeUsersForCompany(companyId).stream()
                .map(userToDTO::apply)
                .collect(Collectors.toList());
    }

    @Override
    public void delete(Long id, String client) {
        UserEntity clientEntity = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(clientEntity);
        Assert.isTrue(isSuperUser, "You haven't required permissions.");
        UserEntity user = userRepository.findById(id);
        Assert.notNull(user, "User doesn't exist.");
        userRepository.delete(user);
    }

    //todo : rewrite
    @Override
    public AuthenticableDTO findAuthenticable(String login) {
        UserEntity user = userRepository.getUserByName(login);
        if (user == null) {
            return new AuthenticableDTO();
        }
        AuthenticableDTO authenticableDTO = new AuthenticableDTO();
        authenticableDTO.setPassphrase(user.getPassword());
        Set<RoleEntityDTO> roleEntityDTOs = user.readRoles().stream().map(entity -> RoleEntityDTO.valueOf(entity.name())).collect(Collectors.toSet());
        authenticableDTO.setRoles(roleEntityDTOs);
        return authenticableDTO;
    }

    @Override
    public UserDTO createUser(UserDetailDTO userDetailDTO) {
        validateUserDTO(userDetailDTO);
        Assert.isNull(userDetailDTO.getId(), "Id is not null");
        Assert.hasLength(userDetailDTO.getPassword(), "Password must not be null and not the empty String.");

        UserEntity newUser = userDetailFromDTO.apply(userDetailDTO);
        Assert.notNull(newUser, "User must not be null.");

        long invitesCount = inviteRepository.getInvitesCountForEmail(newUser.getEmail());
        if (invitesCount < 1) {
            String confirmationToken = TokenUtil.createToken();
            newUser.setConfirmUserToken(confirmationToken);
            mailService.sendMailForConfirmRegistration(
                    newUser.getEmail(), clientSettingsService.get().getRegistrationConfirmURL() + confirmationToken
            );
        }

        userRepository.insert(newUser);
        return userToDTO.apply(newUser);
    }

    @Override
    public UserDetailDTO getAutorizedUser(String login) {
        UserEntity user = userRepository.getUserByName(login);
        Assert.isNull(user.getConfirmUserToken(), "Only confirmed user can login.");
        UserDetailDTO userDTO = userToDetailDTO.apply(user);
        Assert.notNull(userDTO, "User doesn't exists.");
        userDTO.setPassword(null);
        return userDTO;
    }

    @Override
    public UserDTO update(Long id, UserDTO dto, String client) {
        UserEntity user = userRepository.findById(id);
        Assert.notNull(user, "User doesn't exist.");

        UserEntity clientEntity = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(clientEntity);
        boolean isSelfUpdate = user.getId().equals(clientEntity.getId());
        Assert.isTrue(isSuperUser || isSelfUpdate, "You haven't required permissions.");

        UserEntity newUser = userFromDTO.apply(dto);
        if (isSuperUser && newUser != null)
            newUser.setPrincipalRole(clientEntity.getPrincipalRole());
        user = userRepository.update(newUser);

        UserDTO updatedDTO = userToDTO.apply(user);
        if (updatedDTO != null) {
            updatedDTO.setIsSuperUser(userRepository.isSuperUser(user));
        }
        return updatedDTO;
    }

    @Override
    public void makeAdmin(Long id, String client) {
        UserEntity user = userRepository.findById(id);
        Assert.notNull(user, "User doesn't exist.");

        UserEntity clientEntity = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(clientEntity);
        Assert.isTrue(isSuperUser, "You haven't required permissions.");

        user.setPrincipalRole(clientEntity.getPrincipalRole());
        userRepository.update(user);
    }

    @Override
    public void makeNonAdmin(Long id, String client) {
        UserEntity user = userRepository.findById(id);
        Assert.notNull(user, "User doesn't exist.");
        UserEntity clientEntity = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(clientEntity);
        Assert.isTrue(isSuperUser, "You haven't required permissions.");

        user.setPrincipalRole(null);
        userRepository.update(user);
    }

    @Override
    public void requestOnChangingPassword(String email) {
        UserEntity user = userRepository.getUserByName(email);
        Assert.notNull(user, "User can not be find.");

        String changePasswordToken = TokenUtil.createToken();
        user.setChangePasswordToken(changePasswordToken);
        mailService.sendMailForRestorePassword(
                email, clientSettingsService.get().getPasswordRestoreURL() + changePasswordToken
        );
        userRepository.update(user);
    }

    @Override
    public void changePassword(String password, String token) {
        UserEntity user = userRepository.getUserByToken(token);
        Assert.notNull(user, "Wrong token.");
        user.setPassword(password);
        user.setChangePasswordToken(null);
        userRepository.update(user);
    }

    @Override
    public void confirmUser(String token) {
        UserEntity user = userRepository.getUserByConfirmationToken(token);
        Assert.notNull(user, "User doesn't exist or already confirmed.");
        user.setConfirmUserToken(null);
        userRepository.update(user);
    }

    private void validateUserDTO(UserDTO userDTO) {
        Assert.hasLength(userDTO.getLogin(), "Login must not be null and not the empty String.");
        UserEntity userWithSameLogin = userRepository.getUserByName(userDTO.getLogin());
        Assert.isNull(userWithSameLogin, "User with same login already exists.");

        Assert.hasLength(userDTO.getEmail(), "Email must not be null and not the empty String.");
        userWithSameLogin = userRepository.getUserByName(userDTO.getEmail());
        Assert.isNull(userWithSameLogin, "User with same email already exists.");

        Assert.hasLength(userDTO.getFirstName(), "First name must not be null and not the empty String.");
        Assert.hasLength(userDTO.getLastName(), "Last name must not be null and not the empty String.");
    }

}
