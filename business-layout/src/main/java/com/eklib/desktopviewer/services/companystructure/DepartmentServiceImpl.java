package com.eklib.desktopviewer.services.companystructure;

import com.eklib.desktopviewer.convertor.fromdto.companystructure.DepartmentFromDTO;
import com.eklib.desktopviewer.convertor.fromdto.companystructure.DepartmentFromDetailDTO;
import com.eklib.desktopviewer.convertor.fromdto.security.UserFromDTO;
import com.eklib.desktopviewer.convertor.todto.companystructure.DepartmentToDTO;
import com.eklib.desktopviewer.convertor.todto.companystructure.DepartmentToDetailDTO;
import com.eklib.desktopviewer.dto.companystructure.DepartmentDTO;
import com.eklib.desktopviewer.dto.companystructure.DepartmentDetailDTO;
import com.eklib.desktopviewer.dto.enums.StatusDTO;
import com.eklib.desktopviewer.persistance.model.companystructure.CompanyEntity;
import com.eklib.desktopviewer.persistance.model.companystructure.DepartmentEntity;
import com.eklib.desktopviewer.persistance.model.companystructure.ProjectEntity;
import com.eklib.desktopviewer.persistance.model.enums.StatusEnum;
import com.eklib.desktopviewer.persistance.model.security.Role;
import com.eklib.desktopviewer.persistance.model.security.UserDepartmentRole;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.model.security.UserProjectRole;
import com.eklib.desktopviewer.persistance.repository.companystructure.CompanyRepository;
import com.eklib.desktopviewer.persistance.repository.companystructure.DepartmentRepository;
import com.eklib.desktopviewer.persistance.repository.companystructure.UserDepartmentRoleRepository;
import com.eklib.desktopviewer.persistance.repository.security.RoleRepository;
import com.eklib.desktopviewer.persistance.repository.security.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private UserFromDTO userFromDTO;
    @Autowired
    private DepartmentFromDTO departmentFromDTO;
    @Autowired
    private DepartmentToDTO departmentToDTO;
    @Autowired
    private DepartmentFromDetailDTO departmentFromDetailDTO;
    @Autowired
    private DepartmentToDetailDTO departmentToDetailDTO;
    @Autowired
    private DepartmentRepository departmentRepository;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserDepartmentRoleRepository userDepartmentRoleRepository;


    @Override
    public DepartmentDetailDTO insert(DepartmentDTO departmentDTO, String client) {
        Assert.isNull(departmentDTO.getId(), "Id must be null.");
        validateDepartmentDTO(departmentDTO);
        DepartmentEntity newDepartment = departmentFromDTO.apply(departmentDTO);
        Assert.notNull(newDepartment, "Department must not be null.");

        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean canCreateDepartmentForCompany = canCreateDepartmentForCompany(newDepartment.getCompany(), user);
        Assert.isTrue(isSuperUser || canCreateDepartmentForCompany, "You haven't required permissions.");

        newDepartment.setOwner(user);
        departmentRepository.insert(newDepartment);

        Role adminDepartmentRole = roleRepository.getRoleByName(RoleRepository.ADMIN_DEPARTMENT_ROLE_NAME);
        Assert.notNull(adminDepartmentRole, "Required role doesn't exist.");

        attachUserToDepartmentWithRole(user, newDepartment, adminDepartmentRole);

        DepartmentDetailDTO dto = departmentToDetailDTO.apply(newDepartment);
        if (dto != null) {
            dto.setAdmin(true);
        }
        return dto;
    }

    @Override
    public DepartmentDetailDTO update(Long id, DepartmentDTO departmentDTO, String client) {
        DepartmentEntity department = departmentRepository.findById(id);
        Assert.notNull(department, "Department doesn't exist.");

        departmentDTO.setId(id);
        DepartmentEntity updateDepartment = departmentFromDTO.apply(departmentDTO);
        Assert.notNull(updateDepartment, "Department must not be null.");

        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean isDepartmentAdmin = departmentRepository.isDepartmentAdmin(department, user);
        boolean canCreateDepartmentForCompany = canCreateDepartmentForCompany(updateDepartment.getCompany(), user);
        Assert.isTrue(isSuperUser || (isDepartmentAdmin && canCreateDepartmentForCompany), "You haven't required permissions.");

        department = departmentRepository.update(updateDepartment);
        DepartmentDetailDTO dto = departmentToDetailDTO.apply(department);
        if (dto != null) {
            dto.setAdmin(true);
        }
        return dto;
    }

    @Override
    public void delete(Long id, String client) {
        UserEntity clientEntity = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(clientEntity);
        Assert.isTrue(isSuperUser, "You haven't required permissions.");

        DepartmentEntity department = departmentRepository.findById(id);
        Assert.notNull(department, "Department doesn't exist.");

        departmentRepository.delete(department);
    }

    @Override
    public DepartmentDetailDTO findById(Long id, String client) {
        DepartmentEntity department = departmentRepository.findById(id);
        Assert.notNull(department, "Department doesn't exist.");

        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean isDepartmentAdmin = departmentRepository.isDepartmentAdmin(department, user);
        Assert.isTrue(isSuperUser || isDepartmentAdmin, "You haven't required permissions.");

        DepartmentDetailDTO dto = departmentToDetailDTO.apply(department);
        if (dto != null) {
            dto.setAdmin(true);
        }
        return dto;
    }

    @Override
    public Collection<DepartmentDetailDTO> findAll(String client) {
        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        if (isSuperUser) {
            List<DepartmentEntity> allDepartments = departmentRepository.findAll();
            return allDepartments.stream()
                    .map(departmentToDetailDTO::apply)
                    .peek(dto -> {
                        if (dto != null) {
                            dto.setAdmin(true);
                        }
                    })
                    .collect(Collectors.toList());
        } else {
            List<DepartmentEntity> userDepartments = new ArrayList<>();
            userDepartments.addAll(getUserDepartmentsByRelatedProjects(user));
            userDepartments.addAll(getUserDepartments(user));
            return userDepartments.stream()
                    .distinct()
                    .map(department -> {
                        DepartmentDetailDTO dto = departmentToDetailDTO.apply(department);
                        if (dto != null) {
                            dto.setAdmin(departmentRepository.isDepartmentAdmin(department, user));
                        }
                        return dto;
                    })
                    .collect(Collectors.toList());
        }
    }

    @Override
    public DepartmentDetailDTO changeStatus(Long id, StatusDTO statusDTO, String client) {
        DepartmentEntity department = departmentRepository.findById(id);
        Assert.notNull(department, "Couldn't find department.");

        StatusEnum newStatus = StatusEnum.valueOf(statusDTO.name());
        StatusEnum comStatusEnum = department.getCompany().getStatus();
        if (comStatusEnum.equals(StatusEnum.OPEN)) {
            return changeStatus(department, newStatus, client);
        }

        Assert.isTrue(comStatusEnum.equals(StatusEnum.PAUSED) && newStatus.equals(StatusEnum.CLOSED),
                "Can`t change status in department because company status is " + comStatusEnum);

        return changeStatus(department, newStatus, client);
    }

    private DepartmentDetailDTO changeStatus(DepartmentEntity department, StatusEnum newStatus, String client) {
        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean isDepartmentAdmin = departmentRepository.isDepartmentAdmin(department, user);
        Assert.isTrue(isSuperUser || isDepartmentAdmin, "You haven't required permissions.");

        if (departmentRepository.changeStatus(department, newStatus)) {
            department = departmentRepository.findById(department.getId());
        }
        return departmentToDetailDTO.apply(department);
    }

    @Override
    public Collection<DepartmentDTO> findOpen(String client) {
        List<DepartmentEntity> departments = new ArrayList<>();

        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);

        if (isSuperUser) {
            departments = departmentRepository.findAll();
        } else {
            departments.addAll(getUserDepartments(user));
            departments.addAll(
                    user.getUserProjectRoles().stream()
                            .filter(upr -> RoleRepository.ADMIN_PROJECT_ROLE_NAME.equals(upr.getRole().getName()))
                            .map(UserProjectRole::getProject)
                            .map(ProjectEntity::getDepartment)
                            .collect(Collectors.toList())
            );
        }
        return departments.stream()
                .distinct()
                .filter(department -> StatusEnum.OPEN.equals(department.getStatus()))
                .map(departmentToDTO::apply)
                .collect(Collectors.toList());
    }

    @Override
    public DepartmentDetailDTO detailUpdate(Long id, DepartmentDetailDTO departmentDetailDTO, String client) {
        DepartmentEntity department = departmentRepository.findById(id);
        Assert.notNull(department, "Department doesn't exist.");

        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean isDepartmentAdmin = departmentRepository.isDepartmentAdmin(department, user);
        Assert.isTrue(isSuperUser || isDepartmentAdmin, "You haven't required permissions.");

        //todo refactoring
        departmentDetailDTO.getUserRoles().stream()
                .filter(userCompanyRoleDTO -> userCompanyRoleDTO.getId() == null && userCompanyRoleDTO.getRole().getName().equals(RoleRepository.ADMIN_DEPARTMENT_ROLE_NAME))
                .forEach(userCompanyRoleDTO -> department.getProjects()
                        .forEach(projectEntity -> {
                            UserProjectRole userProjectRole = new UserProjectRole();
                            Role adminProjectRole = roleRepository.getRoleByName(RoleRepository.ADMIN_PROJECT_ROLE_NAME);
                            userProjectRole.setUser(userFromDTO.apply(userCompanyRoleDTO.getUser()));
                            userProjectRole.setProject(projectEntity);
                            userProjectRole.setRole(adminProjectRole);
                            projectEntity.getUserProjectRoles().add(userProjectRole);
                        }));
        DepartmentEntity updatedDepartment = Objects.requireNonNull(departmentFromDetailDTO.apply(departmentDetailDTO));
        departmentRepository.merge(updatedDepartment);

        DepartmentDetailDTO dto = departmentToDetailDTO.apply(updatedDepartment);
        if (dto != null) {
            dto.setAdmin(true);
        }
        return dto;
    }

    @Override
    public void deleteUserDepartmentRole(Long id, String client) {
        UserDepartmentRole userDepartmentRole = userDepartmentRoleRepository.findById(id);
        Assert.notNull(userDepartmentRole, "UserDepartmentRole doesn't exist.");

        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean isDepartmentAdmin = departmentRepository.isDepartmentAdmin(userDepartmentRole.getDepartment(), user);
        boolean isOwnerRole = userDepartmentRole.getDepartment().getOwner().equals(userDepartmentRole.getUser()) &&
                RoleRepository.ADMIN_DEPARTMENT_ROLE_NAME.equals(userDepartmentRole.getRole().getName());
        Assert.isTrue(!isOwnerRole && (isSuperUser || isDepartmentAdmin), "You haven't required permissions.");

        userDepartmentRoleRepository.delete(userDepartmentRole);
    }

    void validateDepartmentDTO(DepartmentDTO departmentDTO) {
        Assert.hasLength(departmentDTO.getName(), "Name must not be null or empty string.");
        DepartmentEntity departmentWithSameName = departmentRepository.getDepartmentByName(departmentDTO.getName());
        Assert.isNull(departmentWithSameName, "Department with same name already exists.");

        Assert.notNull(departmentDTO.getCompanyId(), "CompanyId must not be null.");
        CompanyEntity attachingCompany = companyRepository.findById(departmentDTO.getCompanyId());
        Assert.notNull(attachingCompany, "Company doesn't exist.");
        Assert.isTrue(StatusEnum.OPEN.equals(attachingCompany.getStatus()), "Only open company could be attached to department.");
    }

    private void attachUserToDepartmentWithRole(UserEntity user, DepartmentEntity department, Role role) {
        UserDepartmentRole relation = new UserDepartmentRole();
        relation.setUser(user);
        relation.setDepartment(department);
        relation.setRole(role);
        userDepartmentRoleRepository.insert(relation);
        department.getUserDepartmentRoles().add(relation);
    }

    private List<DepartmentEntity> getUserDepartmentsByRelatedProjects(UserEntity user) {
        return user.getUserProjectRoles().stream()
                .map(UserProjectRole::getProject)
                .map(ProjectEntity::getDepartment)
                .collect(Collectors.toList());
    }

    private List<DepartmentEntity> getUserDepartments(UserEntity user) {
        return user.getUserDepartmentRoles().stream()
                .map(UserDepartmentRole::getDepartment)
                .collect(Collectors.toList());
    }

    private boolean canCreateDepartmentForCompany(CompanyEntity company, UserEntity user) {
        if (companyRepository.isCompanyAdmin(company, user)) {
            return true;
        }
        Set<DepartmentEntity> companyDepartments = company.getDepartments();
        return !companyDepartments.stream()
                .filter(department -> departmentRepository.isDepartmentAdmin(department, user))
                .collect(Collectors.toList()).isEmpty();
    }
}
