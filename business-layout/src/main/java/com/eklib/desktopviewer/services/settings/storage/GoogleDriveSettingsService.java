package com.eklib.desktopviewer.services.settings.storage;

import com.eklib.desktopviewer.dto.settings.storage.GoogleDriveSettingsDTO;

public interface GoogleDriveSettingsService {

    GoogleDriveSettingsDTO getGoogleDriveSettings(String client);

    void setGoogleDriveSettings(GoogleDriveSettingsDTO newGoogleDriveSettings, String client);
}
