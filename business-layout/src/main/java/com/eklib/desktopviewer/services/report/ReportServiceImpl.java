package com.eklib.desktopviewer.services.report;

import com.eklib.desktopviewer.convertor.fromdto.companystructure.ProjectFromDTO;
import com.eklib.desktopviewer.convertor.todto.companystructure.CompanyToExtendDTO;
import com.eklib.desktopviewer.convertor.todto.companystructure.DepartmentToExtendDTO;
import com.eklib.desktopviewer.convertor.todto.companystructure.ProjectToDTO;
import com.eklib.desktopviewer.convertor.todto.report.PeriodToDTO;
import com.eklib.desktopviewer.convertor.todto.report.WorkDiaryToDTO;
import com.eklib.desktopviewer.convertor.todto.security.UserToDTO;
import com.eklib.desktopviewer.dto.companystructure.CompanyExtendDTO;
import com.eklib.desktopviewer.dto.companystructure.DepartmentExtendDTO;
import com.eklib.desktopviewer.dto.companystructure.ProjectDTO;
import com.eklib.desktopviewer.dto.report.PeriodDTO;
import com.eklib.desktopviewer.dto.report.WorkDiaryDTO;
import com.eklib.desktopviewer.dto.security.UserDTO;
import com.eklib.desktopviewer.persistance.model.companystructure.ProjectEntity;
import com.eklib.desktopviewer.persistance.model.enums.PeriodEnum;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.model.security.UserProjectRole;
import com.eklib.desktopviewer.persistance.repository.companystructure.ProjectRepository;
import com.eklib.desktopviewer.persistance.repository.companystructure.UserProjectRoleRepository;
import com.eklib.desktopviewer.persistance.repository.security.UserRepository;
import com.eklib.desktopviewer.persistance.repository.snapshot.SnapshotRepository;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by alex on 4/1/2015.
 */
@Service
@Transactional
public class ReportServiceImpl implements ReportService {

    private static final int COUNT_MINUTES_IN_HOURS = 60;
    private static final int COUNTS_MINUTES_BETWEEN_TWO_SNAPSHOTS = 10;

    @Autowired
    private SnapshotRepository snapshotRepository;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserProjectRoleRepository userProjectRoleRepository;
    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private WorkDiaryToDTO workDiaryToDTO;
    @Autowired
    private PeriodToDTO periodToDTO;
    @Autowired
    private UserToDTO userToDTO;
    @Autowired
    private ProjectFromDTO projectFromDTO;
    @Autowired
    private ProjectToDTO projectToDTO;
    @Autowired
    private CompanyToExtendDTO companyToExtendDTO;
    @Autowired
    private DepartmentToExtendDTO departmentToExtendDTO;

    @Override
    public Collection<CompanyExtendDTO> findAdminProjectsHierarchyByAdminLogin(String login) {
        Set<ProjectEntity> adminProjects = new HashSet<>(projectRepository.findByProjectAdmin(login));
        Set<ProjectEntity> departmentAdminProjects = new HashSet<>(projectRepository.findByDepartmentAdmin(login));
        Set<ProjectEntity> companyAdminProjects = new HashSet<>(projectRepository.findByCompanyAdmin(login));
        Set<ProjectEntity> allAdminProjects = new HashSet<>();
        allAdminProjects.addAll(adminProjects);
        allAdminProjects.addAll(departmentAdminProjects);
        allAdminProjects.addAll(companyAdminProjects);

        List<ProjectDTO> projects = allAdminProjects.stream()
                .map(projectToDTO::apply)
                .collect(Collectors.toList());

        Map<Long, CompanyExtendDTO> companies = new HashMap<>();
        Map<Long, DepartmentExtendDTO> departments = new HashMap<>();
        CompanyExtendDTO tempCompanyDTO;
        DepartmentExtendDTO tempDepartmentDTO;
        for (ProjectEntity project : allAdminProjects) {
            tempCompanyDTO = companyToExtendDTO.apply(project.getDepartment().getCompany());
            if (tempCompanyDTO != null) {
                companies.put(tempCompanyDTO.getId(), tempCompanyDTO);
            }

            tempDepartmentDTO = departmentToExtendDTO.apply(project.getDepartment());
            if (tempDepartmentDTO != null) {
                departments.put(tempDepartmentDTO.getId(), tempDepartmentDTO);
            }
        }

        for (ProjectDTO project : projects) {
            departments.get(project.getDepartmentId()).getProjects().add(project);
        }

        for (DepartmentExtendDTO department : departments.values()) {
            companies.get(department.getCompanyId()).getDepartments().add(department);
        }

        return companies.values();
    }

    @Override
    public List<WorkDiaryDTO> getWorkingHoursByTimePeriod(List<ProjectDTO> projectDTOs, PeriodEnum period,
                                                          LocalDate startDate, LocalDate endDate, String client) {
        UserEntity user = userRepository.getUserByName(client);

        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean isAdminProject = projectDTOs.stream()
                .map(projectFromDTO::apply)
                .anyMatch(projectEntity -> projectRepository.isProjectAdmin(projectEntity, user));

        Assert.isTrue(isSuperUser || isAdminProject, "You haven't required permissions.");

        List<ProjectEntity> projects = projectDTOs.stream()
                .map(projectFromDTO::apply)
                .collect(Collectors.toList());
        Set<UserDTO> users = getUsersForProjects(projects);
        List<PeriodDTO> periods = calculatePeriodsBetweenDates(startDate, endDate, period);
        return users.stream()
                .map(userDTO -> getWorkDiaryForUser(userDTO, projects, periods))
                .collect(Collectors.toList());
    }

    private Set<UserDTO> getUsersForProjects(List<ProjectEntity> projects) {
        List<UserProjectRole> items = userProjectRoleRepository.findUsersByProjects(projects);
        return items.stream()
                .map(UserProjectRole::getUser)
                .map(userToDTO::apply)
                .distinct()
                .collect(Collectors.toSet());
    }

    private List<PeriodDTO> calculatePeriodsBetweenDates(LocalDate startDate, LocalDate endDate, PeriodEnum period) {
        List<PeriodDTO> result = new ArrayList<>();
        switch (period) {
            case DAY:
                result = calculatePeriodsBetweenDatesByDay(startDate, endDate);
                break;
            case WEEK:
                result = calculatePeriodsBetweenTwoDatesByWeek(startDate, endDate);
                break;
            case MONTH:
                result = calculatePeriodsBetweenTwoDatesByMonth(startDate, endDate);
                break;
        }
        return result;
    }

    private List<PeriodDTO> calculatePeriodsBetweenDatesByDay(LocalDate startDate, LocalDate endDate) {
        List<PeriodDTO> result = new ArrayList<>();
        PeriodDTO temp;
        while (!startDate.equals(endDate.plusDays(1))) {
            temp = new PeriodDTO();
            temp.setStartDate(startDate.toDate());
            temp.setEndDate(startDate.toDate());
            result.add(temp);
            startDate = startDate.plusDays(1);
        }
        return result;
    }

    private List<PeriodDTO> calculatePeriodsBetweenTwoDatesByWeek(LocalDate startDate, LocalDate endDate) {
        List<PeriodDTO> result = new ArrayList<>();
        PeriodDTO temp;
        LocalDate curr;
        int daysInWeek = 7;
        int dayOfWeek;
        while (!startDate.equals(endDate.plusDays(1))) {
            dayOfWeek = startDate.getDayOfWeek();
            curr = startDate.plusDays(0);
            if (Days.daysBetween(startDate, endDate).getDays() <= (daysInWeek - dayOfWeek)) {
                startDate = startDate.plusDays(Days.daysBetween(startDate, endDate).getDays());
            } else {
                startDate = startDate.plusDays(daysInWeek - dayOfWeek);
            }
            temp = new PeriodDTO();
            temp.setStartDate(curr.toDate());
            temp.setEndDate(startDate.toDate());
            result.add(temp);
            startDate = startDate.plusDays(1);
        }
        return result;
    }

    private List<PeriodDTO> calculatePeriodsBetweenTwoDatesByMonth(LocalDate startDate, LocalDate endDate) {
        List<PeriodDTO> result = new ArrayList<>();
        PeriodDTO temp;
        LocalDate curr;
        int daysInMonth;
        int dayOfMonth;
        while (!startDate.equals(endDate.plusDays(1))) {
            daysInMonth = startDate.dayOfMonth().getMaximumValue();
            dayOfMonth = startDate.getDayOfMonth();
            curr = startDate.plusDays(0);
            if (Days.daysBetween(startDate, endDate).getDays() <= (daysInMonth - dayOfMonth)) {
                startDate = startDate.plusDays(Days.daysBetween(startDate, endDate).getDays());
            } else {
                startDate = startDate.plusDays(daysInMonth - dayOfMonth);
            }
            temp = new PeriodDTO();
            temp.setStartDate(curr.toDate());
            temp.setEndDate(startDate.toDate());
            result.add(temp);
            startDate = startDate.plusDays(1);
        }
        return result;
    }

    private WorkDiaryDTO getWorkDiaryForUser(UserDTO user, List<ProjectEntity> projects, List<PeriodDTO> periods) {
        WorkDiaryDTO resultWorkDiary = new WorkDiaryDTO();
        resultWorkDiary.setUser(user);
        List<PeriodDTO> resultPeriods = new ArrayList<>();
        PeriodDTO resultPeriod;
        Object[] userResultForPeriod;
        Long efficiency;
        Long snapshotsCount;
        Double activitySum;
        Double workingHours;

        for (PeriodDTO period : periods) {
            userResultForPeriod = snapshotRepository.findSnapshotsCountAndActivitySumForUsersByPeriodAndProjects(user.getId(), projects, period.getStartDate(), new Date(period.getEndDate().getTime() + TimeUnit.DAYS.toMillis(1)));
            snapshotsCount = (Long) userResultForPeriod[0];
            activitySum = userResultForPeriod[2] == null ? 0.0 : Double.parseDouble(userResultForPeriod[2].toString());

            workingHours = snapshotsCount * COUNTS_MINUTES_BETWEEN_TWO_SNAPSHOTS / (double) COUNT_MINUTES_IN_HOURS;
            efficiency = Math.round(activitySum * 100 / snapshotsCount);

            resultPeriod = new PeriodDTO();
            resultPeriod.setStartDate(period.getStartDate());
            resultPeriod.setEndDate(period.getEndDate());
            resultPeriod.setHours(workingHours);
            resultPeriod.setEfficiency(efficiency);
            resultPeriods.add(resultPeriod);
        }
        resultWorkDiary.setPeriods(resultPeriods);
        return resultWorkDiary;
    }
}
