package com.eklib.desktopviewer.services.companystructure;

import com.eklib.desktopviewer.convertor.fromdto.companystructure.ProjectFromDTO;
import com.eklib.desktopviewer.convertor.fromdto.companystructure.ProjectFromDetailDTO;
import com.eklib.desktopviewer.convertor.todto.companystructure.ProjectToDetailDTO;
import com.eklib.desktopviewer.convertor.todto.companystructure.ProjectToExtendDTO;
import com.eklib.desktopviewer.dto.companystructure.ProjectDTO;
import com.eklib.desktopviewer.dto.companystructure.ProjectDetailDTO;
import com.eklib.desktopviewer.dto.companystructure.ProjectExtendDTO;
import com.eklib.desktopviewer.dto.enums.StatusDTO;
import com.eklib.desktopviewer.persistance.model.companystructure.DepartmentEntity;
import com.eklib.desktopviewer.persistance.model.companystructure.ProjectEntity;
import com.eklib.desktopviewer.persistance.model.enums.StatusEnum;
import com.eklib.desktopviewer.persistance.model.security.Role;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.model.security.UserProjectRole;
import com.eklib.desktopviewer.persistance.repository.companystructure.DepartmentRepository;
import com.eklib.desktopviewer.persistance.repository.companystructure.ProjectRepository;
import com.eklib.desktopviewer.persistance.repository.companystructure.UserProjectRoleRepository;
import com.eklib.desktopviewer.persistance.repository.security.RoleRepository;
import com.eklib.desktopviewer.persistance.repository.security.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProjectServicesImpl implements ProjectService {

    @Autowired
    private ProjectFromDTO projectFromDTO;
    @Autowired
    private ProjectFromDetailDTO projectFromDetailDTO;
    @Autowired
    private ProjectToDetailDTO projectToDetailDTO;
    @Autowired
    private ProjectToExtendDTO projectToExtendDTO;

    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private DepartmentRepository departmentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserProjectRoleRepository userProjectRoleRepository;

    @Override
    public ProjectDetailDTO insert(ProjectDTO projectDTO, String client) {
        Assert.isNull(projectDTO.getId(), "Project id must be null.");
        validateProjectDTO(projectDTO);
        ProjectEntity newProject = projectFromDTO.apply(projectDTO);
        Assert.notNull(newProject, "Project must not be null.");

        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean canCreateProjectForDepartment = canCreateProjectForDepartment(newProject.getDepartment(), user);
        Assert.isTrue(isSuperUser || canCreateProjectForDepartment, "You haven't required permissions");

        newProject.setOwner(user);
        projectRepository.insert(newProject);
        Role adminProjectRole = roleRepository.getRoleByName(RoleRepository.ADMIN_PROJECT_ROLE_NAME);
        Assert.notNull(adminProjectRole, "Required role doesn't exist.");

        attachUserToProjectWithRole(user, newProject, adminProjectRole);

        ProjectDetailDTO dto = projectToDetailDTO.apply(newProject);
        if (dto != null) {
            dto.setAdmin(true);
        }
        return dto;
    }

    @Override
    public ProjectDetailDTO update(Long id, ProjectDTO projectDTO, String client) {
        ProjectEntity project = projectRepository.findById(id);
        Assert.notNull(project, "Project doesn't exist.");

        projectDTO.setId(id);
        ProjectEntity updateProject = projectFromDTO.apply(projectDTO);
        Assert.notNull(updateProject, "Project must not be null.");

        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean isProjectAdmin = projectRepository.isProjectAdmin(project, user);
        boolean canCreateProjectForDepartment = canCreateProjectForDepartment(updateProject.getDepartment(), user);
        Assert.isTrue(isSuperUser || (isProjectAdmin && canCreateProjectForDepartment), "You haven't required permissions.");

        project = projectRepository.update(updateProject);

        ProjectDetailDTO dto = projectToDetailDTO.apply(project);
        if (dto != null) {
            dto.setAdmin(true);
        }
        return dto;
    }

    @Override
    public void delete(Long id, String client) {
        UserEntity clientEntity = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(clientEntity);
        Assert.isTrue(isSuperUser, "You haven't required permissions.");

        ProjectEntity project = projectRepository.findById(id);
        Assert.notNull(project, "Project doesn't exist.");

        projectRepository.delete(project);
    }

    @Override
    public ProjectDetailDTO detailUpdate(Long id, ProjectDetailDTO projectDetailDTO, String client) {
        ProjectEntity project = projectRepository.findById(id);
        Assert.notNull(project, "Project doesn't exist.");

        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean isProjectAdmin = projectRepository.isProjectAdmin(project, user);
        Assert.isTrue(isSuperUser || isProjectAdmin, "You haven't required permissions");

        ProjectEntity updatedProject = Objects.requireNonNull(projectFromDetailDTO.apply(projectDetailDTO));
        project = projectRepository.merge(updatedProject);

        ProjectDetailDTO dto = projectToDetailDTO.apply(project);
        if (dto != null) {
            dto.setAdmin(true);
        }
        return dto;
    }

    @Override
    public ProjectDetailDTO findById(Long id, String client) {
        ProjectEntity project = projectRepository.findById(id);
        Assert.notNull(project, "Project doesn't exist.");

        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean isProjectAdmin = projectRepository.isProjectAdmin(project, user);
        Assert.isTrue(isSuperUser || isProjectAdmin, "You haven't required permissions");
        ProjectDetailDTO dto = projectToDetailDTO.apply(project);
        if (dto != null) {
            dto.setAdmin(true);
        }
        return dto;
    }

    @Override
    public Collection<ProjectDetailDTO> findAll(String client) {
        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        List<ProjectEntity> projects;
        if (isSuperUser) {
            projects = projectRepository.findAll();
            return projects.stream()
                    .map(project -> {
                        ProjectDetailDTO dto = projectToDetailDTO.apply(project);
                        if (dto != null) {
                            dto.setAdmin(true);
                        }
                        return dto;
                    })
                    .collect(Collectors.toList());
        } else {
            projects = getUserProjects(user);
            return projects.stream()
                    .map(project -> {
                        ProjectDetailDTO dto = projectToDetailDTO.apply(project);
                        if (dto != null) {
                            dto.setAdmin(projectRepository.isProjectAdmin(project, user));
                        }
                        return dto;
                    })
                    .collect(Collectors.toList());
        }
    }

    @Override
    public Collection<ProjectExtendDTO> findForUser(String client) {
        return projectRepository.findForUser(client).stream().map(projectToExtendDTO::apply).collect(Collectors.toList());
    }

    @Override
    public Collection<ProjectDetailDTO> findUserDependent(String client) {
        List<ProjectEntity> projects = new ArrayList<>();
        projects.addAll(projectRepository.findByProjectAdmin(client));
        projects.addAll(projectRepository.findByDepartmentAdmin(client));
        projects.addAll(projectRepository.findByCompanyAdmin(client));
        return projects.stream()
                .filter(p -> !p.getStatus().equals(StatusEnum.CLOSED))
                .distinct()
                .map(projectToDetailDTO::apply)
                .collect(Collectors.toList());
    }

    @Override
    public ProjectDetailDTO changeStatus(Long id, StatusDTO statusDTO, String client) {
        ProjectEntity project = projectRepository.findById(id);
        Assert.notNull(project, "Project doesn't exist.");

        UserEntity user = userRepository.getUserByName(client);
        StatusEnum newStatus = StatusEnum.valueOf(statusDTO.name());
        StatusEnum depStatusEnum = project.getDepartment().getStatus();
        if (depStatusEnum.equals(StatusEnum.OPEN)) {
            return changeStatus(project, newStatus, user);
        }
        Assert.isTrue(depStatusEnum.equals(StatusEnum.PAUSED) && newStatus.equals(StatusEnum.CLOSED), "Can`t change status in project because department status is " + depStatusEnum);
        return changeStatus(project, newStatus, user);
    }

    private ProjectDetailDTO changeStatus(ProjectEntity project, StatusEnum newStatus, UserEntity user) {
        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean isProjectAdmin = projectRepository.isProjectAdmin(project, user);
        Assert.isTrue(isSuperUser || isProjectAdmin, "You haven't required permissions.");

        if (projectRepository.changeStatus(project, newStatus)) {
            project = projectRepository.findById(project.getId());
        }
        ProjectDetailDTO dto = projectToDetailDTO.apply(project);
        if (dto != null) {
            dto.setAdmin(true);
        }
        return dto;
    }

    @Override
    public void deleteUserProjectRole(Long id, String client) {
        UserProjectRole userProjectRole = userProjectRoleRepository.findById(id);
        Assert.notNull(userProjectRole, "UserProjectRole doesn't exist.");

        UserEntity user = userRepository.getUserByName(client);
        boolean isSuperUser = userRepository.isSuperUser(user);
        boolean isProjectAdmin = projectRepository.isProjectAdmin(userProjectRole.getProject(), user);
        boolean isOwnerRole = userProjectRole.getUser().equals(userProjectRole.getProject().getOwner()) &&
                RoleRepository.ADMIN_PROJECT_ROLE_NAME.equals(userProjectRole.getRole().getName());
        Assert.isTrue(!isOwnerRole && (isSuperUser || isProjectAdmin), "You haven't required permissions");

        userProjectRoleRepository.delete(userProjectRole);
    }

    private void attachUserToProjectWithRole(UserEntity user, ProjectEntity project, Role role) {
        UserProjectRole relation = new UserProjectRole();
        relation.setUser(user);
        relation.setProject(project);
        relation.setRole(role);
        userProjectRoleRepository.insert(relation);
    }

    private List<ProjectEntity> getUserProjects(UserEntity user) {
        return user.getUserProjectRoles().stream()
                .map(UserProjectRole::getProject)
                .distinct()
                .collect(Collectors.toList());
    }

    private void validateProjectDTO(ProjectDTO projectDTO) {
        Assert.hasLength(projectDTO.getName(), "Project name must not be null or empty string.");
        Assert.notNull(projectDTO.getDepartmentId(), "Project must be attached to department.");
        DepartmentEntity attachingDepartment = departmentRepository.findById(projectDTO.getDepartmentId());
        Assert.notNull(attachingDepartment, "Attaching department doesn't exist.");
        Assert.isTrue(attachingDepartment.getStatus().equals(StatusEnum.OPEN), "Only open department could be attached to project.");
        ProjectEntity projectWithSameName = projectRepository.getProjectByDepartmentAndName(attachingDepartment.getId(), projectDTO.getName());
        Assert.isNull(projectWithSameName, "Project with the same name already exists for the department.");
    }

    private boolean canCreateProjectForDepartment(DepartmentEntity department, UserEntity user) {
        if (departmentRepository.isDepartmentAdmin(department, user)) {
            return true;
        }

        Set<ProjectEntity> departmentProjects = department.getProjects();
        return !departmentProjects.stream()
                .filter(project -> projectRepository.isProjectAdmin(project, user))
                .collect(Collectors.toList()).isEmpty();
    }
}
