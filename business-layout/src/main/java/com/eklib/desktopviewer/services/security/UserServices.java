package com.eklib.desktopviewer.services.security;

import com.eklib.desktopviewer.dto.security.AuthenticableDTO;
import com.eklib.desktopviewer.dto.security.UserDTO;
import com.eklib.desktopviewer.dto.security.UserDetailDTO;

import java.util.List;
import java.util.Set;


/**
 * Created by vadim on 18.09.2014.
 */
public interface UserServices {

    AuthenticableDTO findAuthenticable(String name);

    UserDTO createUser(UserDetailDTO userDetailDTO);

    UserDetailDTO getAutorizedUser(String name);

    UserDTO findById(Long id, String client);

    List<UserDTO> findAll(String client);

    Set<UserDTO> findFreeUsers(Long projectId, String client);

    List<UserDTO> findFreeUsersForDepartment(Long departmentId, String client);

    List<UserDTO> findFreeUsersForCompany(Long companyId, String client);

    void delete(Long id, String client);

    UserDTO update(Long id, UserDTO dto, String client);

    void makeAdmin(Long id, String client);

    void makeNonAdmin(Long id, String client);

    void requestOnChangingPassword(String login);

    void changePassword(String password, String token);

    void confirmUser(String token);
}
