package com.eklib.desktopviewer.services.file.google;

import com.eklib.desktopviewer.dto.settings.file.OutdatedFilesDeleteResultDTO;
import com.eklib.desktopviewer.dto.settings.file.StorageOutdatedFilesInfoDTO;
import com.eklib.desktopviewer.dto.settings.storage.GoogleDriveSettingsDTO;
import com.eklib.desktopviewer.exception.server.DesktopviewerServerException;
import com.eklib.desktopviewer.persistance.model.property.PropertyEntity;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.repository.property.PropertyRepository;
import com.eklib.desktopviewer.persistance.repository.security.UserRepository;
import com.eklib.desktopviewer.services.file.FileService;
import com.eklib.desktopviewer.services.settings.storage.GoogleDriveSettingsService;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.services.drive.model.File;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;

@Service
@Transactional
public class GoogleDriveFileService implements FileService, GoogleDriveSettingsService {

    private static final int OUTDATED_FILES_RESULTS_COUNT = 1000;
    private static final String IMAGE_FILE_FORMAT = "jpg";
    private static final String IMAGE_MIME_TYPE = "image/jpeg";
    private static final String GOOGLE_DRIVE_FOLDER_MIME_TYPE = "application/vnd.google-apps.folder";

    @Autowired
    private GoogleDriveService googleDriveService;
    @Autowired
    private PropertyRepository propertyRepository;
    @Autowired
    private UserRepository userRepository;

    private File storageFolder;

    @Value("${IMAGE.TEMP_FILE_DIRECTORY}")
    private String tempFolderName;
    @Value("${GOOGLE.IMAGES_FOLDER_TITLE}")
    private String storageFolderName;

    @Override
    public String saveToFile(byte[] data) {
        try{
            File file = new File().setTitle(generateFileName()).setMimeType(IMAGE_MIME_TYPE);
            ByteArrayContent content = new ByteArrayContent(IMAGE_MIME_TYPE, data);
            file = googleDriveService.createFile(file, content);
            file = googleDriveService.shareFileToAnyone(file);
            return file.getWebContentLink();
        } catch(IOException e){
            throw new DesktopviewerServerException("Couldn't save image on Google Drive", e);
        }
    }

    @Override
    public boolean deleteFileByLink(String link) {
        if(link != null && link.contains("googledrive.com")){
            try{
                String fileName = link.substring(link.lastIndexOf("/") + 1);
                File file = googleDriveService.getFileByName(fileName);
                if(file != null){
                    googleDriveService.deleteFile(file);
                }
            } catch(IOException e){
                return false;
            }
        }
        return true;
    }

    @Override
    public StorageOutdatedFilesInfoDTO getOutdatedFilesInfo(LocalDate limitDate) {
        StorageOutdatedFilesInfoDTO result = new StorageOutdatedFilesInfoDTO();
        String searchQuery = String.format("modifiedDate<'%s' and mimeType!='%s'",
                limitDate.toString("yyyy-MM-dd") + "T00:00:00",
                GOOGLE_DRIVE_FOLDER_MIME_TYPE);
        try{
            List<File> files = googleDriveService.searchFiles(searchQuery, OUTDATED_FILES_RESULTS_COUNT);
            result.setFilesCount(files.size());
        } catch (IOException e){
            result.setFailed(true);
            result.setFailMessage(e.getMessage());
        }
        return result;
    }

    @Override
    public OutdatedFilesDeleteResultDTO deleteOutdatedFiles(LocalDate limitDate) {
        OutdatedFilesDeleteResultDTO result = new OutdatedFilesDeleteResultDTO();
        String searchQuery = String.format("modifiedDate<'%s' and mimeType!='%s'",
                limitDate.toString("yyyy-MM-dd") + "T00:00:00",
                GOOGLE_DRIVE_FOLDER_MIME_TYPE);
        try{
            List<File> outdatedFiles = googleDriveService.searchFiles(searchQuery, OUTDATED_FILES_RESULTS_COUNT);
            result.setFilesCount(outdatedFiles.size());
            result.setDeletedFilesCount(googleDriveService.deleteFilesBatch(outdatedFiles));
        } catch(IOException e){
            result.setFailed(true);
            result.setFailMessage(e.getMessage());
        }
        return result;
    }

    @Override
    public GoogleDriveSettingsDTO getGoogleDriveSettings(String client) {
        UserEntity user = userRepository.getUserByName(client);
        Assert.notNull(user, "Couldn't find user with same name.");
        Assert.isTrue(userRepository.isSuperUser(user), "You have not required permissions.");
        GoogleDriveSettingsDTO settings = new GoogleDriveSettingsDTO();
        settings.setServiceAccountId(googleDriveService.getServiceAccountId());
        settings.setSecretFileLocation(googleDriveService.getSecretFileLocation());
        settings.setStorageFolderName(this.storageFolderName);
        settings.setTempFolderName(this.tempFolderName);
        return settings;
    }

    @Override
    public void setGoogleDriveSettings(GoogleDriveSettingsDTO newGoogleDriveSettings, String client) {
        UserEntity user = userRepository.getUserByName(client);
        Assert.notNull(user, "Couldn't find user with same name.");
        Assert.isTrue(userRepository.isSuperUser(user), "You have not required permissions.");
        googleDriveService.setServiceAccountId(newGoogleDriveSettings.getServiceAccountId());
        PropertyEntity gd_serviceAccountId = propertyRepository.findByName("GoogleDrive.serviceAccountId");
        if(gd_serviceAccountId != null) {
            gd_serviceAccountId.setValue(googleDriveService.getServiceAccountId());
            propertyRepository.update(gd_serviceAccountId);
        } else {
            gd_serviceAccountId = new PropertyEntity();
            gd_serviceAccountId.setName("GoogleDrive.serviceAccountId");
            gd_serviceAccountId.setValue(googleDriveService.getServiceAccountId());
            propertyRepository.insert(gd_serviceAccountId);
        }

        googleDriveService.setSecretFileLocation(newGoogleDriveSettings.getSecretFileLocation());
        PropertyEntity gd_secretFileLocation = propertyRepository.findByName("GoogleDrive.secretFileLocation");
        if(gd_secretFileLocation != null) {
            gd_secretFileLocation.setValue(googleDriveService.getSecretFileLocation());
            propertyRepository.update(gd_secretFileLocation);
        } else {
            gd_secretFileLocation = new PropertyEntity();
            gd_secretFileLocation.setName("GoogleDrive.secretFileLocation");
            gd_secretFileLocation.setValue(googleDriveService.getSecretFileLocation());
            propertyRepository.insert(gd_secretFileLocation);
        }

        this.storageFolderName = newGoogleDriveSettings.getStorageFolderName();
        PropertyEntity gd_storageFolderName = propertyRepository.findByName("GoogleDrive.storageFolderName");
        if(gd_storageFolderName != null) {
            gd_storageFolderName.setValue(this.storageFolderName);
            propertyRepository.update(gd_storageFolderName);
        } else {
            gd_storageFolderName = new PropertyEntity();
            gd_storageFolderName.setName("GoogleDrive.storageFolderName");
            gd_storageFolderName.setValue(this.storageFolderName);
            propertyRepository.insert(gd_storageFolderName);
        }

        this.tempFolderName = newGoogleDriveSettings.getTempFolderName();
        PropertyEntity gd_tempFolderName = propertyRepository.findByName("GoogleDrive.tempFolderName");
        if(gd_tempFolderName != null) {
            gd_tempFolderName.setValue(this.tempFolderName);
            propertyRepository.update(gd_tempFolderName);
        } else {
            gd_tempFolderName = new PropertyEntity();
            gd_tempFolderName.setName("GoogleDrive.tempFolderName");
            gd_tempFolderName.setValue(this.tempFolderName);
            propertyRepository.insert(gd_tempFolderName);
        }
    }

    @PostConstruct
    private void getGoogleDriveSettingsFormDB() {
        PropertyEntity gd_serviceAccountId = propertyRepository.findByName("GoogleDrive.serviceAccountId");
        if (gd_serviceAccountId != null){
            googleDriveService.setServiceAccountId(gd_serviceAccountId.getValue());
        }
        PropertyEntity gd_secretFileLocation = propertyRepository.findByName("GoogleDrive.secretFileLocation");
        if(gd_secretFileLocation != null) {
            googleDriveService.setSecretFileLocation(gd_secretFileLocation.getValue());
        }
        PropertyEntity gd_storageFolderName = propertyRepository.findByName("GoogleDrive.storageFolderName");
        if(gd_storageFolderName != null) {
            this.storageFolderName = gd_storageFolderName.getValue();
        }
        PropertyEntity gd_tempFolderName = propertyRepository.findByName("GoogleDrive.tempFolderName");
        if(gd_tempFolderName != null) {
            this.tempFolderName = gd_tempFolderName.getValue();
        }
    }

    private String generateFileName() {
        return String.valueOf(System.identityHashCode(new Object())) + System.currentTimeMillis() + "." + IMAGE_FILE_FORMAT;
    }

    private File getStorageFolder() throws IOException{
        if(storageFolder == null){
            storageFolder = googleDriveService.getFileByName(storageFolderName);
        }
        if(storageFolder == null){
            File folder = new File().setTitle(storageFolderName).setMimeType(GOOGLE_DRIVE_FOLDER_MIME_TYPE);
            storageFolder = googleDriveService.createFile(folder, null);
        }
        if(storageFolder.getWebViewLink() == null ||storageFolder.getWebViewLink().isEmpty()){
            storageFolder = googleDriveService.shareFileToAnyone(storageFolder);
        }
        return storageFolder;
    }
}
