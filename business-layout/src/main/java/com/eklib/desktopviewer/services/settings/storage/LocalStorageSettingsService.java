package com.eklib.desktopviewer.services.settings.storage;

import com.eklib.desktopviewer.dto.settings.storage.LocalStorageSettingsDTO;

public interface LocalStorageSettingsService {

    LocalStorageSettingsDTO getLocalStorageSettings(String client);

    void setLocalStorageSettings(LocalStorageSettingsDTO newLocalStorageSettings, String client);
}
