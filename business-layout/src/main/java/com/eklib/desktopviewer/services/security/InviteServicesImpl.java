package com.eklib.desktopviewer.services.security;

import com.eklib.desktopviewer.convertor.fromdto.security.InviteFromDTO;
import com.eklib.desktopviewer.convertor.todto.security.InviteToDTO;
import com.eklib.desktopviewer.convertor.todto.security.UserToDTO;
import com.eklib.desktopviewer.dto.security.InviteDTO;
import com.eklib.desktopviewer.persistance.model.companystructure.CompanyEntity;
import com.eklib.desktopviewer.persistance.model.companystructure.DepartmentEntity;
import com.eklib.desktopviewer.persistance.model.companystructure.ProjectEntity;
import com.eklib.desktopviewer.persistance.model.companystructure.StructureType;
import com.eklib.desktopviewer.persistance.model.security.*;
import com.eklib.desktopviewer.persistance.repository.companystructure.*;
import com.eklib.desktopviewer.persistance.repository.security.InviteRepository;
import com.eklib.desktopviewer.persistance.repository.security.RoleRepository;
import com.eklib.desktopviewer.persistance.repository.security.UserRepository;
import com.eklib.desktopviewer.services.mail.MailService;
import com.eklib.desktopviewer.services.settings.client.ClientSettingsService;
import com.eklib.desktopviewer.util.TokenUtil;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.Date;

@Service
@Transactional
public class InviteServicesImpl implements InviteServices {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private UserCompanyRoleRepository companyRoleRepository;
    @Autowired
    private DepartmentRepository departmentRepository;
    @Autowired
    private UserDepartmentRoleRepository departmentRoleRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private UserProjectRoleRepository projectRoleRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private InviteRepository inviteRepository;
    @Autowired
    private InviteToDTO inviteToDTO;
    @Autowired
    private InviteFromDTO inviteFromDTO;
    @Autowired
    private UserToDTO userToDTO;
    @Autowired
    private MailService mailService;
    @Autowired
    private ClientSettingsService clientSettingsService;

    @Override
    public InviteDTO create(InviteDTO inviteDTO, String clientLogin) {
        Assert.notNull(inviteDTO, "Invite must not be null.");
        Assert.notNull(inviteDTO.getType(), "Invite type must not be null.");
        Assert.notNull(inviteDTO.getStructureId(), "Structure id must not be null.");
        Assert.notNull(inviteDTO.getRoleId(), "Role id must not be null.");
        Assert.hasLength(inviteDTO.getEmail(), "User email must not be null or empty string");

        UserEntity client = userRepository.getUserByName(clientLogin);
        InviteEntity invite = inviteFromDTO.apply(inviteDTO);
        Assert.notNull(invite, "Invite must not be null.");

        String structureName = validateDataAndGetStructureName(invite, client);
        invite.setExpire(LocalDateTime.now().plusDays(1).toDate());
        invite.setToken(TokenUtil.createToken());
        invite = inviteRepository.insert(invite);
        mailService.sendMailForConfirmInvite(
                invite.getEmail(), clientSettingsService.get().getInviteConfirmURL() + invite.getToken(), structureName
        );
        mailService.sendMailForInviteAuthor(client.getEmail(), invite.getEmail(), structureName);
        return inviteToDTO.apply(invite);
    }

    @Override
    public InviteDTO getByToken(String token) {
        InviteEntity invite = inviteRepository.getByToken(token);
        InviteDTO inviteDTO = inviteToDTO.apply(invite);
        Assert.notNull(inviteDTO, "Invite doesn't exist.");
        Assert.isTrue(new Date().before(invite.getExpire()), "Invite is expired.");
        UserEntity user = userRepository.getUserByName(inviteDTO.getEmail());
        inviteDTO.setUser(userToDTO.apply(user));
        return inviteDTO;
    }

    @Override
    public InviteDTO confirm(String token, String clientLogin) {
        Assert.hasLength(token, "Token must not be null or empty string.");
        InviteEntity invite = inviteRepository.getByToken(token);
        Assert.notNull(invite, "Invite doesn't exist.");
        Assert.isTrue(new Date().before(invite.getExpire()), "Invite expired.");
        UserEntity client = userRepository.getUserByName(clientLogin);
        Assert.notNull(client, "Client doesn't exists.");
        Assert.isTrue(client.getEmail().equals(invite.getEmail()), "You haven't required permissions.");

        Role role = roleRepository.findById(invite.getRoleId());
        Assert.notNull(role, "Role doesn't exist.");

        switch (invite.getType()) {
            case COMPANY:
                attachToCompany(client, invite.getStructureId(), role);
                break;
            case DEPARTMENT:
                attachToDepartment(client, invite.getStructureId(), role);
                break;
            case PROJECT:
                attachToProject(client, invite.getStructureId(), role);
                break;
        }
        InviteDTO dto = inviteToDTO.apply(invite);
        inviteRepository.delete(invite);
        return dto;
    }

    private String validateDataAndGetStructureName(InviteEntity invite, UserEntity client) {
        boolean isSuperUser = userRepository.isSuperUser(client);
        boolean canCreateInvite = false;
        String structureName = "";
        switch (invite.getType()) {
            case COMPANY:
                CompanyEntity company = companyRepository.findById(invite.getStructureId());
                Assert.notNull(company, "Company doesn't exist.");
                structureName = company.getName();
                canCreateInvite = isSuperUser || companyRepository.isCompanyAdmin(company, client);
                checkRole(invite.getRoleId(), StructureType.Values.COMPANY);
                break;
            case DEPARTMENT:
                DepartmentEntity department = departmentRepository.findById(invite.getStructureId());
                Assert.notNull(department, "Department doesn't exist.");
                structureName = department.getName();
                canCreateInvite = isSuperUser || departmentRepository.isDepartmentAdmin(department, client);
                checkRole(invite.getRoleId(), StructureType.Values.DEPARTMENT);
                break;
            case PROJECT:
                ProjectEntity project = projectRepository.findById(invite.getStructureId());
                Assert.notNull(project, "Project doesn't exist.");
                structureName = project.getName();
                canCreateInvite = isSuperUser || projectRepository.isProjectAdmin(project, client);
                checkRole(invite.getRoleId(), StructureType.Values.PROJECT);
                break;
        }
        Assert.isTrue(canCreateInvite, "You haven't required permissions.");
        return structureName;
    }

    private void checkRole(Long roleId, StructureType.Values roleType) {
        Role role = roleRepository.findById(roleId);
        Assert.notNull(role, "Role doesn't exist.");
        Assert.isTrue(roleType.name().equals(role.getStructureType().getType()), "Role has an invalid type.");
    }

    private void attachToCompany(UserEntity user, Long companyId, Role role) {
        CompanyEntity company = companyRepository.findById(companyId);
        Assert.notNull(company, "Company doesn't exist.");
        Assert.isTrue(role.getStructureType().getType().equals(StructureType.Values.COMPANY.name()), "Invalid role type.");

        //todo refactoring
        if (role.getName().equals(RoleRepository.ADMIN_COMPANY_ROLE_NAME)) {
            company.getDepartments().forEach(departmentEntity -> {

                Role adminDepartmentRole = roleRepository.getRoleByName(RoleRepository.ADMIN_DEPARTMENT_ROLE_NAME);

                UserDepartmentRole departmentRole = new UserDepartmentRole();
                departmentRole.setUser(user);
                departmentRole.setDepartment(departmentEntity);
                departmentRole.setRole(adminDepartmentRole);

                departmentEntity.getUserDepartmentRoles().add(departmentRole);
                departmentEntity.getProjects().forEach(projectEntity -> {

                    Role adminProjectRole = roleRepository.getRoleByName(RoleRepository.ADMIN_PROJECT_ROLE_NAME);

                    UserProjectRole userProjectRole = new UserProjectRole();
                    userProjectRole.setUser(user);
                    userProjectRole.setProject(projectEntity);
                    userProjectRole.setRole(adminProjectRole);

                    projectEntity.getUserProjectRoles().add(userProjectRole);
                });
            });
        }


        UserCompanyRole companyRole = new UserCompanyRole();
        companyRole.setUser(user);
        companyRole.setCompany(company);
        companyRole.setRole(role);
        companyRoleRepository.insert(companyRole);
    }

    private void attachToDepartment(UserEntity user, Long departmentId, Role role) {
        DepartmentEntity department = departmentRepository.findById(departmentId);
        Assert.notNull(department, "Department doesn't exist.");
        Assert.isTrue(role.getStructureType().getType().equals(StructureType.Values.DEPARTMENT.name()), "Invalid role type.");

        //todo refactoring
        if (role.getName().equals(RoleRepository.ADMIN_DEPARTMENT_ROLE_NAME)) {
            department.getProjects()
                    .forEach(projectEntity -> {
                        UserProjectRole userProjectRole = new UserProjectRole();
                        Role adminProjectRole = roleRepository.getRoleByName(RoleRepository.ADMIN_PROJECT_ROLE_NAME);
                        userProjectRole.setUser(user);
                        userProjectRole.setProject(projectEntity);
                        userProjectRole.setRole(adminProjectRole);
                        projectEntity.getUserProjectRoles().add(userProjectRole);
                    });
        }

        UserDepartmentRole departmentRole = new UserDepartmentRole();
        departmentRole.setUser(user);
        departmentRole.setDepartment(department);
        departmentRole.setRole(role);
        departmentRoleRepository.insert(departmentRole);
    }

    private void attachToProject(UserEntity user, Long projectId, Role role) {
        ProjectEntity project = projectRepository.findById(projectId);
        Assert.notNull(project, "Project doesn't exist.");
        Assert.isTrue(role.getStructureType().getType().equals(StructureType.Values.PROJECT.name()), "Invalid role type.");
        UserProjectRole projectRolee = new UserProjectRole();
        projectRolee.setUser(user);
        projectRolee.setProject(project);
        projectRolee.setRole(role);
        projectRoleRepository.insert(projectRolee);
    }
}
