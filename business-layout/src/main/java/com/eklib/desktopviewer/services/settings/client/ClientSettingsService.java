package com.eklib.desktopviewer.services.settings.client;

import com.eklib.desktopviewer.dto.settings.client.ClientSettingsDTO;

public interface ClientSettingsService {

    ClientSettingsDTO get(String clientLogin);

    ClientSettingsDTO get();

    ClientSettingsDTO update(ClientSettingsDTO newSettings, String clientLogin);
}
