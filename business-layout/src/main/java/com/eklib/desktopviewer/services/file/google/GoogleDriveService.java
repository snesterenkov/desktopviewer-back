package com.eklib.desktopviewer.services.file.google;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.http.AbstractInputStreamContent;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.Permission;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class GoogleDriveService {

    private JsonFactory jsonFactory;
    private HttpTransport httpTransport;
    private Credential credential;
    private Drive drive;

    @Value("${GOOGLE.SERVICE_ACCOUNT_ID}")
    private String serviceAccountId;
    @Value("${GOOGLE.SECRET_FILE_PATH}")
    private String secretFileLocation;

    public GoogleDriveService() throws GeneralSecurityException, IOException {
        jsonFactory = JacksonFactory.getDefaultInstance();
        httpTransport = GoogleNetHttpTransport.newTrustedTransport();
    }

    public File createFile(File metadata, AbstractInputStreamContent content) throws IOException{
        if(content == null){
            return getDrive().files().insert(metadata).execute();
        } else {
            return getDrive().files().insert(metadata, content).execute();
        }
    }

    public File getFileByName(String name) throws IOException{
        String query = String.format("title='%s'", name);
        List<File> files = getDrive().files().list().setQ(query).setMaxResults(1).execute().getItems();
        return files.isEmpty()? null: files.get(0);
    }

    public void deleteFile(File file) throws IOException{
        if(file != null){
            getDrive().files().delete(file.getId()).execute();
        }
    }

    public File shareFileToAnyone(File file) throws IOException {
        if(file != null){
            Permission permission = new Permission().setRole("reader").setType("anyone");
            getDrive().permissions().insert(file.getId(), permission).execute();
            return getDrive().files().get(file.getId()).execute();
        }
        return null;
    }

    public List<File> searchFiles(String searchQuery, int maxResults) throws IOException{
        if(maxResults < 0 || 1000< maxResults){
            throw new RuntimeException("MaxResults must be between 0 and 1000 inclusive.");
        }
        return getDrive().files().list().setQ(searchQuery).setMaxResults(maxResults).execute().getItems();
    }

    public int deleteFilesBatch(List<File> files) throws IOException{
        Counter deletedFilesCounter = new Counter();
        BatchRequest batch = getDrive().batch();
        JsonBatchCallback<Void> callback = new JsonBatchCallback<Void>() {
            @Override
            public void onFailure(GoogleJsonError googleJsonError, HttpHeaders httpHeaders) throws IOException {

            }
            @Override
            public void onSuccess(Void aVoid, HttpHeaders httpHeaders) throws IOException {
                deletedFilesCounter.inc();
            }
        };
        if(!files.isEmpty()){
            for(File file: files){
                getDrive().files().delete(file.getId()).queue(batch, callback);
            }
            batch.execute();
            return deletedFilesCounter.value();
        }
        return 0;
    }

    public String getServiceAccountId() {
        return serviceAccountId;
    }

    public void setServiceAccountId(String serviceAccountId) {
        this.serviceAccountId = serviceAccountId;
        this.credential = null;
        this.drive = null;
    }

    public String getSecretFileLocation() {
        return secretFileLocation;
    }

    public void setSecretFileLocation(String secretFileLocation) {
        this.secretFileLocation = secretFileLocation;
        this.drive = null;
        this.credential = null;
    }

    private Drive getDrive() throws IOException {
        if(drive == null) {
            drive = new Drive.Builder(httpTransport, jsonFactory, getCredential()).build();
        }
        return drive;
    }

    private Credential getCredential() throws IOException{
        if(credential == null) {
            credential = authorize();
        }
        return credential;
    }

    private Credential authorize() throws IOException{
        Credential result;
        try{
            result = new GoogleCredential.Builder()
                    .setTransport(httpTransport)
                    .setJsonFactory(jsonFactory)
                    .setServiceAccountId(serviceAccountId)
                    .setServiceAccountPrivateKeyFromP12File(new java.io.File(secretFileLocation))
                    .setServiceAccountScopes(Collections.singleton(DriveScopes.DRIVE_FILE))
                    .build();
        } catch(GeneralSecurityException e) {
            IOException exception = new IOException("Security exception during authorize on GD.", e.getCause());
            exception.setStackTrace(e.getStackTrace());
            throw exception;
        }
        return result;
    }

    private class Counter{
        private int value = 0;

        public void inc(){
            value++;
        }

        public void dec(){
            value--;
        }

        public int value(){
            return value;
        }
    }
}
