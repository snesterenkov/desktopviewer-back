package com.eklib.desktopviewer.services.companystructure;

import com.eklib.desktopviewer.dto.companystructure.ProjectDTO;
import com.eklib.desktopviewer.dto.companystructure.ProjectDetailDTO;
import com.eklib.desktopviewer.dto.companystructure.ProjectExtendDTO;
import com.eklib.desktopviewer.dto.enums.StatusDTO;

import java.util.Collection;

public interface ProjectService {

    ProjectDetailDTO insert(ProjectDTO departmentDTO, String client);

    ProjectDetailDTO update(Long id, ProjectDTO departmentDTO, String client);

    void delete(Long id, String client);

    ProjectDetailDTO detailUpdate(Long id, ProjectDetailDTO projectDetailDTO, String client);

    ProjectDetailDTO findById(Long id, String client);

    Collection<ProjectDetailDTO> findAll(String client);

    Collection<ProjectExtendDTO> findForUser(String client);

    Collection<ProjectDetailDTO> findUserDependent(String client);

    ProjectDetailDTO changeStatus(Long id, StatusDTO newStatus, String client);

    void deleteUserProjectRole(Long id, String client);
}
