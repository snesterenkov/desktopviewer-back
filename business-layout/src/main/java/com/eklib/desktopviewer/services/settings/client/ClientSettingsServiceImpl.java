package com.eklib.desktopviewer.services.settings.client;

import com.eklib.desktopviewer.dto.settings.client.ClientSettingsDTO;
import com.eklib.desktopviewer.persistance.model.property.PropertyEntity;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.repository.property.PropertyRepository;
import com.eklib.desktopviewer.persistance.repository.security.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;

@Service
@Transactional
public class ClientSettingsServiceImpl implements ClientSettingsService{

    private static ClientSettingsDTO currentSettings;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PropertyRepository propertyRepository;

    @Override
    public ClientSettingsDTO get(String clientLogin) {
        UserEntity client = userRepository.getUserByName(clientLogin);
        Assert.isTrue(userRepository.isSuperUser(client), "You haven't required permissions.");
        return get();
    }

    @Override
    public ClientSettingsDTO get() {
        if(currentSettings == null){
            currentSettings = loadSettings();
        }
        return currentSettings;
    }

    @Override
    public ClientSettingsDTO update(ClientSettingsDTO newSettings, String clientLogin) {
        UserEntity client = userRepository.getUserByName(clientLogin);
        Assert.isTrue(userRepository.isSuperUser(client), "You haven't required permissions.");
        saveSettings(newSettings);
        currentSettings = newSettings;
        return newSettings;
    }

    private ClientSettingsDTO loadSettings(){
        ClientSettingsDTO loadedSettings = new ClientSettingsDTO();
        loadedSettings.setRegistrationConfirmURL(loadSetting("client.registrationConfirmURL"));
        loadedSettings.setInviteConfirmURL(loadSetting("client.inviteConfirmURL"));
        loadedSettings.setPasswordRestoreURL(loadSetting("client.passwordRestoreURL"));
        return loadedSettings;
    }

    private String loadSetting(String name){
        PropertyEntity setting = propertyRepository.findByName(name);
        return setting != null? setting.getValue(): "";
    }

    private void saveSettings(ClientSettingsDTO newSettings){
        saveSetting("client.registrationConfirmURL", newSettings.getRegistrationConfirmURL());
        saveSetting("client.inviteConfirmURL", newSettings.getInviteConfirmURL());
        saveSetting("client.passwordRestoreURL", newSettings.getPasswordRestoreURL());
    }

    private void saveSetting(String name, String value){
        PropertyEntity setting = propertyRepository.findByName(name);
        if(setting == null){
            setting = new PropertyEntity(name, value);
            propertyRepository.insert(setting);
        } else {
            setting.setValue(value);
            propertyRepository.update(setting);
        }
    }
}
