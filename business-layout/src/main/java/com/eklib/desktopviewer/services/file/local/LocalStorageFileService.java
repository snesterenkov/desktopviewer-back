package com.eklib.desktopviewer.services.file.local;

import com.eklib.desktopviewer.dto.settings.file.OutdatedFilesDeleteResultDTO;
import com.eklib.desktopviewer.dto.settings.file.StorageOutdatedFilesInfoDTO;
import com.eklib.desktopviewer.dto.settings.storage.LocalStorageSettingsDTO;
import com.eklib.desktopviewer.exception.server.DesktopviewerServerException;
import com.eklib.desktopviewer.persistance.model.property.PropertyEntity;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.repository.property.PropertyRepository;
import com.eklib.desktopviewer.persistance.repository.security.UserRepository;
import com.eklib.desktopviewer.services.file.FileService;
import com.eklib.desktopviewer.services.settings.storage.LocalStorageSettingsService;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

@Service
@Transactional
public class LocalStorageFileService implements FileService, LocalStorageSettingsService {

    private static final String IMAGE_FILE_FORMAT = "jpg";

    @Autowired
    private PropertyRepository propertyRepository;
    @Autowired
    private UserRepository userRepository;

    @Value("${SERVER.IMAGES_FOLDER_URL}")
    private String imagesHostURL;
    @Value("${IMAGE.FILE_DIRECTORY}")
    private String storageFolderName;

    @Override
    public String saveToFile(byte[] data) {
        String fileName = generateFileName();
        File file = new File(fileName);
        file.getParentFile().mkdirs();
        try(FileOutputStream out = new FileOutputStream(file)) {
            out.write(data);
            out.close();
        } catch (IOException e) {
            throw new DesktopviewerServerException("Couldn't save image to file.", e.getCause());
        }
        return imagesHostURL + file.getName();
    }

    @Override
    public boolean deleteFileByLink(String link) {
        if(link != null && link.contains(imagesHostURL)){
            String filePath = (storageFolderName + link.replace(imagesHostURL, "")).replace("/", File.separator);
            File file = new File(filePath);
            if(file.exists()){
                return file.delete();
            }
        }
        return true;
    }

    @Override
    public StorageOutdatedFilesInfoDTO getOutdatedFilesInfo(LocalDate limitDate) {
        StorageOutdatedFilesInfoDTO result = new StorageOutdatedFilesInfoDTO();
        BiPredicate<Path, BasicFileAttributes> searchPredicate =
                (path, basicFileAttributes) -> basicFileAttributes.creationTime().toMillis() < limitDate.toDate().getTime() &&
                        path.toFile().isFile();
        try{
            List<Path> outdatedFiles =  Files.find(new File(storageFolderName).toPath(), 1, searchPredicate).collect(Collectors.toList());
            result.setFilesCount(outdatedFiles.size());
        } catch (IOException e){
            result.setFailed(true);
            result.setFailMessage(e.getMessage());
        }
        return result;
    }

    @Override
    public OutdatedFilesDeleteResultDTO deleteOutdatedFiles(LocalDate limitDate) {
        BiPredicate<Path, BasicFileAttributes> searchPredicate =
                (path, basicFileAttributes) -> basicFileAttributes.creationTime().toMillis() < limitDate.toDate().getTime() &&
                path.toFile().isFile();
        OutdatedFilesDeleteResultDTO result = new OutdatedFilesDeleteResultDTO();
        try{
            List<Path> outdatedFiles =  Files.find(new File(storageFolderName).toPath(), 1, searchPredicate).collect(Collectors.toList());
            result.setFilesCount(outdatedFiles.size());
            result.setDeletedFilesCount(outdatedFiles.stream().map(Path::toFile).filter(File::delete).count());
        } catch (IOException e){
            result.setFailed(true);
            result.setFailMessage(e.getMessage());
        }
        return result;
    }

    @Override
    public LocalStorageSettingsDTO getLocalStorageSettings(String client) {
        UserEntity user = userRepository.getUserByName(client);
        Assert.notNull(user, "Couldn't find user with same name.");
        Assert.isTrue(userRepository.isSuperUser(user), "You have not required permissions.");
        LocalStorageSettingsDTO settings = new LocalStorageSettingsDTO();
        settings.setStorageFolderName(this.storageFolderName);
        settings.setImagesHostURL(this.imagesHostURL);
        return settings;
    }

    @Override
    public void setLocalStorageSettings(LocalStorageSettingsDTO newLocalStorageSettings, String client) {
        UserEntity user = userRepository.getUserByName(client);
        Assert.notNull(user, "Couldn't find user with same name.");
        Assert.isTrue(userRepository.isSuperUser(user), "You have not required permissions.");
        this.storageFolderName = newLocalStorageSettings.getStorageFolderName();
        PropertyEntity ls_storageFolderName = propertyRepository.findByName("LocalStorage.storageFolderName");
        if(ls_storageFolderName != null) {
            ls_storageFolderName.setValue(this.storageFolderName);
            propertyRepository.update(ls_storageFolderName);
        } else {
            ls_storageFolderName = new PropertyEntity();
            ls_storageFolderName.setName("LocalStorage.storageFolderName");
            ls_storageFolderName.setValue(this.storageFolderName);
            propertyRepository.insert(ls_storageFolderName);
        }
        this.imagesHostURL = newLocalStorageSettings.getImagesHostURL();
        PropertyEntity ls_imagesHostURL = propertyRepository.findByName("LocalStorage.imagesHostURL");
        if(ls_imagesHostURL != null){
            ls_imagesHostURL.setValue(this.imagesHostURL);
            propertyRepository.update(ls_imagesHostURL);
        } else {
            ls_imagesHostURL = new PropertyEntity();
            ls_imagesHostURL.setName("LocalStorage.imagesHostURL");
            ls_imagesHostURL.setValue(this.imagesHostURL);
            propertyRepository.insert(ls_imagesHostURL);
        }
    }

    @PostConstruct
    private void getLocalStorageSettingsFromDB() {
        PropertyEntity ls_storageFolderName = propertyRepository.findByName("LocalStorage.storageFolderName");
        if(ls_storageFolderName != null) {
            this.storageFolderName = ls_storageFolderName.getValue();
        }
        PropertyEntity ls_imagesHostUrl = propertyRepository.findByName("LocalStorage.imagesHostURL");
        if(ls_imagesHostUrl != null) {
            this.imagesHostURL = ls_imagesHostUrl.getValue();
        }
    }

    private String generateFileName() {
        return storageFolderName + String.valueOf(System.identityHashCode(new Object())) + System.currentTimeMillis() + "." + IMAGE_FILE_FORMAT;
    }
}
