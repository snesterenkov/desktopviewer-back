package com.eklib.desktopviewer.services.security;


import com.eklib.desktopviewer.dto.security.InviteDTO;

public interface InviteServices {

    InviteDTO create(InviteDTO inviteDTO, String clientLogin);

    InviteDTO getByToken(String token);

    InviteDTO confirm(String token, String clientLogin);
}
