package com.eklib.desktopviewer.services.settings.storage;

import com.eklib.desktopviewer.dto.enums.StorageTypeDTO;
import com.eklib.desktopviewer.dto.settings.file.OutdatedFilesDeleteResultDTO;
import com.eklib.desktopviewer.dto.settings.file.StorageOutdatedFilesInfoDTO;
import com.eklib.desktopviewer.dto.settings.storage.StorageDTO;

import java.util.List;

public interface StorageSettingsService {

    List<StorageDTO> getAllAvailableStorage(String client);

    StorageDTO setCurrentStorage(StorageDTO storage, String client);

    List<StorageOutdatedFilesInfoDTO> getInfoAboutOutdatedFiles(String client);

    StorageOutdatedFilesInfoDTO getInfoAboutStorageOutdatedFiles(StorageTypeDTO storage, String client);

    List<OutdatedFilesDeleteResultDTO> deleteOutdatedImages(String client);

    OutdatedFilesDeleteResultDTO deleteStorageOutdatedFiles(StorageTypeDTO storage, String client);
}
