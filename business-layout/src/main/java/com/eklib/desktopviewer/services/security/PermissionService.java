package com.eklib.desktopviewer.services.security;

import com.eklib.desktopviewer.dto.security.PermissionDTO;

import java.util.List;

/**
 * Created by Лол on 09.10.2015.
 */
public interface PermissionService {
    public PermissionDTO createPermission(PermissionDTO permissionForSave);

    public List<PermissionDTO> getAvailablePermission();

    void delete(Long id);

    PermissionDTO update(Long id, PermissionDTO dto);
}
