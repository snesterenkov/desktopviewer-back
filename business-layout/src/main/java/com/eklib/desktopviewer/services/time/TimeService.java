package com.eklib.desktopviewer.services.time;

import java.util.Map;

public interface TimeService {

    @Deprecated
    String getCurrentTime();

    Map<String, String> getTime();
}
