package com.eklib.desktopviewer.util;

import org.springframework.util.Assert;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by User on 15.02.2016.
 */
public class ImageUtil {

    private static final String imageFormat = "jpg";

    public static byte[] resizeImage(byte[] data, int width, int height){
        InputStream in = new ByteArrayInputStream(data);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try{
            Image image = ImageIO.read(in).getScaledInstance(width, height, Image.SCALE_SMOOTH);
            BufferedImage reducedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            reducedImage.createGraphics().drawImage(image, 0, 0, null);
            ImageIO.write(reducedImage, imageFormat, out);
        } catch (IOException e) {
            Assert.isTrue(false, "Couldn't resize image.");
        }
        return out.toByteArray();
    }
}
