package com.eklib.desktopviewer.container.service;

import com.eklib.desktopviewer.dto.enums.StorageTypeDTO;
import com.eklib.desktopviewer.services.file.FileService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Admin on 09.12.2015.
 */
@Component
public class FileServiceContainer implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    private Map<StorageTypeDTO, FileService> services;

    @Value("${FILE_SERVICE.DEFAULT}")
    private StorageTypeDTO currentFileServiceType;

    public FileService getCurrentFileService() {
        if(services == null) {
            initServices();
        }
        return services.get(currentFileServiceType);
    }

    public FileService getFileService(StorageTypeDTO storageTypeDTO){
        if(services == null){
            initServices();
        }
        return services.get(storageTypeDTO);
    }

    public StorageTypeDTO getCurrentFileServiceType() {
        return this.currentFileServiceType;
    }

    public void setCurrentFileServiceType(StorageTypeDTO fileServiceType) {
        this.currentFileServiceType = fileServiceType;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    private void initServices() {
        services = new HashMap<>();
        services.put(StorageTypeDTO.LocalStorage, (FileService)applicationContext.getBean("localStorageFileService"));
        services.put(StorageTypeDTO.GoogleDrive, (FileService)applicationContext.getBean("googleDriveFileService"));
    }
}
