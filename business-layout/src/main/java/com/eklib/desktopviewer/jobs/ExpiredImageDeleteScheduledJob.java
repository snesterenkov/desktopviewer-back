package com.eklib.desktopviewer.jobs;

import com.eklib.desktopviewer.persistance.repository.snapshot.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ExpiredImageDeleteScheduledJob {

    @Autowired
    private ImageRepository imageRepository;

    @Scheduled(cron = "0 0 0 * * ?")
    public void markExpiredImagesAsDeleted(){
        System.out.println("Start delete expired images.");
        imageRepository.markExpiredImagesAsDeleted(new Date());
        System.out.println("Finish delete expired images.");
    }
}
