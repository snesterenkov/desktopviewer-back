package com.eklib.desktopviewer.jobs;

import com.eklib.desktopviewer.container.service.FileServiceContainer;
import com.eklib.desktopviewer.dto.enums.StorageTypeDTO;
import com.eklib.desktopviewer.persistance.model.enums.StorageTypeEnum;
import com.eklib.desktopviewer.persistance.model.snapshot.ImageEntity;
import com.eklib.desktopviewer.persistance.repository.snapshot.ImageRepository;
import com.eklib.desktopviewer.services.file.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class FullDeleteIMagesScheduledJob {

    private static final int MAX_IMAGES_COUNT = 100;

    @Autowired
    private ImageRepository imageRepository;
    @Autowired
    private FileServiceContainer fileServiceContainer;

    @Scheduled(initialDelay = 5000l, fixedDelay = 60000l)
    public void deleteImagesMarkedAsDeleted() throws Exception{
        for(StorageTypeEnum storageType: StorageTypeEnum.values()){
            List<ImageEntity> images = imageRepository.findDeletedImagesByStorageType(storageType, MAX_IMAGES_COUNT);
            FileService fileService = fileServiceContainer.getFileService(StorageTypeDTO.valueOf(storageType.toString()));
            if(fileService != null){
                images = images.stream()
                        .filter(image -> fileService.deleteFileByLink(image.getViewLink()) &&
                                fileService.deleteFileByLink(image.getThumbLink()))
                        .collect(Collectors.toList());
            }
            if(images.size() > 0){
                imageRepository.deleteImages(images);
            }
        }
    }

}
