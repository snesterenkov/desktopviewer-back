package com.eklib.desktopviewer.exception.server;

import com.eklib.desktopviewer.exception.DesktopviewerRuntimeException;

/**
 * Created by User on 17.05.2016.
 */
public class DesktopviewerServerException extends DesktopviewerRuntimeException {

    public DesktopviewerServerException() {
        super();
    }

    public DesktopviewerServerException(String message) {
        super(message);
    }

    public DesktopviewerServerException(String message, Throwable cause) {
        super(message, cause);
    }

    public DesktopviewerServerException(Throwable cause) {
        super(cause);
    }

    protected DesktopviewerServerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
