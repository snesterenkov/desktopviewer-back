package com.eklib.desktopviewer.exception;

public class DesktopviewerRuntimeException extends RuntimeException {

    public DesktopviewerRuntimeException() {
        super();
    }

    public DesktopviewerRuntimeException(String message) {
        super(message);
    }

    public DesktopviewerRuntimeException(String message, Throwable cause){
        super(message, cause);
    }

    public DesktopviewerRuntimeException(Throwable cause) {
        super(cause);
    }

    protected DesktopviewerRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
