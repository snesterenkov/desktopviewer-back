package com.eklib.desktopviewer.exception.client;

import com.eklib.desktopviewer.exception.DesktopviewerRuntimeException;

/**
 * Created by User on 17.05.2016.
 */
public class DesktopviewerClientException extends DesktopviewerRuntimeException {

    public DesktopviewerClientException() {
        super();
    }

    public DesktopviewerClientException(String message) {
        super(message);
    }

    public DesktopviewerClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public DesktopviewerClientException(Throwable cause) {
        super(cause);
    }

    protected DesktopviewerClientException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
