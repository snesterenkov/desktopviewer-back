package com.eklib.desktopviewer.convertor.todto.snapshot;

import com.eklib.desktopviewer.dto.enums.ProductivityDTO;
import com.eklib.desktopviewer.dto.snapshot.SnapshotDTO;
import com.eklib.desktopviewer.dto.snapshot.UserStatsDTO;
import com.google.common.base.Function;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * Created by s.sheman on 15.09.2015.
 */
@Component
public class SnapshotsListToUserStatsDTO implements Function<List<SnapshotDTO>, UserStatsDTO> {

    @Override
    public UserStatsDTO apply(List<SnapshotDTO> snapshots) {
        if(snapshots.isEmpty()){
            return null;
        }
        Long snapshotsCount = (long)snapshots.size();
        Long productiveSnapshotsCount = 0l;
        Long efficiency = 0l;
        Date startTime = null;
        int userActivityPercentsSum = 0;
        for(SnapshotDTO snapshot: snapshots){
            if(!ProductivityDTO.UNPRODUCTIVE.equals(snapshot.getProductivity())){
                productiveSnapshotsCount++;
                userActivityPercentsSum += snapshot.getUserActivityPercent();
            }
            if(startTime == null || startTime.after(snapshot.getTime())){
                startTime = snapshot.getTime();
            }
        }
        UserStatsDTO result = new UserStatsDTO();
        result.setUserId(snapshots.get(0).getId());
        result.setSnapshotsCount(snapshotsCount);
        result.setProductiveSnapshotsCount(productiveSnapshotsCount);
        result.setStartTime(startTime);
        efficiency = Math.round((productiveSnapshotsCount/(double)snapshotsCount)*(userActivityPercentsSum/(double)productiveSnapshotsCount));
        result.setEfficiency(efficiency);
        return result;
    }
}
