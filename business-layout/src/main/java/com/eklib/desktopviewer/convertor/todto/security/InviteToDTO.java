package com.eklib.desktopviewer.convertor.todto.security;

import com.eklib.desktopviewer.dto.enums.InviteTypeDTO;
import com.eklib.desktopviewer.dto.security.InviteDTO;
import com.eklib.desktopviewer.persistance.model.security.InviteEntity;
import com.google.common.base.Function;
import org.springframework.stereotype.Component;

@Component
public class InviteToDTO implements Function<InviteEntity, InviteDTO> {

    @Override
    public InviteDTO apply(InviteEntity entity) {
        if(entity == null){
            return null;
        }

        InviteDTO dto = new InviteDTO();
        dto.setType(InviteTypeDTO.valueOf(entity.getType().name()));
        dto.setStructureId(entity.getStructureId());
        dto.setRoleId(entity.getRoleId());
        dto.setEmail(entity.getEmail());
        return dto;
    }
}
