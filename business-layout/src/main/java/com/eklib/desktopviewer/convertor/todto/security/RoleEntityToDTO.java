package com.eklib.desktopviewer.convertor.todto.security;

import com.eklib.desktopviewer.dto.security.RoleEntityDTO;
import com.eklib.desktopviewer.persistance.model.security.RoleEntity;
import com.google.common.base.Function;
import org.springframework.stereotype.Component;

/**
 * Created by vadim on 12.11.2014.
 */
@Component
public class RoleEntityToDTO implements Function<RoleEntity,RoleEntityDTO> {

    @Override
    public RoleEntityDTO apply(RoleEntity role) {
        if(role == null){
            return null;
        }
        return RoleEntityDTO.valueOf(role.name());
    }
}
