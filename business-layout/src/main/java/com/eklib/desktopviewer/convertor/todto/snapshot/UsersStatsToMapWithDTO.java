package com.eklib.desktopviewer.convertor.todto.snapshot;

import com.eklib.desktopviewer.dto.snapshot.UserStatsDTO;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 24.11.2015.
 */
@Component
public class UsersStatsToMapWithDTO {

    public Map<Long, UserStatsDTO> apply(List<Object[]> objects) {
        Map<Long,UserStatsDTO> result = new HashMap<>();
        for(Object[] stats: objects) {
            UserStatsDTO dto = new UserStatsDTO();
            dto.setUserId((long)stats[0]);
            dto.setStartTime((Date)stats[1]);
            dto.setSnapshotsCount((long)stats[2]);
            dto.setProductiveSnapshotsCount(Long.parseLong(stats[3].toString()));
            dto.setEfficiency(Math.round(Double.parseDouble(stats[4].toString())*100/(long)stats[2]));
            result.put(dto.getUserId(), dto);
        }
        return result;
    }
}
