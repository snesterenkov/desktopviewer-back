package com.eklib.desktopviewer.convertor.todto.security;

import com.eklib.desktopviewer.convertor.todto.companystructure.DepartmentToDTO;
import com.eklib.desktopviewer.dto.security.UserDepartmentRoleDTO;
import com.eklib.desktopviewer.persistance.model.security.UserDepartmentRole;
import com.google.common.base.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by s.sheman on 23.10.2015.
 */
@Component
public class UserDepartmentRoleToDTO implements Function<UserDepartmentRole, UserDepartmentRoleDTO> {

    @Autowired
    private UserToDTO userToDTO;

    @Autowired
    private DepartmentToDTO departmentToDTO;

    @Autowired
    private RoleToDTO roleToDTO;

    @Override
    public UserDepartmentRoleDTO apply(UserDepartmentRole userDepartmentRole) {
        if(userDepartmentRole == null){
            return null;
        }
        UserDepartmentRoleDTO result = new UserDepartmentRoleDTO();
        result.setId(userDepartmentRole.getId());
        result.setUser(userToDTO.apply(userDepartmentRole.getUser()));
        result.setDepartment(departmentToDTO.apply(userDepartmentRole.getDepartment()));
        result.setRole(roleToDTO.apply(userDepartmentRole.getRole()));
        return result;
    }
}
