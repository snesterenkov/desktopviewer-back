package com.eklib.desktopviewer.convertor.todto.snapshot;


import com.eklib.desktopviewer.dto.enums.ProductivityDTO;
import com.eklib.desktopviewer.dto.snapshot.SnapshotDTO;
import com.eklib.desktopviewer.persistance.model.snapshot.SnapshotEntity;
import com.google.common.base.Function;
import org.springframework.stereotype.Component;

@Component
public class SnapshotToDTO implements Function<SnapshotEntity, SnapshotDTO> {

    @Override
    public SnapshotDTO apply(SnapshotEntity snapshot) {
        if(snapshot == null){
            return null;
        }
        SnapshotDTO snapshotDTO = new SnapshotDTO();
        snapshotDTO.setId(snapshot.getId());
        snapshotDTO.setUserId(snapshot.getUser().getId());
        snapshotDTO.setMessage(snapshot.getMessage());
        snapshotDTO.setNote(snapshot.getNote());
        snapshotDTO.setProgectId(snapshot.getProject().getId());
        snapshotDTO.setCountMouseClick(snapshot.getCountMouseClick());
        snapshotDTO.setCountKeyboardClick(snapshot.getCountKeyboardClick());
        snapshotDTO.setTimeInterval(snapshot.getTimeInterval());
        snapshotDTO.setDate(snapshot.getDate());
        snapshotDTO.setTime(snapshot.getDate());
        snapshotDTO.setProductivity(ProductivityDTO.valueOf(snapshot.getProductivity().toString()));
        if(snapshot.getImage() != null){
            snapshotDTO.setOriginalFileName(snapshot.getImage().getViewLink());
            snapshotDTO.setResizedFileName(snapshot.getImage().getThumbLink());
        }
        return snapshotDTO;
    }
}
