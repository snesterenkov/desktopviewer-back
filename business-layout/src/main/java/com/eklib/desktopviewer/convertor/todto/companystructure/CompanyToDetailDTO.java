package com.eklib.desktopviewer.convertor.todto.companystructure;

import com.eklib.desktopviewer.convertor.todto.security.UserCompanyRoleToDTO;
import com.eklib.desktopviewer.convertor.todto.security.UserToDTO;
import com.eklib.desktopviewer.dto.companystructure.CompanyDetailDTO;
import com.eklib.desktopviewer.dto.enums.StatusDTO;
import com.eklib.desktopviewer.persistance.model.companystructure.CompanyEntity;
import com.google.common.base.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.stream.Collectors;

@Component
public class CompanyToDetailDTO implements Function<CompanyEntity,CompanyDetailDTO> {

    @Autowired
    private UserCompanyRoleToDTO userCompanyRoleToDTO;

    @Autowired
    private UserToDTO userToDTO;

    @Override
    public CompanyDetailDTO apply(CompanyEntity company) {
        if(company == null){
            return null;
        }
        CompanyDetailDTO companyDTO = new CompanyDetailDTO();
        companyDTO.setId(company.getId());
        companyDTO.setName(company.getName());
        companyDTO.setStatus(StatusDTO.valueOf(company.getStatus().name()));
        companyDTO.setOwner(userToDTO.apply(company.getOwner()));
        companyDTO.setWorkdayStart(new SimpleDateFormat("HH:mm").format(company.getWorkdayStart()));
        companyDTO.setWorkdayLength(company.getWorkdayLength());
        companyDTO.setWorkdayDelay(company.getWorkdayDelay());
        companyDTO.setScreenshotsSavingMonth(company.getScreenshotsSavingMonth());
        companyDTO.setUserRoles(company.getUserCompanyRoles().stream().map(userCompanyRoleToDTO::apply).collect(Collectors.toSet()));
        return companyDTO;
    }
}
