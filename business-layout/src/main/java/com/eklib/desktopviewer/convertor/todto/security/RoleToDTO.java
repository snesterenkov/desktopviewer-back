package com.eklib.desktopviewer.convertor.todto.security;

import com.eklib.desktopviewer.convertor.todto.companystructure.StructureTypeToDTO;
import com.eklib.desktopviewer.dto.security.RoleDTO;
import com.eklib.desktopviewer.persistance.model.security.Role;
import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by s.sheman on 12.10.2015.
 */
@Component
public class RoleToDTO implements Function<Role, RoleDTO> {
    @Autowired
    private PermissionToDTO permissionToDTO;

    @Autowired
    private StructureTypeToDTO structureTypeToDTO;

    @Override
    public RoleDTO apply(Role role) {
        if(role == null){
            return null;
        }
        RoleDTO result = new RoleDTO();
        result.setId(role.getId());
        result.setName(role.getName());
        result.setStructureType(structureTypeToDTO.apply(role.getStructureType()));
        result.setPermissions(FluentIterable.from(role.getPermissions()).transform(permissionToDTO).toList());
        return result;
    }
}
