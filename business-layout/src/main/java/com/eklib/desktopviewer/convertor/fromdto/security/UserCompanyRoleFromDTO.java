package com.eklib.desktopviewer.convertor.fromdto.security;

import com.eklib.desktopviewer.convertor.fromdto.companystructure.CompanyFromDTO;
import com.eklib.desktopviewer.dto.security.UserCompanyRoleDTO;
import com.eklib.desktopviewer.persistance.model.security.UserCompanyRole;
import com.google.common.base.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by s.sheman on 29.10.2015.
 */
@Component
public class UserCompanyRoleFromDTO implements Function<UserCompanyRoleDTO, UserCompanyRole>{
    @Autowired
    private UserFromDTO userFromDTO;
    @Autowired
    private CompanyFromDTO companyFromDTO;
    @Autowired
    private RoleFromDTO roleFromDTO;

    @Override
    public UserCompanyRole apply(UserCompanyRoleDTO userCompanyRoleDTO) {
        if(userCompanyRoleDTO == null){
            return null;
        }
        UserCompanyRole result = new UserCompanyRole();
        result.setId(userCompanyRoleDTO.getId());
        result.setUser(userFromDTO.apply(userCompanyRoleDTO.getUser()));
        result.setCompany(companyFromDTO.apply(userCompanyRoleDTO.getCompany()));
        result.setRole(roleFromDTO.apply(userCompanyRoleDTO.getRole()));
        return result;
    }
}
