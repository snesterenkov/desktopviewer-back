package com.eklib.desktopviewer.convertor.todto.companystructure;

import com.eklib.desktopviewer.dto.companystructure.ProjectDTO;
import com.eklib.desktopviewer.dto.enums.StatusDTO;
import com.eklib.desktopviewer.persistance.model.companystructure.ProjectEntity;
import com.google.common.base.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProjectToDTO implements Function<ProjectEntity, ProjectDTO> {

    @Autowired
    private DepartmentToDTO departmentToDTO;

    @Override
    public ProjectDTO apply(ProjectEntity project) {
        if(project == null){
            return null;
        }
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setStatus(StatusDTO.valueOf(project.getStatus().name()));
        if(project.getDepartment() != null){
            projectDTO.setDepartmentId(project.getDepartment().getId());
            projectDTO.setDepartment(departmentToDTO.apply(project.getDepartment()));
            projectDTO.setParentStatus(StatusDTO.valueOf(project.getDepartment().getStatus().name()));
        }
        return projectDTO;
    }
}
