package com.eklib.desktopviewer.convertor.fromdto.security;

import com.eklib.desktopviewer.dto.security.RoleEntityDTO;
import com.eklib.desktopviewer.persistance.model.security.RoleEntity;
import com.google.common.base.Function;
import org.springframework.stereotype.Component;

/**
 * Created by vadim on 12.11.2014.
 */
@Component
public class RoleEntityFromDTO implements Function<RoleEntityDTO, RoleEntity> {

    @Override
    public RoleEntity apply(RoleEntityDTO roleEntityDTO) {
        if(roleEntityDTO == null){
            return null;
        }
        return RoleEntity.valueOf(roleEntityDTO.name());
    }
}
