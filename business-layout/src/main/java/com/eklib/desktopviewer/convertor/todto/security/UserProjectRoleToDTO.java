package com.eklib.desktopviewer.convertor.todto.security;

import com.eklib.desktopviewer.convertor.todto.companystructure.ProjectToDTO;
import com.eklib.desktopviewer.dto.security.UserProjectRoleDTO;
import com.eklib.desktopviewer.persistance.model.security.UserProjectRole;
import com.eklib.desktopviewer.persistance.model.security.UserProjectRoleCompositeKey;
import com.google.common.base.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/*
 * Created by s.sheman on 15.10.2015.

*/

@Component
public class UserProjectRoleToDTO implements Function<UserProjectRole, UserProjectRoleDTO> {

    @Autowired
    private UserToDTO userToDTO;

    @Autowired
    private ProjectToDTO projectToDTO;

    @Autowired
    private RoleToDTO roleToDTO;

    @Override
    public UserProjectRoleDTO apply(UserProjectRole userProjectRole) {
        if(userProjectRole == null){
            return null;
        }
        UserProjectRoleDTO result = new UserProjectRoleDTO();
        result.setId(userProjectRole.getId());
        result.setUser(userToDTO.apply(userProjectRole.getUser()));
        result.setProject(projectToDTO.apply(userProjectRole.getProject()));
        result.setRole(roleToDTO.apply(userProjectRole.getRole()));
        return result;
    }
}
