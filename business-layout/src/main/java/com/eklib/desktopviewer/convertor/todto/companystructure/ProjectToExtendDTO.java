package com.eklib.desktopviewer.convertor.todto.companystructure;

import com.eklib.desktopviewer.dto.companystructure.ProjectExtendDTO;
import com.eklib.desktopviewer.persistance.model.companystructure.ProjectEntity;
import com.google.common.base.Function;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;

/**
 * Created by User on 16.03.2016.
 */
@Component
public class ProjectToExtendDTO implements Function<ProjectEntity, ProjectExtendDTO>{

    @Nullable
    @Override
    public ProjectExtendDTO apply(ProjectEntity projectEntity) {
        if(projectEntity == null){
            return null;
        }

        ProjectExtendDTO project = new ProjectExtendDTO();
        project.setId(projectEntity.getId());
        project.setName(projectEntity.getName());
        if(projectEntity.getDepartment() != null){
            project.setDepartmentId(projectEntity.getDepartment().getId());
            project.setDepartmentName(projectEntity.getDepartment().getName());
            if(projectEntity.getDepartment().getCompany() != null){
                project.setCompanyId(projectEntity.getDepartment().getCompany().getId());
                project.setCompanyName(projectEntity.getDepartment().getCompany().getName());
            }
        }
        return project;
    }
}
