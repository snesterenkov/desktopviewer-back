package com.eklib.desktopviewer.convertor.todto.companystructure;

import com.eklib.desktopviewer.dto.companystructure.StructureTypeDTO;
import com.eklib.desktopviewer.persistance.model.companystructure.StructureType;
import com.google.common.base.Function;
import org.springframework.stereotype.Component;

/**
 * Created by s.sheman on 15.10.2015.
 */
@Component
public class StructureTypeToDTO implements Function<StructureType, StructureTypeDTO> {

    @Override
    public StructureTypeDTO apply(StructureType structureType) {
        if(structureType == null){
            return null;
        }
        StructureTypeDTO result = new StructureTypeDTO();
        result.setId(structureType.getId());
        result.setType(structureType.getType());
        return result;
    }
}
