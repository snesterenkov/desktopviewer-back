package com.eklib.desktopviewer.convertor.fromdto.security;

import com.eklib.desktopviewer.convertor.fromdto.companystructure.StructureTypeFromDTO;
import com.eklib.desktopviewer.dto.security.RoleDTO;
import com.eklib.desktopviewer.persistance.model.security.Role;
import com.google.common.base.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by s.sheman on 12.10.2015.
 */
@Component
public class RoleFromDTO implements Function<RoleDTO, Role> {

    @Autowired
    private PermissionFromDTO permissionFromDTO;

    @Autowired
    private StructureTypeFromDTO structureTypeFromDTO;

    @Override
    public Role apply(RoleDTO roleDTO) {
        if(roleDTO == null){
            return null;
        }
        Role result = new Role();
        result.setId(roleDTO.getId());
        result.setName(roleDTO.getName());
        result.setStructureType(structureTypeFromDTO.apply(roleDTO.getStructureType()));
        result.setPermissions(roleDTO.getPermissions().stream().map(permissionFromDTO::apply).collect(Collectors.toList()));
        return result;
    }
}
