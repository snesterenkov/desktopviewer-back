package com.eklib.desktopviewer.convertor.todto.companystructure;

import com.eklib.desktopviewer.convertor.todto.security.UserProjectRoleToDTO;
import com.eklib.desktopviewer.convertor.todto.security.UserToDTO;
import com.eklib.desktopviewer.dto.companystructure.ProjectDetailDTO;
import com.eklib.desktopviewer.dto.enums.StatusDTO;
import com.eklib.desktopviewer.persistance.model.companystructure.ProjectEntity;
import com.google.common.base.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ProjectToDetailDTO implements Function<ProjectEntity, ProjectDetailDTO> {

    @Autowired
    private DepartmentToDTO departmentToDTO;

    @Autowired
    private UserProjectRoleToDTO userProjectRoleToDTO;

    @Autowired
    private UserToDTO userToDTO;

    @Override
    public ProjectDetailDTO apply(ProjectEntity project) {
        if(project == null){
            return null;
        }
        ProjectDetailDTO projectDTO = new ProjectDetailDTO();
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setStatus(StatusDTO.valueOf(project.getStatus().name()));
        projectDTO.setOwner(userToDTO.apply(project.getOwner()));
        if(project.getDepartment() != null){
            projectDTO.setDepartmentId(project.getDepartment().getId());
            projectDTO.setDepartment(departmentToDTO.apply(project.getDepartment()));
            projectDTO.setParentStatus(StatusDTO.valueOf(project.getDepartment().getStatus().name()));
        }
        projectDTO.setUserRoles(project.getUserProjectRoles().stream().map(userProjectRoleToDTO::apply).collect(Collectors.toList()));
        return projectDTO;
    }
}
