package com.eklib.desktopviewer.convertor.todto.application;

import com.eklib.desktopviewer.dto.application.ApplicationDTO;
import com.eklib.desktopviewer.persistance.model.application.ApplicationEntity;
import com.google.common.base.Function;
import org.springframework.stereotype.Component;

/**
 * Created by Admin on 20.10.2015.
 */
@Component
public class ApplicationToDTO implements Function<ApplicationEntity, ApplicationDTO> {

    @Override
    public ApplicationDTO apply(ApplicationEntity applicationEntity) {
        if(applicationEntity == null){
            return null;
        }
        ApplicationDTO applicationDTO = new ApplicationDTO();
        applicationDTO.setId(applicationEntity.getId());
        applicationDTO.setName(applicationEntity.getName());
        return applicationDTO;
    }
}
