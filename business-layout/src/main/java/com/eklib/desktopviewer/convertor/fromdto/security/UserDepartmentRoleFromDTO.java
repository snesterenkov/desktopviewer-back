package com.eklib.desktopviewer.convertor.fromdto.security;

import com.eklib.desktopviewer.convertor.fromdto.companystructure.DepartmentFromDTO;
import com.eklib.desktopviewer.dto.security.UserDepartmentRoleDTO;
import com.eklib.desktopviewer.persistance.model.security.UserDepartmentRole;
import com.google.common.base.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by s.sheman on 23.10.2015.
 */
@Component
public class UserDepartmentRoleFromDTO implements Function<UserDepartmentRoleDTO, UserDepartmentRole> {
    @Autowired
    private UserFromDTO userFromDTO;

    @Autowired
    private DepartmentFromDTO departmentFromDTO;

    @Autowired
    private RoleFromDTO roleFromDTO;

    @Override
    public UserDepartmentRole apply(UserDepartmentRoleDTO userDepartmentRoleDTO) {
        if(userDepartmentRoleDTO == null){
            return null;
        }
        UserDepartmentRole result = new UserDepartmentRole();
        result.setId(userDepartmentRoleDTO.getId());
        result.setUser(userFromDTO.apply(userDepartmentRoleDTO.getUser()));
        result.setDepartment(departmentFromDTO.apply(userDepartmentRoleDTO.getDepartment()));
        result.setRole(roleFromDTO.apply(userDepartmentRoleDTO.getRole()));
        return result;
    }
}
