package com.eklib.desktopviewer.convertor.todto.companystructure;

import com.eklib.desktopviewer.convertor.todto.security.UserDepartmentRoleToDTO;
import com.eklib.desktopviewer.convertor.todto.security.UserToDTO;
import com.eklib.desktopviewer.dto.companystructure.DepartmentDetailDTO;
import com.eklib.desktopviewer.dto.enums.StatusDTO;
import com.eklib.desktopviewer.persistance.model.companystructure.DepartmentEntity;
import com.google.common.base.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class DepartmentToDetailDTO implements Function<DepartmentEntity,DepartmentDetailDTO> {

    @Autowired
    private CompanyToDTO companyToDTO;

    @Autowired
    private UserDepartmentRoleToDTO userDepartmentRoleToDTO;

    @Autowired
    private UserToDTO userToDTO;

    @Override
    public DepartmentDetailDTO apply(DepartmentEntity department) {
        if(department == null){
            return null;
        }

        DepartmentDetailDTO departmentDTO = new DepartmentDetailDTO();
        departmentDTO.setId(department.getId());
        departmentDTO.setName(department.getName());
        departmentDTO.setStatus(StatusDTO.valueOf(department.getStatus().name()));
        departmentDTO.setOwner(userToDTO.apply(department.getOwner()));
        if(department.getCompany() != null){
            departmentDTO.setCompanyId(department.getCompany().getId());
            departmentDTO.setCompany(companyToDTO.apply(department.getCompany()));
            departmentDTO.setParentStatus(StatusDTO.valueOf(department.getCompany().getStatus().name()));
        }
        departmentDTO.setUserRoles(department.getUserDepartmentRoles().stream().map(userDepartmentRoleToDTO::apply).collect(Collectors.toList()));
        return departmentDTO;
    }
}
