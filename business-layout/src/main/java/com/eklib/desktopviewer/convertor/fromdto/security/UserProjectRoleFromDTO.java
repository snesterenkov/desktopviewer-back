package com.eklib.desktopviewer.convertor.fromdto.security;

import com.eklib.desktopviewer.convertor.fromdto.companystructure.ProjectFromDTO;
import com.eklib.desktopviewer.dto.security.UserProjectRoleDTO;
import com.eklib.desktopviewer.persistance.model.security.UserProjectRole;
import com.google.common.base.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by s.sheman on 15.10.2015.
 */

@Component
public class UserProjectRoleFromDTO implements Function<UserProjectRoleDTO, UserProjectRole>{

    @Autowired
    private UserFromDTO userFromDTO;

    @Autowired
    private ProjectFromDTO projectFromDTO;

    @Autowired
    private RoleFromDTO roleFromDTO;

    @Override
    public UserProjectRole apply(UserProjectRoleDTO userProjectRoleDTO) {
        if(userProjectRoleDTO == null){
            return null;
        }
        UserProjectRole result = new UserProjectRole();
        result.setId(userProjectRoleDTO.getId());
        result.setUser(userFromDTO.apply(userProjectRoleDTO.getUser()));
        result.setProject(projectFromDTO.apply(userProjectRoleDTO.getProject()));
        result.setRole(roleFromDTO.apply(userProjectRoleDTO.getRole()));
        return result;
    }
}
