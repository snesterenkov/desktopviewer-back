package com.eklib.desktopviewer.convertor.todto.companystructure;

import com.eklib.desktopviewer.dto.companystructure.DepartmentDTO;
import com.eklib.desktopviewer.dto.enums.StatusDTO;
import com.eklib.desktopviewer.persistance.model.companystructure.DepartmentEntity;
import com.google.common.base.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DepartmentToDTO implements Function<DepartmentEntity, DepartmentDTO> {

    @Autowired
    private CompanyToDTO companyToDTO;

    @Override
    public DepartmentDTO apply(DepartmentEntity department) {
        if(department == null){
            return null;
        }
        DepartmentDTO departmentDTO = new DepartmentDTO();
        departmentDTO.setId(department.getId());
        departmentDTO.setName(department.getName());
        departmentDTO.setStatus(StatusDTO.valueOf(department.getStatus().name()));
        if(department.getCompany() != null){
            departmentDTO.setCompanyId(department.getCompany().getId());
            departmentDTO.setCompany(companyToDTO.apply(department.getCompany()));
            departmentDTO.setParentStatus(StatusDTO.valueOf(department.getCompany().getStatus().name()));
        }
        return departmentDTO;
    }
}
