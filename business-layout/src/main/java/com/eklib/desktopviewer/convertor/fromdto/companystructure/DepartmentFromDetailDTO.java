package com.eklib.desktopviewer.convertor.fromdto.companystructure;

import com.eklib.desktopviewer.convertor.fromdto.security.UserDepartmentRoleFromDTO;
import com.eklib.desktopviewer.dto.companystructure.DepartmentDetailDTO;
import com.eklib.desktopviewer.persistance.model.companystructure.DepartmentEntity;
import com.eklib.desktopviewer.persistance.repository.companystructure.CompanyRepository;
import com.eklib.desktopviewer.persistance.repository.companystructure.DepartmentRepository;
import com.google.common.base.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by s.sheman on 28.10.2015.
 */
@Component
public class DepartmentFromDetailDTO implements Function<DepartmentDetailDTO, DepartmentEntity> {

    @Autowired
    private UserDepartmentRoleFromDTO userDepartmentRoleFromDTO;
    @Autowired
    private DepartmentRepository departmentRepository;
    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public DepartmentEntity apply(DepartmentDetailDTO departmentDetailDTO) {
        DepartmentEntity department;
        if(departmentDetailDTO == null){
            return null;
        }
        if(departmentDetailDTO.getId() == null || departmentDetailDTO.getId() == 0L){
            department = new DepartmentEntity();
        } else {
            department = departmentRepository.findById(departmentDetailDTO.getId());
        }
        department.setName(departmentDetailDTO.getName());
        department.setCompany(companyRepository.findById(departmentDetailDTO.getCompanyId()));
        department.getUserDepartmentRoles().clear();
        department.getUserDepartmentRoles().addAll(departmentDetailDTO.getUserRoles().stream().map(userDepartmentRoleFromDTO::apply).collect(Collectors.toSet()));
        return department;
    }
}
