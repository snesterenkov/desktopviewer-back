package com.eklib.desktopviewer.convertor.fromdto.application;

import com.eklib.desktopviewer.dto.application.ApplicationDTO;
import com.eklib.desktopviewer.persistance.model.application.ApplicationEntity;
import com.eklib.desktopviewer.persistance.repository.application.ApplicationRepository;
import com.google.common.base.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Admin on 20.10.2015.
 */
@Component
public class ApplicationFromDTO implements Function<ApplicationDTO, ApplicationEntity> {

    @Autowired
    private ApplicationRepository applicationRepository;

    @Override
    public ApplicationEntity apply(ApplicationDTO applicationDTO) {
        ApplicationEntity applicationEntity;
        if(applicationDTO == null){
            return null;
        }
        if(applicationDTO.getId() == null || applicationDTO.getId() == 0L){
            applicationEntity = new ApplicationEntity();
        } else {
            applicationEntity = applicationRepository.findById(applicationDTO.getId());
        }
        applicationEntity.setName(applicationDTO.getName());
        return applicationEntity;
    }
}
