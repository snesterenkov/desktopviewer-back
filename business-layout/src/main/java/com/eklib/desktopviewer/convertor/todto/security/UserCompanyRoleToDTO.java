package com.eklib.desktopviewer.convertor.todto.security;

import com.eklib.desktopviewer.convertor.todto.companystructure.CompanyToDTO;
import com.eklib.desktopviewer.dto.security.UserCompanyRoleDTO;
import com.eklib.desktopviewer.persistance.model.security.UserCompanyRole;
import com.google.common.base.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by s.sheman on 29.10.2015.
 */
@Component
public class UserCompanyRoleToDTO implements Function<UserCompanyRole, UserCompanyRoleDTO> {
    @Autowired
    private UserToDTO userToDTO;
    @Autowired
    private CompanyToDTO companyToDTO;
    @Autowired
    private RoleToDTO roleToDTO;

    @Override
    public UserCompanyRoleDTO apply(UserCompanyRole userCompanyRole) {
        if(userCompanyRole == null){
            return null;
        }
        UserCompanyRoleDTO dto = new UserCompanyRoleDTO();
        dto.setId(userCompanyRole.getId());
        dto.setUser(userToDTO.apply(userCompanyRole.getUser()));
        dto.setCompany(companyToDTO.apply(userCompanyRole.getCompany()));
        dto.setRole(roleToDTO.apply(userCompanyRole.getRole()));
        return dto;
    }
}
