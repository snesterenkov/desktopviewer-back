package com.eklib.desktopviewer.convertor.todto.companystructure;

import com.eklib.desktopviewer.dto.companystructure.CompanyDTO;
import com.eklib.desktopviewer.dto.enums.StatusDTO;
import com.eklib.desktopviewer.persistance.model.companystructure.CompanyEntity;
import com.google.common.base.Function;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

@Component
public class CompanyToDTO implements Function<CompanyEntity,CompanyDTO> {


    @Override
    public CompanyDTO apply(CompanyEntity company) {
        if(company == null){
            return null;
        }
        CompanyDTO companyDTO = new CompanyDTO();
        companyDTO.setId(company.getId());
        companyDTO.setName(company.getName());
        companyDTO.setStatus(StatusDTO.valueOf(company.getStatus().name()));
        companyDTO.setWorkdayStart(new SimpleDateFormat("HH:mm").format(company.getWorkdayStart()));
        companyDTO.setWorkdayLength(company.getWorkdayLength());
        companyDTO.setWorkdayDelay(company.getWorkdayDelay());
        companyDTO.setScreenshotsSavingMonth(company.getScreenshotsSavingMonth());
        return companyDTO;
    }
}
