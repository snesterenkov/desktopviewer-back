package com.eklib.desktopviewer.convertor.fromdto.companystructure;

import com.eklib.desktopviewer.dto.companystructure.StructureTypeDTO;
import com.eklib.desktopviewer.persistance.model.companystructure.StructureType;
import com.google.common.base.Function;
import org.springframework.stereotype.Component;

/**
 * Created by s.sheman on 15.10.2015.
 */
@Component
public class StructureTypeFromDTO implements Function<StructureTypeDTO, StructureType> {

    @Override
    public StructureType apply(StructureTypeDTO structureTypeDTO) {
        if(structureTypeDTO == null){
            return null;
        }
        StructureType result = new StructureType();
        result.setId(structureTypeDTO.getId());
        result.setType(structureTypeDTO.getType());
        return result;
    }
}
