package com.eklib.desktopviewer.convertor.todto.companystructure;

import com.eklib.desktopviewer.dto.companystructure.DepartmentExtendDTO;
import com.eklib.desktopviewer.dto.companystructure.ProjectDTO;
import com.eklib.desktopviewer.persistance.model.companystructure.DepartmentEntity;
import com.google.common.base.Function;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Created by Admin on 06.11.2015.
 */
@Component
public class DepartmentToExtendDTO implements Function<DepartmentEntity, DepartmentExtendDTO> {

    @Override
    public DepartmentExtendDTO apply(DepartmentEntity department) {
        if(department == null){
            return null;
        }
        DepartmentExtendDTO dto = new DepartmentExtendDTO();
        dto.setId(department.getId());
        dto.setName(department.getName());
        if(department.getCompany() != null){
            dto.setCompanyId(department.getCompany().getId());
        }
        dto.setProjects(new ArrayList<ProjectDTO>());
        return dto;
    }
}
