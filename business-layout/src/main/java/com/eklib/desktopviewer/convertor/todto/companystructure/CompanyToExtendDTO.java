package com.eklib.desktopviewer.convertor.todto.companystructure;

import com.eklib.desktopviewer.dto.companystructure.CompanyExtendDTO;
import com.eklib.desktopviewer.dto.companystructure.DepartmentExtendDTO;
import com.eklib.desktopviewer.persistance.model.companystructure.CompanyEntity;
import com.google.common.base.Function;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Admin on 06.11.2015.
 */
@Component
public class CompanyToExtendDTO implements Function<CompanyEntity, CompanyExtendDTO> {

    @Override
    public CompanyExtendDTO apply(CompanyEntity company) {
        if(company == null){
            return null;
        }
        CompanyExtendDTO dto = new CompanyExtendDTO();
        dto.setId(company.getId());
        dto.setName(company.getName());
        dto.setWorkdayStart(new SimpleDateFormat("HH:mm").format(company.getWorkdayStart()));
        dto.setWorkdayLength(company.getWorkdayLength());
        dto.setWorkdayDelay(company.getWorkdayDelay());
        dto.setDepartments(new ArrayList<DepartmentExtendDTO>());
        return dto;
    }
}
