package com.eklib.desktopviewer.convertor.fromdto.companystructure;

import com.eklib.desktopviewer.dto.companystructure.CompanyDTO;
import com.eklib.desktopviewer.persistance.model.companystructure.CompanyEntity;
import com.eklib.desktopviewer.persistance.repository.companystructure.CompanyRepository;
import com.google.common.base.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Component
public class CompanyFromDTO implements Function<CompanyDTO, CompanyEntity> {

    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public CompanyEntity apply(CompanyDTO companyDTO) {
        CompanyEntity company;
        if(companyDTO == null){
            return null;
        }
        if(companyDTO.getId() == null || companyDTO.getId() == 0L){
            company = new CompanyEntity();
        } else {
            company = companyRepository.findById(companyDTO.getId());
        }
        company.setName(companyDTO.getName());
        try {
            company.setWorkdayStart(new SimpleDateFormat("HH:mm").parse(companyDTO.getWorkdayStart()));
        } catch (ParseException e) {
            throw  new IllegalArgumentException("Bad data format");
        }
        company.setWorkdayLength(companyDTO.getWorkdayLength());
        company.setWorkdayDelay(companyDTO.getWorkdayDelay() == null? 0: companyDTO.getWorkdayDelay());
        company.setScreenshotsSavingMonth(companyDTO.getScreenshotsSavingMonth() == null? 1: companyDTO.getScreenshotsSavingMonth());
        return company;
    }
}
