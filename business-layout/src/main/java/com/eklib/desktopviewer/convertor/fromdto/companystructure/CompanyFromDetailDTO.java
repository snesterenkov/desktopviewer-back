package com.eklib.desktopviewer.convertor.fromdto.companystructure;

import com.eklib.desktopviewer.convertor.fromdto.security.UserCompanyRoleFromDTO;
import com.eklib.desktopviewer.dto.companystructure.CompanyDetailDTO;
import com.eklib.desktopviewer.persistance.model.companystructure.CompanyEntity;
import com.google.common.base.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Created by s.sheman on 29.10.2015.
 */
@Component
public class CompanyFromDetailDTO implements Function<CompanyDetailDTO, CompanyEntity> {

    @Autowired
    private CompanyFromDTO companyFromDTO;
    @Autowired
    private UserCompanyRoleFromDTO userCompanyRoleFromDTO;

    @Override
    public CompanyEntity apply(CompanyDetailDTO companyDetailDTO) {
        if(companyDetailDTO == null){
            return null;
        }
        CompanyEntity entity = companyFromDTO.apply(companyDetailDTO);
        if(entity != null) {
            entity.getUserCompanyRoles().clear();
            entity.getUserCompanyRoles().addAll(companyDetailDTO.getUserRoles().stream().map(userCompanyRoleFromDTO::apply).collect(Collectors.toSet()));
        }
        return entity;
    }
}
