package com.eklib.desktopviewer.convertor.fromdto.security;

import com.eklib.desktopviewer.dto.security.PermissionDTO;
import com.eklib.desktopviewer.persistance.model.security.PermissionEntity;
import com.google.common.base.Function;
import org.springframework.stereotype.Component;

/**
 * Created by Лол on 09.10.2015.
 */
@Component
public class PermissionFromDTO implements Function<PermissionDTO, PermissionEntity> {

    @Override
    public PermissionEntity apply(PermissionDTO permissionDTO) {
        if(permissionDTO == null){
            return null;
        }
        PermissionEntity result = new PermissionEntity();
        result.setId(permissionDTO.getId());
        result.setName(permissionDTO.getName());
        result.setCode(permissionDTO.getCode());
        return result;
    }
}
