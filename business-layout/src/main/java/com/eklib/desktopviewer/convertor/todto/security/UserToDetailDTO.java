package com.eklib.desktopviewer.convertor.todto.security;

import com.eklib.desktopviewer.dto.security.RoleDTO;
import com.eklib.desktopviewer.dto.security.UserDetailDTO;
import com.eklib.desktopviewer.persistance.model.security.UserCompanyRole;
import com.eklib.desktopviewer.persistance.model.security.UserDepartmentRole;
import com.eklib.desktopviewer.persistance.model.security.UserEntity;
import com.eklib.desktopviewer.persistance.model.security.UserProjectRole;
import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vadim on 13.11.2014.
 */
@Component
public class UserToDetailDTO implements Function<UserEntity, UserDetailDTO>{

    @Autowired
    private RoleToDTO roleToDTO;

    @Override
    public UserDetailDTO apply(UserEntity user) {
        if(user == null){
            return null;
        }
        UserDetailDTO userDTO = new UserDetailDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setEmail(user.getEmail());
        userDTO.setRole(roleToDTO.apply(user.getPrincipalRole()));
        List<RoleDTO> userRoles = new ArrayList<>();
        if(user.getPrincipalRole() != null) {
            userRoles.add(roleToDTO.apply(user.getPrincipalRole()));
        }
        user.getUserCompanyRoles().stream()
                .map(UserCompanyRole::getRole)
                .distinct()
                .forEach(role -> userRoles.add(roleToDTO.apply(role)));
        user.getUserDepartmentRoles().stream()
                .map(UserDepartmentRole::getRole)
                .distinct()
                .forEach(role -> userRoles.add(roleToDTO.apply(role)));
        user.getUserProjectRoles().stream()
                .map(UserProjectRole::getRole)
                .distinct()
                .forEach(role -> userRoles.add(roleToDTO.apply(role)));
        userDTO.setRoles(userRoles);

        return userDTO;
    }
}
