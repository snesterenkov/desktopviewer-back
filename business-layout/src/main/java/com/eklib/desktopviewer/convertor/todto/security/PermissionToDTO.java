package com.eklib.desktopviewer.convertor.todto.security;

import com.eklib.desktopviewer.dto.security.PermissionDTO;
import com.eklib.desktopviewer.persistance.model.security.PermissionEntity;
import com.google.common.base.Function;
import org.springframework.stereotype.Component;

/**
 * Created by Лол on 09.10.2015.
 */
@Component
public class PermissionToDTO implements Function<PermissionEntity, PermissionDTO> {

    @Override
    public PermissionDTO apply(PermissionEntity permissionEntity) {
        if(permissionEntity == null){
            return null;
        }
        PermissionDTO result = new PermissionDTO();
        result.setId(permissionEntity.getId());
        result.setName(permissionEntity.getName());
        result.setCode(permissionEntity.getCode());
        return result;
    }
}
