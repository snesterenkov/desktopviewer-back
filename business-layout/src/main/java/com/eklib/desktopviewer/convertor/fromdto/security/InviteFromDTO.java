package com.eklib.desktopviewer.convertor.fromdto.security;

import com.eklib.desktopviewer.dto.security.InviteDTO;
import com.eklib.desktopviewer.persistance.model.enums.InviteType;
import com.eklib.desktopviewer.persistance.model.security.InviteEntity;
import com.google.common.base.Function;
import org.springframework.stereotype.Component;

@Component
public class InviteFromDTO implements Function<InviteDTO, InviteEntity>{

    @Override
    public InviteEntity apply(InviteDTO dto) {
        if(dto == null){
            return null;
        }

        InviteEntity entity = new InviteEntity();
        entity.setType(InviteType.valueOf(dto.getType().name()));
        entity.setStructureId(dto.getStructureId());
        entity.setRoleId(dto.getRoleId());
        entity.setEmail(dto.getEmail());
        return entity;
    }
}
