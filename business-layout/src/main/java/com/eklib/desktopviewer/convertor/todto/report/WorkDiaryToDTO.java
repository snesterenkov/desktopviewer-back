package com.eklib.desktopviewer.convertor.todto.report;

import com.eklib.desktopviewer.dto.report.PeriodDTO;
import com.eklib.desktopviewer.dto.report.WorkDiaryDTO;
import com.eklib.desktopviewer.dto.security.UserDTO;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by alex on 4/2/2015.
 */
@Component
public class WorkDiaryToDTO  {

    public WorkDiaryDTO apply(UserDTO user, List<PeriodDTO> hoursInPeriodDTOs) {
        WorkDiaryDTO workDiaryDTO = new WorkDiaryDTO();
        workDiaryDTO.setUser(user);workDiaryDTO.setPeriods(hoursInPeriodDTOs);
        return workDiaryDTO;
    }
}
